<?php

require_once '../vendor/autoload.php';
$purchaseId = $_GET['purchaseId'];

function Convert($amount_number)
{
    $amount_number = number_format($amount_number, 2, ".", "");
    $pt = strpos($amount_number, ".");
    $number = $fraction = "";
    if ($pt === false) {
        $number = $amount_number;
    } else {
        $number = substr($amount_number, 0, $pt);
        $fraction = substr($amount_number, $pt + 1);
    }

    $ret = "";
    $baht = ReadNumber($number);
    if ($baht != "") {
        $ret .= $baht . "บาท";
    }

    $satang = ReadNumber($fraction);
    if ($satang != "") {
        $ret .= $satang . "สตางค์";
    } else {
        $ret .= "ถ้วน";
    }

    return $ret;
}

function ReadNumber($number)
{
    $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
    $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
    $number = $number + 0;
    $ret = "";
    if ($number == 0) {
        return $ret;
    }

    if ($number > 1000000) {
        $ret .= ReadNumber(intval($number / 1000000)) . "ล้าน";
        $number = intval(fmod($number, 1000000));
    }

    $divider = 100000;
    $pos = 0;
    while ($number > 0) {
        $d = intval($number / $divider);
        $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
        ((($divider == 10) && ($d == 1)) ? "" :
            ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
        $ret .= ($d ? $position_call[$pos] : "");
        $number = $number % $divider;
        $divider = $divider / 10;
        $pos++;
    }
    return $ret;
}
// echo $purchaseId;

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => 'http://localhost:3000/tb_purchase/get_purchase_by_id?purchase_id=' . $purchaseId,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
));

$response = curl_exec($curl);
curl_close($curl);

$purchasePRData = json_decode($response, true);
// $purchaseData["data"]["purchaseId"];
// // $purchaseData["data"]["approvalNumber"];

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];
$mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/pdf/tmp',
    'fontdata' => $fontData + [
        'sarabun' => [ // ส่วนที่ต้องเป็น lower case ครับ
            'R' => 'THSarabunNew.ttf',
            // 'I' => 'THSarabunNewItalic.ttf',
            // 'B' =>  'THSarabunNewBold.ttf',
            // 'BI' => "THSarabunNewBoldItalic.ttf",
        ],
    ],
    'format' => 'A4-L',
    'orientation' => 'L',
]);

$img = '<img src="../images/img/logo-pea.jpg" alt="Girl in a jacket" width="40px" height="40px"></img>';
$warehouseIdEquipOwner = $purchasePRData["data"]["warehouseIdEquipOwner"];
$middleJobNumber = $purchasePRData["data"]["middleJobNumber"];
$warehouseName = $purchasePRData["data"]["purchase_belongs_to_tb_warehouse_info_req"]["warehouseName"];
$prDocNumber = $purchasePRData["data"]["purchase_belongs_to_tb_final_purchase"]["prDocNumber"];
$checkDocType = $purchasePRData["data"]["purchase_belongs_to_tb_final_purchase"]["documentType"];

if ($checkDocType == "PR มาตรฐาน (ZNB1)") {
    $ZNB1 = '<b>&#8730;</b>';
} else {
    $ZNB1 = '&#9634;';
}

if ($checkDocType == "PR จาก MRP (ZNB2)") {
    $ZNB2 = '<b>&#8730;</b>';
} else {
    $ZNB2 = '&#9634;';
}

if ($checkDocType == "PR จากปฏิบัติงาน (ZNB3)") {
    $ZNB3 = '<b>&#8730;</b>';
} else {
    $ZNB3 = '&#9634;';
}

if ($checkDocType == "PR สัญญาล่วงหน้า (ZRV1)") {
    $ZRV1 = '<b>&#8730;</b>';
} else {
    $ZRV1 = '&#9634;';
}

$checkitemCat = $purchasePRData["data"]["purchase_belongs_to_tb_final_purchase"]["itemCategory"];

if ($checkitemCat == "มาตรฐาน ()") {
    $STD = '<b>&#8730;</b>';
} else {
    $STD = '&#9634;';
}

if ($checkitemCat == "การรับเหมาช่วง(L)") {
    $STD1 = '<b>&#8730;</b>';
} else {
    $STD1 = '&#9634;';
}

$str = $purchasePRData["data"]["purchase_belongs_to_tb_final_purchase"]["accountAssignmentCat"];
$delimiter = ',';
$words = explode($delimiter, $str);
$ASS1 = '&#9634;';
$ASS2 = '&#9634;';
$ASS3 = '&#9634;';
$ASS4 = '&#9634;';
$ASS5 = '&#9634;';
$ASS6 = '&#9634;';
$ASS7 = '&#9634;';

foreach ($words as $word) {
    switch ($word) {
        case "พัสดุสำรองคลัง ( )":
            $ASS1 = '<b>&#8730;</b>';
            break;
        case "คชจ.เข้าหน่วยงาน (K)":
            $ASS2 = '<b>&#8730;</b>';
            break;
        case "ทรัพย์สินถาวรพร้อมใช้งาน (Z)":
            $ASS3 = '<b>&#8730;</b>';
            break;
        case "งานจ้างเหมาเบ็ดเสร็จ (P)":
            $ASS4 = '<b>&#8730;</b>';
            break;
        case "พัสดุงานโครงการที่มีแผน (Q)":
            $ASS5 = '<b>&#8730;</b>';
            break;
        case "คชจ.เข้าใบสั่งซ่อม/งานบริการ (F)":
            $ASS6 = '<b>&#8730;</b>';
            break;
        case "งานจ้างเหมาบางส่วน (N)":
            $ASS7 = '<b>&#8730;</b>';
            break;
        default:
            // $ASS1 = '&#9634;';
    }
}

$purchasePRDataCount = count($purchasePRData["data"]["tb_purchase_tb_purchase_list_associate"]);

$tr = "";
$trCount = "";
$trSummary = "";
$trSummary2 = 0;

for ($i = 0; $i < $purchasePRDataCount; $i++) {
    $trCount = $i + 1;
    $trSummary = $purchasePRData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["budgetPrice"];
    $trSummary2 = $trSummary2 + $trSummary;

    $tr .=
        '  <tr>
        <td style="text-align: center;">' . $trCount . '</td>
        <td>' . $purchasePRData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["purchase_list_stuff_belongs_to_mb52"]["equipmentId"] . '</td>
        <td style="text-align: right;">' . $purchasePRData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["equipmentValue"] . '</td>
        <td style="text-align: center;">' . $purchasePRData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["purchase_list_stuff_belongs_to_mb52"]["counter"] . '</td>
        <td style="text-align: right;">' . $purchasePRData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["middlePrice"] . '</td>
        <td style="text-align: right;">' . $purchasePRData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["budgetPrice"] . '</td>
        <td style="text-align: center;">THB</td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan="2" style="text-align: center;">' . $middleJobNumber . '</td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td colspan="14">' . $purchasePRData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["purchase_list_stuff_belongs_to_mb52"]["stuffName"] . '</td>
      </tr>';
}

$trVat = ($trSummary2 * 7) / 100;
$trTotal = $trSummary2 + $trVat;

$toUse = $purchasePRData["data"]["toUse"];
$nameOfApproval = $purchasePRData["data"]["nameOfApproval"];
$positionOfApproval = $purchasePRData["data"]["positionOfApproval"];

$content = '
<style>

.table{
  width:100%;
  table-layout: fixed;
  overflow-wrap: break-word;
  border-collapse: collapse;
  border-style: solid;
  font-family: "sarabun";
  font-size: 14pt;
}

.body{
    font-family: "sarabun";
    font-size: 14pt;
  }

.textfooter{
  font-family: "sarabun";
  font-size: 12pt;
}

</style>
<body>
<table border="1" class="table">
<tbody>
  <tr>
    <td rowspan="2"><div class="pea-logo">' . $img . '</div></td>
    <td colspan="3" rowspan="2" style="text-align: left;" font-size: 12pt;><p>การไฟฟ้าส่วนภูมิภาค</p><p>ใบขอเสนอซื้อ/จ้าง(Purchase Requisition)</p></td>
    <td colspan="3">หน่วยงานผู้ขอเสนอซื้อ/จ้าง ' . $warehouseName . '</td>
    <td colspan="4">รหัสกลุ่มจัดซื้อของผู้ซื้อ/ผู้ว่าจ้าง.............</td>
    <td colspan="3" rowspan="2" style="text-align: left; font-size: 12pt;"><p>เลขที่ใบขอเสนอซื้อ/จ้าง (PR)1101662553</p><p>เลขที่ติดตาม (Tracking No.)</p></td>
  </tr>
  <tr>
    <td colspan="3">วันที่ส่งมอบ</td>
    <td colspan="4">คลัง/สถานที่รับบริการ (รง.).' . $warehouseIdEquipOwner . '.</td>
  </tr>
  <tr>
    <td colspan="4" style="text-align: center; ">
    ประเภทเอกสาร (Document Type)
    </td>
    <td colspan="2" style="text-align: center; font-size: 10pt;">หมวดรายการ (Item Category)</td>
    <td colspan="8" style="text-align: center;">หมวดการกำหนดบัญชี (Account Assignment Category ;A)</td>
  </tr>
  <tr>
    <td colspan="4" style="text-align: center; font-size: 12pt;">
      <div>' . $ZNB1 . ' PR มาตรฐาน (ZNB1)   ' . $ZNB3 . ' PR จากปฏิบัติงาน (ZNB3)</div>
      <div>' . $ZNB2 . ' PR จาก MRP (ZNB2)   ' . $ZRV1 . ' PR สัญญาล่วงหน้า (ZRV1)</div>
    </td>
    <td colspan="2" style="">
      <div> ' . $STD . ' มาตรฐาน ( )</div>
      <div> ' . $STD1 . ' การรับเหมาช่วง(L)</div>
    </td>
    <td colspan="8" style="text-align: justify;text-justify: inter-word;">
    <div> ' . $ASS1 . ' พัสดุสำรองคลัง ( ) ' . $ASS2 . ' คชจ.เข้าหน่วยงาน (K) ' . $ASS3 . ' ทรัพย์สินถาวรพร้อมใช้งาน (Z) ' . $ASS4 . ' งานจ้างเหมาเบ็ดเสร็จ (P)</div>
    <div> ' . $ASS5 . ' พัสดุงานโครงการที่มีแผน (Q) ' . $ASS6 . ' คชจ.เข้าใบสั่งซ่อม/งานบริการ (F) ' . $ASS7 . ' งานจ้างเหมาบางส่วน (N)</div>
    </td>
  </tr>
  <tr>
    <td colspan="14">
    <p style="text-align: left;">บันทึกส่วนหัว (Header Note) จัดซื้อพัสดุ-อุปกรณ์ขาดแคลน จำนวน ' . $purchasePRDataCount . ' รายการ</p>
    <p style="text-align: left;">รวมเป็นเงินทั้งสิ้น ' . $trTotal . ' บาท เพื่อใช้งาน ' . $toUse . '</p>
    </td>
  </tr>
  <tr>
    <td rowspan="3" style="text-align: center;">ลำดับที่</td>
    <td rowspan="3" style="text-align: center;">รหัสวัสดุ</td>
    <td rowspan="3" style="text-align: center;">ปริมาณ</td>
    <td rowspan="3" style="text-align: center;">หน่วย</td>
    <td colspan="3" style="text-align: center;">วงเงินงบประมาณ</td>
    <td rowspan="3" style="text-align: center;">กลุ่มวัสดุ</td>
    <td rowspan="3" style="text-align: center;">รหัสบัญชี G/L</td>
    <td rowspan="3" style="text-align: center;">เงินทุน</td>
    <td colspan="4" style="text-align: center;">หมวดการกำหนดบัญชี</td>
  </tr>
  <tr>
    <td rowspan="2" style="text-align: center;">ต่อหน่วย</td>
    <td rowspan="2" style="text-align: center;">ราคารวม</td>
    <td rowspan="2" style="text-align: center;">สกุลงาน</td>
    <td colspan="2" rowspan="2">
    <p style="text-align: left;">ศูนย์ต้นทุน/องค์ประกอบWBS/</p>
    <p style="text-align: left;">รหัสงบประมาณ(WBS)/รหัสใบสั่ง</p>
    </td>
    <td colspan="2" style="text-align: center;">งานจ้างเหมาบางส่วน</td>
  </tr>
  <tr>
    <td style="text-align: center;">รหัสโครงข่าย</td>
    <td style="text-align: center;">กิจกรรม</td>
  </tr>
  <tr>
    <td colspan="4" style="text-align: center;">รายการ(ข้อความแบบสั้น)</td>
    <td colspan="6" style="text-align: center;">ข้อความรายการ</td>
    <td colspan="2" style="text-align: center;">แหล่งเงินกู้</td>
    <td colspan="2" style="text-align: center;">เลขที่สัญญากู้</td>
  </tr>
  ' . $tr . '
  <tr>
    <td colspan="4">รวม ' . $purchasePRDataCount . ' รายการ รวมมูลค่า ' . $trTotal . ' บาท</td>
    <td colspan="3"></td>
    <td colspan="7"></td>
  </tr>
  <tr>
    <td colspan="5" rowspan="4" style="text-align: center;">
    <br>
    <p style="text-align: left;">ผู้ขอเสนอซื้อ/จ้าง...............................................................</p>
    <p style="text-align: center;">(' . $nameOfApproval . ')</p>
    <p style="text-align: center;">ตำแหน่ง ' . $positionOfApproval . '</p>
    <p style="text-align: center;">วันที่ </p>
    </td>
    <td colspan="5" rowspan="4">
    <br>
    <p style="text-align: left;">ผู้อนุมัติ..................</p>
    <p style="text-align: center;"></p>
    <p style="text-align: center;">ตำแหน่ง </p>
    <p style="text-align: center;">วันที่ </p>
    </td>
    <td colspan="4" rowspan="4">
    <br>
    <p style="text-align: left;">ผู้บันทึก..................</p>
    <p style="text-align: center;"></p>
    <p style="text-align: center;">ตำแหน่ง </p>
    <p style="text-align: center;">วันที่ </p>
    </td>
  </tr>
  <tr>
  </tr>
  <tr>
  </tr>
  <tr>
  </tr>
</tbody>
</table>
<div class="textfooter">
<div><u>หมายเหตุ</u> กรณีรายการเพิมเติมจากแบบฟอร์มนี้ให้กรอกรายกายส่วนที่เพิ่มเติมบนแบบฟอร์ม:ใบขอเสนอซื้อ/จ้าง-เพิ่มเติม (Purchase Requisition-Addition)(PURIF02.2)</div>
<div>กรณีหมวดรายการเป็นการรับเหมาชาวง(L)ต้องกรอกรายละเอียดส่วนประกอบบนแบบฟอร์ม:ใบขอเสนอซื้อ/จ้าง-ส่วนประกอบ (Purchase Requisition-Component)(PURIF02.3)</div>
<div>
</body>
';

$mpdf->useActiveForms = true;
$mpdf->SetTitle('ขออนุมัติโอนพัสดุ');
// $mpdf->AddPage('L'); // Adds a new page in Landscape orientation
$mpdf->WriteHTML($content);
$mpdf->Output();