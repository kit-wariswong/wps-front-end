<?php
// Require composer autoload
date_default_timezone_set("Asia/Bangkok");
require_once '../vendor/autoload.php';
// Create an instance of the class:
// $mpdf = new \Mpdf\Mpdf();

date_default_timezone_set( 'Asia/Bangkok' );
$datetime = date( 'Y-m-d H:i:s' );

// $warehouseReqName = $_POST['warehouseReqName'];
$transferId = $_GET['transferId'];
$warehouseOwnName = $_GET['warehouseOwnName'];
$locationIndex = substr($warehouseOwnName,0,1);

if($locationIndex == "D"){
  $locationNumber = 1;
}else if($locationIndex == "E"){
  $locationNumber = 2;
}else if($locationIndex == "F"){
  $locationNumber = 3;
}

// $transferListItem = $_POST['transferListItem'];

// $transferData = $_POST['transferData'];


$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'http://localhost/api/tb_transfer/select_transfer_by_transferID',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => 'transferID='.$transferId.'&warehouseIdOwner='.$warehouseOwnName,
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/x-www-form-urlencoded'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
// echo $response; json_encode( $arr, JSON_UNESCAPED_UNICODE );

$transferData = json_decode($response, true);


function DateThai($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
	}


function thainumDigit($num){
  return str_replace(array( '0' , '1' , '2' , '3' , '4' , '5' , '6' ,'7' , '8' , '9' ),
  array( "o" , "๑" , "๒" , "๓" , "๔" , "๕" , "๖" , "๗" , "๘" , "๙" ),
  $num);
}


function Convert($amount_number)
{
    $amount_number = number_format($amount_number, 2, ".","");
    $pt = strpos($amount_number , ".");
    $number = $fraction = "";
    if ($pt === false) 
        $number = $amount_number;
    else
    {
        $number = substr($amount_number, 0, $pt);
        $fraction = substr($amount_number, $pt + 1);
    }
    
    $ret = "";
    $baht = ReadNumber ($number);
    if ($baht != "")
        $ret .= $baht . "บาท";
    
    $satang = ReadNumber($fraction);
    if ($satang != "")
        $ret .=  $satang . "สตางค์";
    else 
        $ret .= "ถ้วน";
    return $ret;
}

function ReadNumber($number)
{
    $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
    $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
    $number = $number + 0;
    $ret = "";
    if ($number == 0) return $ret;
    if ($number > 1000000)
    {
        $ret .= ReadNumber(intval($number / 1000000)) . "ล้าน";
        $number = intval(fmod($number, 1000000));
    }
    
    $divider = 100000;
    $pos = 0;
    while($number > 0)
    {
        $d = intval($number / $divider);
        $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" : 
            ((($divider == 10) && ($d == 1)) ? "" :
            ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
        $ret .= ($d ? $position_call[$pos] : "");
        $number = $number % $divider;
        $divider = $divider / 10;
        $pos++;
    }
    return $ret;
}

// เพิ่ม Font ให้กับ mPDF
$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];
$mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/tmp',
    'fontdata' => $fontData + [
            'sarabun' => [ // ส่วนที่ต้องเป็น lower case ครับ
                'R' => 'THSarabunNew.ttf',
                // 'I' => 'THSarabunNewItalic.ttf',
                // 'B' =>  'THSarabunNewBold.ttf',
                // 'BI' => "THSarabunNewBoldItalic.ttf",
            ]
        ],
]);


$wh_req = $transferData["data"][0]["tb_transfer_belongs_to_tb_warehouse_info_req"]["warehouseName"];
$wh_owner = $transferData["data"][0]["tb_transfer_belongs_to_tb_warehouse_info_owner"]["warehouseName"];
$wh_req_id = $transferData["data"][0]["tb_transfer_belongs_to_tb_warehouse_info_req"]["warehouseId"];
$wh_owner_id = $transferData["data"][0]["tb_transfer_belongs_to_tb_warehouse_info_owner"]["warehouseId"];
$transferListCount = count($transferData["data"][0]["tb_transfer_tb_transfer_list_associate"]);
$transferListData = $transferData["data"][0]["tb_transfer_tb_transfer_list_associate"];
$wh_req_location = substr($wh_req_id,0,1);
$wh_own_location = substr($wh_owner_id,0,1);

$tr="";

for($i=0;$i<$transferListCount;$i++) 
{
  if($i==0){
    $tr.=
        '<tr >
          <td class="td4" style="width: 4%" >'.($i+1).'</td> 
          <td class="td4" style="width: 10%">'.$transferData["data"][0]["tb_transfer_tb_transfer_list_associate"][$i]["equipmentId"].'</td> 
          <td class="td3" style="width: 56%">'.$transferData["data"][0]["tb_transfer_tb_transfer_list_associate"][$i]["transfer_list_belongs_to_tb_displayed_safety"]["tb_displayed_safety_belongs_to_tb_all_stuff"]["stuffNameTh"].'</td> 
          <td class="td4" style="width: 10%">'.$transferData["data"][0]["tb_transfer_tb_transfer_list_associate"][$i]["equipmentValue"]." ".$transferData["data"][0]["tb_transfer_tb_transfer_list_associate"][$i]["transfer_list_belongs_to_tb_displayed_safety"]["tb_displayed_safety_belongs_to_tb_all_stuff"]["counter"].'</div></td> 
          <td class="td4" style="width: 10%" rowspan="'.$transferListCount.'">'.$wh_owner.'<br>('.$wh_owner_id.')'.'</td>
          <td class="td4" style="width: 10%" rowspan="'.$transferListCount.'">'.$wh_req.'<br>('.$wh_req_id.')'.'</td>
        </tr>';
  }else{
    $tr.=
        '<tr >
          <td class="td4" style="width: 4%" >'.($i+1).'</td> 
          <td class="td4" style="width: 10%">'.$transferData["data"][0]["tb_transfer_tb_transfer_list_associate"][$i]["equipmentId"].'</td> 
          <td class="td3" style="width: 56%">'.$transferData["data"][0]["tb_transfer_tb_transfer_list_associate"][$i]["transfer_list_belongs_to_tb_displayed_safety"]["tb_displayed_safety_belongs_to_tb_all_stuff"]["stuffNameTh"].'</td> 
          <td class="td4" style="width: 10%">'.$transferData["data"][0]["tb_transfer_tb_transfer_list_associate"][$i]["equipmentValue"]." ".$transferData["data"][0]["tb_transfer_tb_transfer_list_associate"][$i]["transfer_list_belongs_to_tb_displayed_safety"]["tb_displayed_safety_belongs_to_tb_all_stuff"]["counter"].'</div></td> 
        </tr>';
  }
  
}



if($wh_req_location == $wh_own_location){

$img = '<img src="../images/img/logo-pea.jpg" alt="Girl in a jacket" width="117px" height="100px">';
$from = "จาก &nbsp;&nbsp;&nbsp; $wh_owner";
$to = 'ถึง &nbsp;&nbsp;&nbsp;กบญ.ฉ.'.$locationNumber;
$subject = 'ขออนุมัติโอนพัสดุ';

$content1 = "$wh_owner  ขออนุมัติโอนพัสดุ  -  อุปกรณ์   ให้กับ  $wh_req    จำนวน   $transferListCount   รายการ";
$content2 = "ทั้งนี้ เพื่อให้ถูกต้องตามหลักเกณฑ์และวิธีปฏิบัตเกี่ยวกับการจำหน่ายพัสดุของ กฟภ. พ.ศ. ๒๕๖๑ ข้อ ๒.๑ การแต่งตั้งคณะกรรมการสอบหาข้อเท็จจริง
จึงขออนุมัติแต่งตั้งคณะกรรมการสอบหาข้อเท็จจริงกรณีทรัพย์สินชำรุดดังกล่าว เพื่อขออนมัติจำหน่ายทรัพย์สินออกจากบัญชี ดังรายนายต่อไปนี้";
$content = '
<style>
.container{
    font-family: "sarabun";
    font-size: 16pt;
    padding-left: 40px;
}
.container-table{
  font-family: "sarabun";
  font-size: 16pt;
}
.container-table2{
  font-family: "sarabun";
  font-size: 14pt;
  border-collapse: collapse
  border: 1px solid black;
}
.container-table3{
  font-family: "sarabun";
  font-size: 16pt;
  border-collapse: collapse
  border: 1px solid black;
}
.td2{
  border: 1px solid black;
  text-align: center;
  font-weight: bold;
}
.td3{
  border: 1px solid black;
}
.td4{
  border: 1px solid black;
  text-align: center;
}
.td5{
  border: 1px solid black;
  text-align: right;
}
.inp{    
  padding-bottom:0px;
  margin-bottom:0px;
  margin-top:5px;
  padding-top:0px;
  font-family: "sarabun";
  font-size: 16pt;
  width:100%;  
  background-color:#fff;
}

.cus-input{
  border-color : #ffffff;
  background-color: #ffffff;
  font-family: "sarabun";  
  
}

.div-pink{
  color: deeppink;  //สีของข้อความ
  text-align: justify;  //ข้อความอยู่ตรงกลาง
}
.topic-title{
  text-align: left;
  padding-left: 48px;
  padding-top: 10px;
}
.first-line{
  padding-left: 100px;
  text-align: justify;
}
.first-line1{
  padding-left: 50px;
  text-align: justify;
}
.thrid-line{
    padding-left: 120px;
    text-align: justify;
  }
  .thrid-line-1{
    padding-left: 140px;
    text-align: justify;
  }
  .thrid-line-2{
    padding-left: 160px;
    text-align: justify;
  }
.sign1{
  padding-left: 0px;
  text-align: center;
}
.approve{
    padding-left: 30px;
    text-align: justify;
  }
.div-right{
  text-align: right;
  font-size: 14pt;
}
p{
    text-align: justify;
}
h1{
    text-align: center;
}
pea-logo{
  text-align: left;
}

div{
  text-align: justify;
}



</style>
<div class="container" style="width: 100%">
<div class="pea-logo">'.$img.'</div>
<table class="container-table" style="width: 80%" border="0">
  <tr >
    <td>'.$from.'</td> 
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> 
    <td>'.$to.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr >
    <td>ที่ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> 
    <td>วันที่ </td>
</tr>
</table>
  เรื่อง &nbsp;&nbsp;&nbsp;'.$subject.'   
<br>
 เรียน &nbsp;&nbsp;&nbsp;อก.บญ.ฉ.'.$locationNumber.'
<br>
<div class="topic-title">'.$content1.'</b></div>

<table class="container-table3" style="width: 100%" >
  <tr >
    <td class="td2" style="width: 4%" >ลำดับ</td> 
    <td class="td2" style="width: 10%">รหัสพัสดุ</td> 
    <td class="td2" style="width: 56%">รายละเอียด</td> 
    <td class="td2" style="width: 10%">จำนวน</div></td>
    <td class="td2" style="width: 10%">ผู้โอน</div></td> 
    <td class="td2" style="width: 10%">ผู้รับโอน</div></td>  
  </tr>'.
  $tr
 
.'</table>
<div class="topic-title">สำหรับการขนส่ง  ผู้รับ  จะเป็นผู้ดำเนินการขนส่งเอง  </div>
<div class="topic-title">จึงเรียนมาเพื่อพิจารณาอนุมัติ และโปรดแจ้ง  ผบพ.กบญ.ฉ.'.$locationNumber.'  เพื่อ สร้าง ใบสั่งโอน (STO)   ต่อไป</div>

';
}else{
  
$img = '<img src="../images/img/logo-pea.jpg" alt="Girl in a jacket" width="117px" height="100px">';
$from = "จาก &nbsp;&nbsp;&nbsp; $wh_owner";
$to = 'ถึง &nbsp;&nbsp;&nbsp;กบญ.ฉ.'.$locationNumber;
$subject = 'ขออนุมัติโอนพัสดุ';

$content1 = "$wh_owner  ขออนุมัติโอนพัสดุ  -  อุปกรณ์   ให้กับ  $wh_req    จำนวน   $transferListCount   รายการ เพื่อใช้งานกับ $wh_req";
$content2 = "และ กฟฟ. ในสังกัด ซึ่งได้ติดต่อประสานงานแล้ว สามารถโอนได้ โดย $wh_req จะเป็นผู้ดำเนินการขนส่ง ตามรายละเอียดดีงนี้.-";
$content = '
<style>
.container{
    font-family: "sarabun";
    font-size: 16pt;
    padding-left: 40px;
}
.container-table{
  font-family: "sarabun";
  font-size: 16pt;
}
.container-table2{
  font-family: "sarabun";
  font-size: 14pt;
  border-collapse: collapse
  border: 1px solid black;
}
.container-table3{
  font-family: "sarabun";
  font-size: 16pt;
  border-collapse: collapse
  border: 1px solid black;
}
.td2{
  border: 1px solid black;
  text-align: center;
  font-weight: bold;
}
.td3{
  border: 1px solid black;
}
.td4{
  border: 1px solid black;
  text-align: center;
}
.td5{
  border: 1px solid black;
  text-align: right;
}
.inp{    
  padding-bottom:0px;
  margin-bottom:0px;
  margin-top:5px;
  padding-top:0px;
  font-family: "sarabun";
  font-size: 16pt;
  width:100%;  
  background-color:#fff;
}

.cus-input{
  border-color : #ffffff;
  background-color: #ffffff;
  font-family: "sarabun";  
  
}

.div-pink{
  color: deeppink;  //สีของข้อความ
  text-align: justify;  //ข้อความอยู่ตรงกลาง
}
.topic-title{
  text-align: left;
  padding-left: 48px;
  padding-top: 10px;
}
.first-line{
  padding-left: 100px;
  text-align: justify;
}
.first-line1{
  padding-left: 50px;
  text-align: justify;
}
.thrid-line{
    padding-left: 120px;
    text-align: justify;
  }
  .thrid-line-1{
    padding-left: 140px;
    text-align: justify;
  }
  .thrid-line-2{
    padding-left: 160px;
    text-align: justify;
  }
.sign1{
  padding-left: 0px;
  text-align: center;
}
.approve{
    padding-left: 30px;
    text-align: justify;
  }
.div-right{
  text-align: right;
  font-size: 14pt;
}
p{
    text-align: justify;
}
h1{
    text-align: center;
}
pea-logo{
  text-align: left;
}

div{
  text-align: justify;
}



</style>
<div class="container" style="width: 100%">
<div class="pea-logo">'.$img.'</div>
<table class="container-table" style="width: 80%" border="0">
  <tr >
    <td>'.$from.'</td> 
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> 
    <td>'.$to.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr >
    <td>ที่ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> 
    <td>วันที่ </td>
</tr>
</table>
  เรื่อง &nbsp;&nbsp;&nbsp;'.$subject.'   
<br>
 เรียน &nbsp;&nbsp;&nbsp;อก.บญ.ฉ.'.$locationNumber.'
<br>
<div class="topic-title">'.$content1.'</b></div>
<div >'.$content2.'</b></div>

<table class="container-table3" style="width: 100%" >
  <tr >
    <td class="td2" style="width: 4%" >ลำดับ</td> 
    <td class="td2" style="width: 10%">รหัสพัสดุ</td> 
    <td class="td2" style="width: 56%">รายละเอียด</td> 
    <td class="td2" style="width: 10%">จำนวน</div></td>
    <td class="td2" style="width: 10%">ผู้โอน</div></td> 
    <td class="td2" style="width: 10%">ผู้รับโอน</div></td>  
  </tr>'.
  $tr
 
.'</table>
<div class="topic-title">สำหรับการขนส่ง  ผู้รับ  จะเป็นผู้ดำเนินการขนส่งเอง  </div>
<div class="topic-title">จึงเรียนมาเพื่อพิจารณาอนุมัติ และโปรดแจ้ง  ผบพ.กบญ.ฉ.'.$locationNumber.'  เพื่อ สร้าง ใบสั่งโอน (STO)   ต่อไป</div>

';
}
$footer = '
<pagefooter name="MyFooter1"
  content-right="โปรแกรม WPS Ref. transfer ID : '.$transferId.' หน้าที่ {PAGENO}/{nbpg}" footer-style="font-family: sarabun; font-size: 10pt;
   font-style: italic; color: #000000;" />

<setpageheader name="MyHeader1" value="on" show-this-page="1" />
<setpagefooter name="MyFooter1" value="on" />
';
$mpdf->useActiveForms = true;
$mpdf->SetTitle('ขออนุมัติโอนพัสดุ');
$mpdf->WriteHTML($footer);
$mpdf->WriteHTML($content);
$mpdf->Output();
