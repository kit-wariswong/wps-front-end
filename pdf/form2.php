<?php

require_once '../vendor/autoload.php';
$purchaseId = $_GET['purchaseId'];

function Convert($amount_number)
{
    $amount_number = number_format($amount_number, 2, ".", "");
    $pt = strpos($amount_number, ".");
    $number = $fraction = "";
    if ($pt === false) {
        $number = $amount_number;
    } else {
        $number = substr($amount_number, 0, $pt);
        $fraction = substr($amount_number, $pt + 1);
    }

    $ret = "";
    $baht = ReadNumber($number);
    if ($baht != "") {
        $ret .= $baht . "บาท";
    }

    $satang = ReadNumber($fraction);
    if ($satang != "") {
        $ret .= $satang . "สตางค์";
    } else {
        $ret .= "ถ้วน";
    }

    return $ret;
}

function ReadNumber($number)
{
    $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
    $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
    $number = $number + 0;
    $ret = "";
    if ($number == 0) {
        return $ret;
    }

    if ($number > 1000000) {
        $ret .= ReadNumber(intval($number / 1000000)) . "ล้าน";
        $number = intval(fmod($number, 1000000));
    }

    $divider = 100000;
    $pos = 0;
    while ($number > 0) {
        $d = intval($number / $divider);
        $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
        ((($divider == 10) && ($d == 1)) ? "" :
            ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
        $ret .= ($d ? $position_call[$pos] : "");
        $number = $number % $divider;
        $divider = $divider / 10;
        $pos++;
    }
    return $ret;
}
// echo $purchaseId;

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => 'http://localhost:3000/tb_purchase/get_purchase_by_id?purchase_id=' . $purchaseId,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
));

$response = curl_exec($curl);
curl_close($curl);

$purchaseData = json_decode($response, true);
$purchaseData["data"]["purchaseId"];
// $purchaseData["data"]["approvalNumber"];

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];
$mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/pdf/tmp',
    'fontdata' => $fontData + [
        'sarabun' => [ // ส่วนที่ต้องเป็น lower case ครับ
            'R' => 'THSarabunNew.ttf',
            // 'I' => 'THSarabunNewItalic.ttf',
            // 'B' =>  'THSarabunNewBold.ttf',
            // 'BI' => "THSarabunNewBoldItalic.ttf",
        ],
    ],
]);

$img = '<img src="../images/img/logo-pea.jpg" alt="Girl in a jacket" width="117px" height="100px"></img>';
$namePea = $purchaseData["data"]["purchase_belongs_to_tb_warehouse_info_req"]["warehouseName"];
$purchaseDataCount = count($purchaseData["data"]["tb_purchase_tb_purchase_list_associate"]);
$toUse = $purchaseData["data"]["toUse"];
$tr = "";
$trCount = "";
$trSummary = "";
$trSummary2 = 0;

for ($i = 0; $i < $purchaseDataCount; $i++) {
    $trCount = $i + 1;

    $trSummary = $purchaseData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["budgetPrice"];
    $trSummary2 = $trSummary2 + $trSummary;
    $tr .=
        '<tr style="height: 18px;">
        <td style="width: 11.2037%; height: 18px; text-align: center;">' . $trCount . '</td>
        <td style="width: 13.7963%; height: 18px; text-align: center;">' . $purchaseData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["purchase_list_stuff_belongs_to_mb52"]["equipmentId"] . '</td>
        <td style="width: 12.5%; height: 18px; ">' . $purchaseData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["purchase_list_stuff_belongs_to_mb52"]["stuffName"] . '</td>
        <td style="width: 12.5%; height: 18px;">' . $purchaseData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["specNo"] . '</td>
        <td style="width: 12.5%; height: 18px; text-align: center;">' . $purchaseData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["purchase_list_stuff_belongs_to_mb52"]["counter"] . '</td>
        <td style="width: 12.5%; height: 18px; text-align: center;">' . $purchaseData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["equipmentValue"] . '</td>
        <td style="width: 12.5%; height: 18px; text-align: center;">' . $purchaseData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["middlePrice"] . '</td>
        <td style="width: 12.5%; height: 18px; text-align: center;">' . $purchaseData["data"]["tb_purchase_tb_purchase_list_associate"][$i]["budgetPrice"] . '</td>
        </tr>';
}

$trVat = ($trSummary2 * 7) / 100;
$trTotal = $trSummary2 + $trVat;

$textfooter = "";
$textpriceRecordNumberDocument = $purchaseData["data"]["priceRecordNumberDocument"];

if ($textpriceRecordNumberDocument != "") {
    $textfooter = '&nbsp;&nbsp; แหล่งที่มาของราคากลางในการจัดซื้อครั้งนี้เป็นราคาที่คณะกรรมการกำหนดราคามาตรฐานและราคากลางพัสดุ ' . $purchaseData["data"]["priceRecordNumberDocument"] . '';
}

$content = '<style>
.container{
    font-family: "sarabun";
    font-size: 16pt;
    padding-left: 40px;
}
.container-table{
  font-family: "sarabun";
  font-size: 16pt;
}
.container-table2{
  font-family: "sarabun";
  font-size: 14pt;
  border-collapse: collapse
  border: 1px solid black;
}
.container-table3{
  font-family: "sarabun";
  font-size: 16pt;
  border-collapse: collapse
  border: 1px solid black;
}
.td2{
  border: 1px solid black;
  text-align: center;
  font-weight: bold;
}
.td3{
  border: 1px solid black;
}
.td4{
  border: 1px solid black;
  text-align: center;
}
.td5{
  border: 1px solid black;
  text-align: right;
}
.inp{
  padding-bottom:0px;
  margin-bottom:0px;
  margin-top:5px;
  padding-top:0px;
  font-family: "sarabun";
  font-size: 16pt;
  width:100%;
  background-color:#fff;
}

.cus-input{
  border-color : #ffffff;
  background-color: #ffffff;
  font-family: "sarabun";

}

.div-pink{
  color: deeppink;  //สีของข้อความ
  text-align: justify;  //ข้อความอยู่ตรงกลาง
}
.topic-title{
  text-align: left;
  padding-left: 48px;
  padding-top: 10px;
}
.first-line{
  padding-left: 100px;
  text-align: justify;
}
.first-line1{
  padding-left: 50px;
  text-align: justify;
}
.thrid-line{
    padding-left: 120px;
    text-align: justify;
  }
  .thrid-line-1{
    padding-left: 140px;
    text-align: justify;
  }
  .thrid-line-2{
    padding-left: 160px;
    text-align: justify;
  }
.sign1{
  padding-left: 0px;
  text-align: center;
}
.approve{
    padding-left: 30px;
    text-align: justify;
  }
.div-right{
  text-align: right;
  font-size: 14pt;
}
p{
    text-align: justify;
}
h1{
    text-align: center;
}
pea-logo{
  text-align: left;
}

div{
  text-align: justify;
}



</style>
<div class="container" style="width: 100%">
<div class="pea-logo">' . $img . '</div>
<table class="container-table" style="border-collapse: collapse; width: 100%; height: 118px;" border="0">
<tbody>
<tr style="height: 18px;">
<td style="width: 5%; height: 10px; text-align: center;">จาก</td>
<td style="width: 11.2037%; height: 10px;">' . $namePea . '</td>
<td style="width: 11.2037%; height: 10px; text-align: center;">ถึง</td>
<td style="width: 11.2037%; height: 10px; text-align: center;">' . $namePea . '</td>
</tr>
<tr style="height: 18px;">
<td style="width: 5%; height: 10px; text-align: center;">เลขที่</td>
<td style="width: 11.2037%; height: 10px; text-align: center;"></td>
<td style="width: 11.2037%; height: 10px; text-align: center;">วันที่</td>
<td style="width: 11.2037%; height: 10px; text-align: center;"></td>
</tr>
<tr height: 18px;>
<td style="width: 5%; height: 10px; text-align: center;">เรื่อง</td>
<td style="width: 40%; height: 10px; text-align: center;">ขออนุมัติให้ดำเนินการจัดซื้อ พัสดุ-อุปกรณ์ที่ขาดแคลน</td>
<td style="width: 11.2037%; height: 10px; text-align: center;"></td>
</tr>
</tbody>
</table>
<hr>
<div>เรียน  ผจก.' . $namePea . '</div>
<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>1.เรื่องเดิม</u></div>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.1 ตามที่ ' . $namePea . ' ได้รับการจัดสรรงบประมาณ ตาม อุนมัติเลขที่ ' . $purchaseData["data"]["approvalNumber"] . ' งบประมาณจำนวน ' . $purchaseData["data"]["approvalBudget"] . ' บาท (ซึ่งไม่รวมภาษีมูลค่าเพิ่ม)  อ้างอิงตามหมายเลขงานกลางที่ ' . $purchaseData["data"]["middleJobNumber"] . ' ให้จัดซื้อ พัสดุ-อุปกรณ์เพื่อใช้งาน ' . $toUse . '
</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1.2 ผคพ.' . $namePea . ' ได้ตรวจสอบปริมาณความต้องการใช้พัสดุ-อุปกรณ์ &nbsp;งาน&nbsp; เร่งรัดขยายเขตการไฟฟ้าให้พื้นที่ทำกินทางเกษตร ในระบบ SAP T-code(ZCN52N) ฉ.วันที่ 20 กรกฎาคม 2565 (ดังเอกสารความต้องการใช้งานพัสดุ-อุปกรณ์ จากระบบ SAP แนบท้ายนี้) พร้อมรายละเอียดคุณลักษณะ(Specification)ของแต่ละรายการ ตามข้อกำหนด กฟภ.(เอกสาร สเปคแนบท้าย)
</p>
<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>3.ข้อเสนอแนะ</u></div>
<p>&nbsp;&nbsp;เพื่อการจัดซื้อให้มีพัสดุสำหรับใช้งานงบผู้ใช้ไฟได้อย่างรวดเร็วทันกับความต้องการใช้งานมีประสิทธิภาพประสิทธิผล ในการดำเนินงานของ ของ กฟภ. และเพื่อป้องกันความเสียหายที่อาจจะเกิดขึ้นต่อ กฟภ. จึงเห็นควรอนุมัติให้ ผบห. เป็น ผู้ดำเนินการจัดซื้อ พัสดุ-อุปกรณ์ โดยให้จัดซื้อตาม สเปคมาตรฐาน ที่ กฟภ.กำหนด จำนวน ' . $trCount . ' รายการ วงเงินงบประมาณทั้งสิ้น ' . $trTotal . ' บาท  (' . ReadNumber($trTotal) . ') (รามภาษีมูลค่าเพิ่ม) โดยมีรายละเอียดดีงนี้ (เอกสาร สเปคแนบท้าย)</p>
<br>
<br>
<br>
<table class="container-table" style="border-collapse: collapse; width: 100%; height: 118px;" border="1">
<tbody>
<tr style="height: 10px;">
<td style="width: 11.2037%; height: 10px; text-align: center;">ที่</td>
<td style="width: 13.7963%; height: 10px; text-align: center;">รหัสพัสดุ</td>
<td style="width: 12.5%; height: 10px; text-align: center;">รายการ (ชื่อพัสดุ)</td>
<td style="width: 12.5%; height: 10px; text-align: center;">
<p>คุณลักษณะ</p>
<p>Specification</p>
</td>
<td style="width: 12.5%; height: 10px; text-align: center;">หน่วยนับ</td>
<td style="width: 12.5%; height: 10px; text-align: center;">จำนวน</td>
<td style="width: 12.5%; height: 10px; text-align: center;">
<p>ราคากลาง</p>
<p>ต่อหน่วย</p>
</td>
<td style="width: 12.5%; height: 10px; text-align: center;">เงินงบประมาณ</td>
' . $tr . '
</tbody>
<tfoot>
<tr style="height: 18px;">
<td style="height: 18px; text-align: center;" colspan="7">มูลค่ารวมเป็นเงิน</td>
<td style="width: 12.5%; height: 18px; text-align: center;">' . $trSummary2 . '</td>
</tr>
<tr style="height: 18px;">
<td style="height: 18px; text-align: center;" colspan="7">ภาษีมูลค่าเพิ่ม 7%</td>
<td style="width: 12.5%; height: 18px; text-align: center;">' . $trVat . '</td>
</tr>
<tr style="height: 18px;">
<td style="width: 11.2037%; height: 18px; text-align: center;" colspan="7">มูลค่ารวมภาษีมูลค่าเพิ่มเป็นเงินทั้งสิ้น</td>
<td style="width: 12.5%; height: 18px; text-align: center;">' . $trTotal . '</td>
</tr>
</tfoot>
</table>
<p></p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; จึงเรียนมาเพื่อโปรดพิจารณาอนุมัติ และแจ้ง ผบห ดำเนินการต่อไป</p>
<p>เรียน หผ.คพ.,หผ.บห.' . $namePea . '</p>
<div> - อนุมัติให้ดำเนินการจัดซื้อตามระเบียบต่อไป&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; นายสามชัย กิ่งมาลา</div>
<div> - เรียน ผบห.' . $namePea . ' ดำเนินการโดยด่วนต่อไป&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; หผ.คพ.เดชอุดม(ชั้น2)</div>
</div>
';

$mpdf->useActiveForms = true;
$mpdf->SetTitle('ขออนุมัติโอนพัสดุ');
// $mpdf->WriteHTML($footer);CSS Script goes here.
$mpdf->WriteHTML($content);
$mpdf->Output();