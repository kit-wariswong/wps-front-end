<!doctype html>
<html lang="en">

<head>
    <?php
    include('../include/head.php');
    ?>
    <title>เข้าสู่ระบบวางแผนและจัดการพัสดุ (งบผู้ใช้ไฟ) แบบเบ็ดเสร็จ</title>
</head>

<body>
    <div class="container-fluid">
        <div class="row flex-nowrap bg-dark">
            <?php
            include('../include/sidemenu.php');
            ?>

            <div class="col p-0 mt-1 bg-light">
                <?php
                include('../include/navbar.php');
                ?>
                <!-- content start-->
                <div class="p-4">
                    <h3>แจ้งความต้องการพัสดุ</h3>
                    <div class="mt-4">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="notiType1" value="1" checked>
                            <label class="form-check-label" for="notiType1">แจ้งเตือนรายอุปกรณ์</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="notiType2" value="2">
                            <label class="form-check-label" for="notiType2">แจ้งเตือนชุดเซ็ต</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="notiType3" value="3">
                            <label class="form-check-label" for="notiType3">แจ้งเตือนราย WBS</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="notiType4" value="4">
                            <label class="form-check-label" for="notiType4">แจ้งเตือนงานที่ชำระเงินแล้ว</label>
                        </div>
                    </div>
                    

                    <div id="notice_list">

                    </div>
                    <!-- <button id="loading" class="btn btn-primary" type="button" disabled style="display: none">
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        <span class="visually-hidden">Loading...</span>
                    </button> -->
                </div>
                <!-- content end-->
            </div>

            <!-- modal loading -->
            <!-- <div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" id="loading">
                <div class="modal-dialog modal-dialog-centered justify-content-center">
                    
                        <button class="btn btn-primary btn-lg p-5" type="button" disabled>
                            <span class="spinner-border" style="width: 5rem; height: 5rem;" role="status" aria-hidden="true"></span>
                            <p class="fs-2 pt-3">โหลดข้อมูล . . .</p>
                        </button>
                   
                </div>
            </div> -->
            <!-- modal loading -->


            <!-- modal confirm noti acknowled -->
            <div class="modal fade" id="confirm-whAck" data-bs-keyboard="false" tabindex="-1" aria-labelledby="confirm-whAck-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content text-center">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <h3 class="mt-4">รับทราบการแจ้งเตือนจาก ผบค.</h3>
                        <!-- <h5 id="itemSelect"class="mt-4"></h5> -->
                    </div>
                    <div class="modal-footer">
                        <button id="btn-confirm-whAck" type="button" class="btn btn-success me-auto col-5 ms-4">ยืนยัน</button>
                        <button id="btn-cancel-whAck" type="button" class="btn btn-secondary col-5 me-4" data-bs-dismiss="modal">ยกเลิก</button>
                    </div>
                    </div>
                </div>
            </div>
            <!-- modal modal confirm noti acknowled -->

        </div>
    </div>

</body>

</html>


<!-- Login check -->
<script>
    // let data = JSON.parse(localStorage.getItem("data"))
    console.log(data)
    if (data === null) {
        window.location.replace("./login.php");
        if (data.LoginResponse.ResponseCode != "WSV0000") {
            window.location.replace("./login.php");
        }
    }

</script>
<!-- Login check -->


<!-- remove and add class active -->
<script>
    $(".nav-link").even().removeClass("active");
    $("#wh_noti_from_cs").even().addClass("active");
</script>


<!-- select PEA code -->
<!-- <script>
    let settings = {
        "url": "/api/tb_estimate_set/get_all_set",
        "method": "GET",
        "timeout": 0,
    };

    $.ajax(settings).done(function(response) {
        let item = response.data;
        console.log(item)
        let select = document.getElementById("itemSet");
        for (let i = 0; i < item.length; i++) {
            let option = document.createElement("option")
            option.text = item[i].setName;
            option.value = item[i].setId;
            select.appendChild(option, select.length - 1);
        }
    });
</script> -->

<!-- function create card notice to waerhouse -->
<script>
    function create_notice_card(){
        function subTable_set ( d ) {
            // `d` is the original data object for the row
            let notice_item_count = d.tb_cs_warehouse_step1_belongs_to_tb_estimate_set[0].estimate_belongs_to_tb_estimate_list.length
                let notice_item_table=""
                for(j=0;j<notice_item_count;j++){
                    if(d.tb_cs_warehouse_step1_belongs_to_tb_estimate_set[0].estimate_belongs_to_tb_estimate_list.length != 0){
                        notice_item_table+=
                        `<tr>
                            <td class="text-center">${j+1}</td>
                            <td>${d.tb_cs_warehouse_step1_belongs_to_tb_estimate_set[0].estimate_belongs_to_tb_estimate_list[j].equipmentId}</td>
                            <td>${d.tb_cs_warehouse_step1_belongs_to_tb_estimate_set[0].estimate_belongs_to_tb_estimate_list[j].estimate_list_item_belongs_tb_all_stuff.stuffNameTh}</td>
                            <td>จำนวน</td>
                            <td>${(d.tb_cs_warehouse_step1_belongs_to_tb_estimate_set[0].estimate_belongs_to_tb_estimate_list[j].equipmentQuantity * d.needValue)}</td>
                            <td>${d.tb_cs_warehouse_step1_belongs_to_tb_estimate_set[0].estimate_belongs_to_tb_estimate_list[j].estimate_list_item_belongs_tb_all_stuff.counter}</td>
                        </tr>`
                    }
                }

            return `<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                        
                        

                            ${notice_item_table}

                        
                    </table>`;
        }

        $("div#notice_list").empty()
        $("div#notice_list").append(`
            <table id="notice_list_Set" class="display" style="width:100%">
                
            </table>
        `)
        
        let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        $.fn.dataTable.moment( 'DD/MM/YYYY เวลา HH:mm น.' ); // sort by moment datetime
        let table = $('#notice_list_Set').DataTable( {
            ajax: {
                url: '/api/cs_warehouse_step1/select_by_wh_id',
                type: "POST",
                data:{
                    warehouseId: Bacode0,
                    whAck:0
                }
            },
            lengthChange: false,
            destroy: true,
            columns: [    
                {
                    className:      'dt-control',
                    orderable:      false,
                    data:           null,
                    defaultContent: ''
                },            
                {
                    title:"step1ID",
                    data:"csWarehouseS1Id"
                },
                { 
                    title:"ชื่องาน",
                    data: "jobName" 
                },            
                { 
                    title:"ชุดอุปกรณ์",
                    data: "tb_cs_warehouse_step1_belongs_to_tb_estimate_set.0.setName" 
                },
                { 
                    title:"จำนวน",
                    data: "needValue" 
                },
                { 
                    title:"วันที่แจ้งเตือน",
                    data: null,
                    render : function(data, type, row) {
                        return moment(new Date(data.createdAt), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                        // return moment(new Date(data.createdAt), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY")
                    }
                },
                {
                    title:"",
                    data: null ,
                    className:"text-center",
                    render : function(data, type, row) { 
                        return `<button class="btn btn-success rounded-5" id="btn-whAck-set">รับทราบ</button>`;          
                    }
                }, 
                
                
                
            ],
            columnDefs: [
                        {
                            targets:[1],
                            visible: false,
                        }
                     ],
            order: [[5, 'desc']]
        } );
        
        // Add event listener for opening and closing details
        $('#notice_list_Set tbody').on('click', 'td.dt-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( subTable_set(row.data()) ).show();
                tr.addClass('shown');
            }
        } );

        $('#notice_list_Set').on('click', 'button#btn-whAck-set', function () {
            let currentRow = $(this).closest('tr')
            let data = $('#notice_list_Set').DataTable().row(currentRow).data();
            let csWarehouseS1Id = data["csWarehouseS1Id"]

            // let confirmwhAck = new bootstrap.Modal(document.getElementById('confirm-whAck'))
            // confirmwhAck.show()   
            $('#confirm-whAck').modal('toggle');

            $('#btn-confirm-whAck').on('click', function () {
            let settings = {
                    "url": "/api/cs_warehouse_step1/edit_cswarehouse_step1_status/",
                    "method": "POST",                       
                    "data": {
                        csWarehouseS1Id: csWarehouseS1Id,
                        whAck: 1,
                    },
                };
                // console.log(settings)
                $.ajax(settings).done(function (response_transfer_list) {
                    $('#confirm-whAck').modal('toggle');

                    // currentRow = ""
                    // data = ""
                    // csWarehouseS1Id = ""
                    // settings = ""

                    create_notice_nonSet()
                })
            })

            // $('#confirm-whAck').on('click', 'button#btn-cancel-whAck', function () {
            //     $('#confirm-whAck').modal('toggle');

            //     currentRow = ""
            //     data = ""
            //     csWarehouseS1Id = ""
            //     settings = ""
            // })
        })

    }


    // Create notice WBS จ่ายเงินแล้ว 
    function create_notice_wbs_paid() {
        // $('#loading').modal({
        //     backdrop: 'static',
        //     keyboard: false  // to prevent closing with Esc button (if you want this too)
        // })
        // $("#loading").modal('show')
        // ดึงข้อมูลจาก Api
        // let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"

        // let settings = {
        //     "url": "/api/cs_warehouse_step3/select_by_wh_id",
        //     "method": "POST",            
        //     "data": {
        //         "warehouseId": Bacode0,
        //         "notWhAck" : "2"
        //     },
        // };

        // $.ajax(settings).done(function (response_transfer) {
        // $("#loading").modal('hide')

        // แสดงข้อมูลใน column
        function subTable_set(d) {
            // `d` is the original data object for the row
            let notice_item_count = d.total
            // console.log(notice_item_count)
            let notice_item_table = ""
            for(j=0;j<notice_item_count;j++){
                if(d.data[j].zmb25_tb_all_stuff_associate.length != 0){
                    notice_item_table+=
                        `<tr>
                            <td class="text-center">${j+1}</td>
                            <td>${d.data[j].zmb25_tb_all_stuff_associate[0].stuffId}</td>
                            <td>${d.data[j].zmb25_tb_all_stuff_associate[0].stuffNameTh}</td>
                            <td>จำนวน ${(d.data[j].qty_needed).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} ${d.data[j].bun}</td>
                        </tr>`
                }
                
            }

            return_table = `<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                                ${notice_item_table}
                            </table>`;

            // return `<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                    
                    

            //             ${notice_item_table}

                    
            //     </table>`;

            return return_table
        }

        $("div#notice_list").empty()
        $("div#notice_list").append(`
            <table id="notice_list_Set" class="display" style="width:100%">
            
            </table>
        `)

        
        let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        $.fn.dataTable.moment( 'DD/MM/YYYY' ); // sort by moment datetime
        let table = $('#notice_list_Set').DataTable({
            ajax: {
                url: '/api/cs_warehouse_step3/select_by_wh_id_only',
                type: "POST",
                data: {
                    warehouseId: Bacode0,
                    notWhAck : "2"
                }
            },
            // data : response_transfer.data,
            lengthChange: false,
            destroy: true,
            columns: [{
                    className: 'dt-control',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                {
                    title:"step3ID",
                    data:"csWarehouseS3Id"
                },
                {
                    title: "รหัสคลัง",
                    data: "tb_cs_warehouse_step3_belongs_to_zcsr181.officeId"
                },
                {
                    title: "ชื่อลูกค้า",
                    data: "tb_cs_warehouse_step3_belongs_to_zcsr181.requestorName"
                },
                {
                    title: "ที่อยู่ลูกค้า",
                    data: "tb_cs_warehouse_step3_belongs_to_zcsr181.requestorAddress"
                },
                {
                    title: "วันที่ชำระเงิน",
                    // data: "tb_cs_warehouse_step3_belongs_to_zcsr181.debtSrDate",
                    data: null,
                    render : function(data, type, row) {
                        return (data["tb_cs_warehouse_step3_belongs_to_zcsr181"]["debtSrDate"]).replaceAll('.','/')
                    }
                },
                {
                    title: "หมายเลขงาน",
                    data: "tb_cs_warehouse_step3_belongs_to_zcsr181.wbsNumber",
                },
                {
                    title:"",
                    data: null ,
                    className:"text-center",
                    render : function(data, type, row) { 
                        if(data["whAck"]=="0"){
                            return `<button class="btn btn-success rounded-5" id="btn-whAck-step3" value="1">รับทราบ</button>`;
                        }else if(data["whAck"]=="1"){
                            return `<button class="btn btn-primary rounded-5" id="btn-whAck-step3" value="2">ดำเนินการแล้ว</button>`;
                        }                                      
                    }
                }, 
            ],
            columnDefs: [
                {
                    targets:[1],
                    visible: false,
                }
            ],
            order: [[ 5, "desc" ]],
        });

        // เปิด column
        $('#notice_list_Set tbody').on('click', 'td.dt-control', function() {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                const d = row.data()
                const wbsNumber = d.tb_cs_warehouse_step3_belongs_to_zcsr181.wbsNumber
                let settings = {
                "url": "/api/cs_warehouse_step3/select_25data_by_181_id",
                "method": "POST",            
                "data": {
                    "wbsNumber": wbsNumber,
                    },
                };

                $.ajax(settings).done(function (response) {
                    row.child(subTable_set(response)).show();
                    tr.addClass('shown');
                })

                // row.child(subTable_set(row.data())).show();
                // tr.addClass('shown');
            }
        });

        $('#notice_list_Set').on('click', 'button#btn-whAck-step3', function () {
            let currentRow = $(this).closest('tr')
            let data = $('#notice_list_Set').DataTable().row(currentRow).data();
            let csWarehouseS3Id = data["csWarehouseS3Id"]
            let whAckValue = $(this).val()
            // console.log(csWarehouseS3Id)
            // console.log(whAckValue)

            // let confirmwhAck = new bootstrap.Modal(document.getElementById('confirm-whAck'))
            // confirmwhAck.show()
            $('#confirm-whAck').modal('toggle');
            

            $('#btn-confirm-whAck').on('click',  function () {
            
            let settings = {
                    "url": "/api/cs_warehouse_step3/edit_cswarehouse_step3_status/",
                    "method": "POST",                       
                    "data": {
                        csWarehouseS3Id: csWarehouseS3Id,
                        whAck: whAckValue,
                    },
                };
                // console.log(settings)
                $.ajax(settings).done(function (response_transfer_list) {
                    $('#confirm-whAck').modal('toggle');

                    // currentRow = ""
                    // data = ""
                    // csWarehouseS1Id = ""
                    // settings = ""

                    create_notice_wbs_paid()
                })
            })

            // $('#confirm-whAck').on('click', 'button#btn-cancel-whAck', function () {
            //     $('#confirm-whAck').modal('toggle');

            //     currentRow = ""
            //     data = ""
            //     csWarehouseS1Id = ""
            //     settings = ""
            // })
        })
    // })

    }

    // create noti non set
    function create_notice_nonSet(){
        let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        $.fn.dataTable.moment( 'DD/MM/YYYY เวลา HH:mm น.' ); // sort by moment datetime
        $("div#notice_list").empty()
        $("div#notice_list").append(`
            <table id="notice_list_nonSet" class="cell-border " style="width:100%">
                
            </table>
        `)

        $('#notice_list_nonSet').DataTable( {
            ajax: {
                url: '/api/cs_warehouse_step1/select_by_wh_id_nonset',
                type: "POST",
                data:{
                    warehouseId: Bacode0,
                    whAck:0
                }
            },
            lengthChange: false,
            destroy: true,
            columns: 
            [
                {
                    title:"step1ID",
                    data:"csWarehouseS1Id"
                },
                {
                    title:"รหัสพัสดุ",
                    data:"equipmentId"
                },
                {
                    title:"รายการ",
                    data:"tb_cs_warehouse_step1_belongs_to_tb_all_stuff.stuffNameTh",
                    className:"text-break"
                },            
                {
                    title:"หน่วยนับ",
                    data:"tb_cs_warehouse_step1_belongs_to_tb_all_stuff.counter",
                    className:"text-center"                
                },
                {
                    title:"จำนวน",
                    data: "needValue",
                    className:"text-end",
                    // render : function(data, type, row) { 
                    //     if(data["tb_mb52_fetch_belongs_to_tb_displayed_safety"] === null){
                            
                    //         return "-"
                    //     }
                    //     else{
                    //         return (data["tb_mb52_fetch_belongs_to_tb_displayed_safety"]["safety_0021"])

                    //     }              
                    // }
                },
                {
                    title:"วันที่แจ้งเตือน",
                    data: null,
                    className:"text-break",
                    render : function(data, type, row) { 
                        return moment(new Date(data["createdAt"]), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")      
                        // return moment(new Date(data["createdAt"]), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY")      
                    }
                },
                {
                    title:"หมายเหตุ",
                    data:"jobName",
                    className:"text-break"
                },
                {
                    title:"เลขที่คำร้อง",
                    data: null ,
                    className:"text-center",
                    render : function(data, type, row) { 
                        if(data["orderId"] ==""){
                            return "-"
                        }else{
                            return data["orderId"]
                        }              
                    }
                },            
                {
                    title:"",
                    data: null ,
                    className:"text-center",
                    render : function(data, type, row) { 
                         return `<button class="btn btn-success rounded-5" id="btn-whAck-equipment">รับทราบ</button>`;
                    }
                }, 
            ],
            order : [[ 5, "desc" ]],
            columnDefs: [
                        {
                            targets:[0],
                            visible: false,
                        }
                     ]
        } );

        $('#notice_list_nonSet').on('click', 'button#btn-whAck-equipment', function () {
            let currentRow = $(this).closest('tr')
            let data = $('#notice_list_nonSet').DataTable().row(currentRow).data();
            let csWarehouseS1Id = data["csWarehouseS1Id"]

            // let confirmwhAck = new bootstrap.Modal(document.getElementById('confirm-whAck'))
            // confirmwhAck.show()   
            $('#confirm-whAck').modal('toggle');

            $('#btn-confirm-whAck').on('click', function () {
            let settings = {
                    "url": "/api/cs_warehouse_step1/edit_cswarehouse_step1_status/",
                    "method": "POST",                       
                    "data": {
                        csWarehouseS1Id: csWarehouseS1Id,
                        whAck: 1,
                    },
                };
                // console.log(settings)
                $.ajax(settings).done(function (response_transfer_list) {
                    $('#confirm-whAck').modal('toggle');

                    // currentRow = ""
                    // data = ""
                    // csWarehouseS1Id = ""
                    // settings = ""

                    create_notice_nonSet()
                })
            })

            // $('#confirm-whAck').on('click', 'button#btn-cancel-whAck', function () {
            //     $('#confirm-whAck').modal('toggle');

            //     currentRow = ""
            //     data = ""
            //     csWarehouseS1Id = ""
            //     settings = ""
            // })
        })
    
    }

    function create_notice_wbs(){
        // แสดงข้อมูลใน column
        function subTable_set(d) {
            // `d` is the original data object for the row
            let notice_item_count = d.tb_cs_warehouse_step2_belongs_to_zcsr181[0].zcsr181_zmb25_associate.length
            // console.log(d)
            let notice_item_table = ""
            for(j=0;j<notice_item_count;j++){
                if(d.tb_cs_warehouse_step2_belongs_to_zcsr181[0].zcsr181_zmb25_associate.length != 0){
                    notice_item_table+=
                        `<tr>
                            <td class="text-center">${j+1}</td>
                            <td>${d.tb_cs_warehouse_step2_belongs_to_zcsr181[0].zcsr181_zmb25_associate[j].zmb25_tb_all_stuff_associate[0].stuffId}</td>
                            <td>${d.tb_cs_warehouse_step2_belongs_to_zcsr181[0].zcsr181_zmb25_associate[j].zmb25_tb_all_stuff_associate[0].stuffNameTh}</td>
                            <td>จำนวน ${(d.tb_cs_warehouse_step2_belongs_to_zcsr181[0].zcsr181_zmb25_associate[j].qty_needed).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} ${d.tb_cs_warehouse_step2_belongs_to_zcsr181[0].zcsr181_zmb25_associate[j].bun}</td>
                        </tr>`
                }
                
            }

            return `<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                    
                    

                        ${notice_item_table}

                    
                </table>`;
        }

        $("div#notice_list").empty()
        $("div#notice_list").append(`
            <table id="notice_list_wbs" class="display" style="width:100%">
                
            </table>
        `)
        
        let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        $.fn.dataTable.moment( 'DD/MM/YYYY เวลา HH:mm น.' ); // sort by moment datetime
        let table = $('#notice_list_wbs').DataTable( {
            ajax: {
                url: '/api/cs_warehouse_step2/select_by_wh_id',
                type: "POST",
                data:{
                    warehouseId: Bacode0,
                    whAck: 0,
                }
            },
            lengthChange: false,
            destroy: true,
            columns: [    
                {
                    className:      'dt-control',
                    orderable:      false,
                    data:           null,
                    defaultContent: ''
                },
                {
                    title:"step2ID",
                    data:"csWarehouseS2Id"
                },            
                { 
                    title:"หมายเลข WBS",
                    data: "wbsNumber" 
                },            
                { 
                    title:"ชื่องาน",
                    data: "tb_cs_warehouse_step2_belongs_to_zcsr181.0.requestType" 
                },
                { 
                    title:"ชื่อลูกค้า",
                    data: "tb_cs_warehouse_step2_belongs_to_zcsr181.0.requestorName" 
                },
                { 
                    title:"วันที่แจ้งเตือน",
                    data: null,
                    render : function(data, type, row) {
                        return (data["tb_cs_warehouse_step2_belongs_to_zcsr181"][0]["dateRecievedRequest"]).replaceAll('.','/')
                    }
                },
                {
                    title:"",
                    data: null ,
                    className:"text-center",
                    render : function(data, type, row) { 
                        return `<button class="btn btn-success rounded-5" id="btn-whAck-wbs">รับทราบ</button>`;              
                    }
                }, 
                
                
                
            ],
            columnDefs: [
                {
                    targets:[1],
                    visible: false,
                }
            ],
            order: [[ 5, "desc" ]],

        } );

        $('#notice_list_wbs').on('click', 'button#btn-whAck-wbs', function () {
            let currentRow = $(this).closest('tr')
            let data = $('#notice_list_wbs').DataTable().row(currentRow).data();
            let csWarehouseS2Id = data["csWarehouseS2Id"]

            // let confirmwhAck = new bootstrap.Modal(document.getElementById('confirm-whAck'))
            // confirmwhAck.show()  
            $('#confirm-whAck').modal('toggle'); 

            $('#btn-confirm-whAck').on('click', function () {
            let settings = {
                    "url": "/api/cs_warehouse_step2/edit_cswarehouse_step2_status/",
                    "method": "POST",                       
                    "data": {
                        csWarehouseS2Id: csWarehouseS2Id,
                        whAck: 1,
                    },
                };
                // console.log(settings)
                $.ajax(settings).done(function (response_transfer_list) {
                    $('#confirm-whAck').modal('toggle');

                    // currentRow = ""
                    // data = ""
                    // csWarehouseS2Id = ""
                    // settings = ""

                    create_notice_wbs()
                })
            })

            // $('#confirm-whAck').on('click', 'button#btn-cancel-whAck', function () {
            //     $('#confirm-whAck').modal('toggle');

            //     currentRow = ""
            //     data = ""
            //     csWarehouseS2Id = ""
            //     settings = ""
            // })
        })
        
        // Add event listener for opening and closing details
        $('#notice_list_wbs tbody').on('click', 'td.dt-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( subTable_set(row.data()) ).show();
                tr.addClass('shown');
            }
        } );

    }

</script>



<!-- call create_notice_card function -->
<script>
    create_notice_nonSet()
</script>

<!-- get noti transfer count number -->
<script>
    $(document).ready(function() {
        let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        getNotiTransferCount(Bacode0)

        function getNotiTransferCount(Bacode0) {
            let settings = {
                "url": "/api/tb_transfer/select_count_transfer/",
                "method": "POST",
                "data": {
                    "warehouseIdRequester": Bacode0,
                    "transferStatus": 1
                },
            };
            $.ajax(settings).done(function(response_transfer_count) {
                let NotiTransferCount = response_transfer_count.total
                if (NotiTransferCount != 0) {
                    $("#transferNotice").html(NotiTransferCount)
                    $("#transferNotice").attr("style", "display:block")
                    $("#side_noti_transfer_req").html(NotiTransferCount)
                    $("#side_noti_transfer_req").show()
                } else {
                    $("#transferNotice").attr("style", "display:none")
                    $("#side_noti_transfer_req").hide()
                }
            });
        }
    })
</script>

<!-- get noti transfer count number for owner -->
<script>
    $(document).ready(function() {
        let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        getNotiTransferCountOwner(Bacode0)

        function getNotiTransferCountOwner(Bacode0) {
            let settings = {
                "url": "/api/tb_transfer/select_count_transfer_owner/",
                "method": "POST",
                "data": {
                    "warehouseIdOwner": Bacode0,
                    "transferStatus": 2
                },
            };
            $.ajax(settings).done(function(response_transfer_count_owner) {
                let NotiTransferCountOwner = response_transfer_count_owner.total
                if (NotiTransferCountOwner != 0) {
                    $("#side_noti_transfer_own").html(NotiTransferCountOwner)
                    $("#side_noti_transfer_own").show()
                } else {
                    $("#side_noti_transfer_own").hide()
                }
            });
        }
    })
</script>

<!-- get cs noti count number -->
<script>
    $(document).ready(function() {
        let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        getNotiTransferCountOwner(Bacode0)

        function getNotiTransferCountOwner(Bacode0) {
            let settings = {
                "url": "/api/cs_warehouse_step1/select_by_wh_id_count/",
                "method": "POST",
                "data": {
                    "warehouseId": Bacode0
                },
            };
            $.ajax(settings).done(function(response_cs_notice_count) {
                let csNotiCount = response_cs_notice_count.total
                if (csNotiCount != 0) {
                    $("#Notice").html(csNotiCount)
                    $("#Notice").attr("style", "display:block")
                    $("#side_noti_cs").html(csNotiCount)
                    $("#side_noti_cs").show()
                } else {
                    $("#Notice").attr("style", "display:none")
                    $("#side_noti_cs").hide()
                }
            });
        }
    })
</script>

<!-- get cs warehouse step3 -->
<script>
    $(document).ready(function() {
        let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        getNotiTransferCountOwner(Bacode0)

        function getNotiTransferCountOwner(Bacode0) {
            let settings = {
                "url": "/api/cs_warehouse_step3/select_by_wh_id",
                "method": "POST",
                data: {
                    warehouseId: Bacode0
                },
            };
            $.ajax(settings).done(function(response_cs_notice_count) {
                let csNotiCount = response_cs_notice_count.total
                if (csNotiCount != 0) {
                    $("#Notice").html(csNotiCount)
                    $("#Notice").attr("style", "display:block")
                    $("#side_noti_cs").html(csNotiCount)
                    $("#side_noti_cs").show()
                } else {
                    $("#Notice").attr("style", "display:none")
                    $("#side_noti_cs").hide()
                }
            });
        }
    })
</script>

<script>
    $('input[type=radio][name=inlineRadioOptions]').on('change', function() {
        switch ($(this).val()) {
            case '1':
                create_notice_nonSet()
                break;
            case '2':
                create_notice_card()
                break;
            case '3':
                create_notice_wbs()
                break;
            case '4':
                create_notice_wbs_paid()
                break;
        }
    });
</script>

<script>
    $('#confirm-whAck').on('hide.bs.modal', function(event)
    {
        let btnNotiToWearhouse3 = $(this).find('#btn-confirm-whAck');
        btnNotiToWearhouse3.unbind("click");
        // console.log("clear")
    });
</script>