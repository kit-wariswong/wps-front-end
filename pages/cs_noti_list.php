<!doctype html>
<html lang="en">
<head>
    <?php 
        include ('../include/head.php');
    ?>
    <title>เข้าสู่ระบบวางแผนและจัดการพัสดุ (งบผู้ใช้ไฟ) แบบเบ็ดเสร็จ</title>
</head>

<body>  
    <div class="container-fluid">        
        <div class="row flex-nowrap bg-dark">           
            <?php 
                include ('../include/sidemenu.php');
            ?>            
                      
            <div class="col p-0 mt-1 bg-light" >
            <?php 
                include ('../include/navbar.php');
            ?>            
            <!-- content start-->  
            <div class= "p-4">
                <h3>ประวัติการแจ้งเตือนคลังพัสดุ</h3>                
                <div class="mt-4">    
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="notiType1" value="1" checked>
                        <label class="form-check-label" for="notiType1">แจ้งเตือนรายอุปกรณ์</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="notiType2" value="2">
                        <label class="form-check-label" for="notiType2">แจ้งเตือนชุดเซ็ต</label>
                    </div>               
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="notiType3" value="3">
                        <label class="form-check-label" for="notiType3">แจ้งเตือนราย WBS</label>
                    </div>   
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="notiType4" value="4">
                        <label class="form-check-label" for="notiType4">แจ้งเตือนงานที่ชำระเงินแล้ว</label>
                    </div>
                </div>
                <div id="notice_list">

                </div>
            </div>
            <!-- content end-->
            </div>
            
        </div>
         <!-- modal loading -->
         <div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" id="loading">
            <div class="modal-dialog modal-dialog-centered justify-content-center">
                <!-- <div class="d-flex align-items-center justify-content-center"> -->
                    <button class="btn btn-primary btn-lg p-5" type="button" disabled>
                        <span class="spinner-border" style="width: 5rem; height: 5rem;" role="status" aria-hidden="true"></span>
                        <p class="fs-2 pt-3">โหลดข้อมูล . . .</p>
                    </button>
                <!-- </div> -->
            </div>
        </div>
        <!-- modal loading -->
    </div>
    
</body>
</html>


<!-- Login check -->
<script>
    // let data = JSON.parse(localStorage.getItem("data"))
    console.log(data)
    if(data===null)
    {
        window.location.replace("./login.php");
        if(data.LoginResponse.ResponseCode != "WSV0000"){
            window.location.replace("./login.php");
        } 
    }
       
</script>
<!-- Login check -->

<!-- Hide Notice button on navbar -->
<script>
    $("#transferNoticeButton").attr("style", "display:none");
    $("#NoticeButton").attr("style", "display:none");
</script>



<!-- remove and add class active -->
<script>
    $(".nav-link").even().removeClass("active");
    // $("#cs_noti_to_wh").even().addClass("active");
</script>

<!-- function create card notice to warehouse -->
<script>

function create_notice_card(){
    function subTable_set ( d ) {
        // `d` is the original data object for the row
        console.log(d)
        let notice_item_count = d.tb_cs_warehouse_step1_belongs_to_tb_estimate_set[0].estimate_belongs_to_tb_estimate_list.length
            let notice_item_table=""
            for(j=0;j<notice_item_count;j++){
                if(d.tb_cs_warehouse_step1_belongs_to_tb_estimate_set[0].estimate_belongs_to_tb_estimate_list.length != 0){
                    notice_item_table+=
                    `<tr>
                        <td class="text-center">${j+1}</td>
                        <td>${d.tb_cs_warehouse_step1_belongs_to_tb_estimate_set[0].estimate_belongs_to_tb_estimate_list[j].equipmentId}</td>
                        <td>${d.tb_cs_warehouse_step1_belongs_to_tb_estimate_set[0].estimate_belongs_to_tb_estimate_list[j].estimate_list_item_belongs_tb_all_stuff.stuffNameTh}</td>
                        <td>จำนวน</td>
                        <td>${(d.tb_cs_warehouse_step1_belongs_to_tb_estimate_set[0].estimate_belongs_to_tb_estimate_list[j].equipmentQuantity * d.needValue)}</td>
                        <td>${d.tb_cs_warehouse_step1_belongs_to_tb_estimate_set[0].estimate_belongs_to_tb_estimate_list[j].estimate_list_item_belongs_tb_all_stuff.counter}</td>
                    </tr>`
                }
            }

        return `<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                    
                    

                        ${notice_item_table}

                    
                </table>`;
    }

    $("div#notice_list").empty()
    $("div#notice_list").append(`
        <table id="notice_list_Set" class="display" style="width:100%">
            
        </table>
    `)
    
    let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
    $.fn.dataTable.moment( 'DD/MM/YYYY เวลา HH:mm น.' );
    let table = $('#notice_list_Set').DataTable( {
        ajax: {
            url: '/api/cs_warehouse_step1/select_by_wh_id',
            type: "POST",
            data:{
                warehouseId: Bacode0,
                whAck:""
            }
        },
        lengthChange: false,
        destroy: true,
        columns: [    
            {
                className:      'dt-control',
                orderable:      false,
                data:           null,
                defaultContent: ''
            },            
            { 
                title:"ชื่องาน",
                data: "jobName" 
            },            
            { 
                title:"ชุดอุปกรณ์",
                data: "tb_cs_warehouse_step1_belongs_to_tb_estimate_set.0.setName" 
            },
            { 
                title:"จำนวน",
                data: "needValue" 
            },
            { 
                title:"วันที่แจ้งเตือน",
                data: null,
                render : function(data, type, row) {
                    return moment(new Date(data.createdAt), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                }
            },
            {
                title:"วันที่คลังพัสดุรับทราบ",
                data: null ,
                className:"text-center",
                render : function(data, type, row) { 
                    if(data["whAck"]=="0"){
                        return "-"
                    }else{
                        return moment(new Date(data["updatedAt"]), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                    }               
                }
            }, 
            
            
            
        ],
        order : [[ 4, "desc" ]]
    } );
    
    // Add event listener for opening and closing details
    $('#notice_list_Set tbody').on('click', 'td.dt-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( subTable_set(row.data()) ).show();
            tr.addClass('shown');
        }
    } );

}

function create_notice_nonSet(){
    let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
    $.fn.dataTable.moment( 'DD/MM/YYYY เวลา HH:mm น.' );
    $("div#notice_list").empty()
    $("div#notice_list").append(`
        <table id="notice_list_nonSet" class="cell-border " style="width:100%">
            
        </table>
    `)

    $('#notice_list_nonSet').DataTable( {
        ajax: {
            url: '/api/cs_warehouse_step1/select_by_wh_id_nonset',
            type: "POST",
            data:{
                warehouseId: Bacode0,
                whAck:""
            }
        },
        lengthChange: false,
        destroy: true,
        columns: 
        [
            {
                title:"รหัสพัสดุ",
                data:"equipmentId"
            },
            {
                title:"รายการ",
                data:"tb_cs_warehouse_step1_belongs_to_tb_all_stuff.stuffNameTh",
                className:"text-break"
            },            
            {
                title:"หน่วยนับ",
                data:"tb_cs_warehouse_step1_belongs_to_tb_all_stuff.counter",
                className:"text-center"                
            },
            {
                title:"จำนวน",
                data: "needValue",
                className:"text-end",
                // render : function(data, type, row) { 
                //     if(data["tb_mb52_fetch_belongs_to_tb_displayed_safety"] === null){
                        
                //         return "-"
                //     }
                //     else{
                //         return (data["tb_mb52_fetch_belongs_to_tb_displayed_safety"]["safety_0021"])

                //     }              
                // }
            },
            {
                title:"วันที่แจ้งเตือน",
                data: null,
                className:"text-break",
                render : function(data, type, row) { 
                    return moment(new Date(data["createdAt"]), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")          
                }
            },
            {
                title:"หมายเหตุ",
                data:"jobName",
                className:"text-break"
            },
            {
                title:"เลขที่คำร้อง",
                data: null ,
                className:"text-center",
                render : function(data, type, row) { 
                    if(data["orderId"] ==""){
                        return "-"
                    }else{
                        return data["orderId"]
                    }              
                }
            },            
            {
                title:"วันที่คลังพัสดุรับทราบ",
                data: null ,
                className:"text-center",
                render : function(data, type, row) { 
                    if(data["whAck"]=="0"){
                        return "-"
                    }else{
                        return moment(new Date(data["updatedAt"]), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                    }               
                }
            }, 
        ],
        order : [[ 4, "desc" ]]
        // columnDefs: [
        //             {
        //                 render: function (data, type, full, meta) {
        //                     return "<div class='text-break'>" + data + "</div>";
        //                 },
        //                 targets: 2
        //             }
        //          ]
    } );
   
}


function create_notice_wbs(){
    function subTable_set(d) {
            // `d` is the original data object for the row
            let notice_item_count = d.tb_cs_warehouse_step2_belongs_to_zcsr181[0].zcsr181_zmb25_associate.length
            console.log(notice_item_count)
            let notice_item_table = ""
            for(j=0;j<notice_item_count;j++){
                if(d.tb_cs_warehouse_step2_belongs_to_zcsr181[0].zcsr181_zmb25_associate.length != 0){
                    notice_item_table+=
                        `<tr>
                            <td class="text-center">${j+1}</td>
                            <td>${d.tb_cs_warehouse_step2_belongs_to_zcsr181[0].zcsr181_zmb25_associate[j].zmb25_tb_all_stuff_associate[0].stuffId}</td>
                            <td>${d.tb_cs_warehouse_step2_belongs_to_zcsr181[0].zcsr181_zmb25_associate[j].zmb25_tb_all_stuff_associate[0].stuffNameTh}</td>
                            <td>จำนวน ${(d.tb_cs_warehouse_step2_belongs_to_zcsr181[0].zcsr181_zmb25_associate[j].qty_needed).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} ${d.tb_cs_warehouse_step2_belongs_to_zcsr181[0].zcsr181_zmb25_associate[j].bun}</td>
                        </tr>`
                }
                
            }

            return `<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                    
                    

                        ${notice_item_table}

                    
                </table>`;
        }

    $("div#notice_list").empty()
    $("div#notice_list").append(`
        <table id="notice_list_wbs" class="display" style="width:100%">
            
        </table>
    `)
    
    let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
    $.fn.dataTable.moment( 'DD/MM/YYYY' );
    let table = $('#notice_list_wbs').DataTable( {
        ajax: {
            url: '/api/cs_warehouse_step2/select_by_wh_id',
            type: "POST",
            data:{
                warehouseId: Bacode0,
                whAck:""
            }
        },
        lengthChange: false,
        destroy: true,
        columns: [    
            {
                className:      'dt-control',
                orderable:      false,
                data:           null,
                defaultContent: ''
            },            
            { 
                title:"หมายเลข WBS",
                data: "wbsNumber" 
            },            
            { 
                title:"ชื่องาน",
                data: "tb_cs_warehouse_step2_belongs_to_zcsr181.0.requestType" 
            },
            { 
                title:"ชื่อลูกค้า",
                data: "tb_cs_warehouse_step2_belongs_to_zcsr181.0.requestorName" 
            },
            { 
                title:"วันที่แจ้งเตือน",
                data: null,
                render : function(data, type, row) {
                    return (data["tb_cs_warehouse_step2_belongs_to_zcsr181"][0]["dateRecievedRequest"]).replaceAll('.','/')
                }
            },
            {
                title:"วันที่คลังพัสดุรับทราบ",
                data: null ,
                className:"text-center",
                render : function(data, type, row) { 
                    if(data["whAck"]=="0"){
                        return "-"
                    }else{
                        return moment(new Date(data["updatedAt"]), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                    }               
                }
            }, 
            
            
            
        ],
        order : [[ 4, "desc" ]]

    } );
    
    // Add event listener for opening and closing details
    $('#notice_list_wbs tbody').on('click', 'td.dt-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( subTable_set(row.data()) ).show();
            tr.addClass('shown');
        }
    } );

}

// Create notice WBS จ่ายเงินแล้ว 
function create_notice_wbs_paid() {
        // $('#loading').modal({
        //     backdrop: 'static',
        //     keyboard: false  // to prevent closing with Esc button (if you want this too)
        // })
        // $("#loading").modal('show')
        // ดึงข้อมูลจาก Api
        // let Peacode0 = data.userLoginDataResponce.Peacode.substring(0, 3) + "0"

        // let settings = {
        //     "url": "/api/cs_warehouse_step3/select_by_wh_id_only",
        //     "method": "POST",            
        //     "data": {
        //         "warehouseId": Peacode0,
        //         "notWhAck" : "3"
        //     },
        // };

        // $.ajax(settings).done(function (response_transfer) {
        // $("#loading").modal('hide')

        // แสดงข้อมูลใน column
        function subTable_set(d) {
            // `d` is the original data object for the row
            let notice_item_count = d.total
            console.log(d)
            let notice_item_table = ""
            for(j=0;j<notice_item_count;j++){
                if(d.data[j].zmb25_tb_all_stuff_associate.length != 0){
                    notice_item_table+=
                        `<tr>
                            <td class="text-center">${j+1}</td>
                            <td>${d.data[j].zmb25_tb_all_stuff_associate[0].stuffId}</td>
                            <td>${d.data[j].zmb25_tb_all_stuff_associate[0].stuffNameTh}</td>
                            <td>จำนวน ${(d.data[j].qty_needed).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")} ${d.data[j].bun}</td>
                        </tr>`
                }
                
            }

            return `<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                    
                    

                        ${notice_item_table}

                    
                </table>`;
        }

        $("div#notice_list").empty()
        $("div#notice_list").append(`
            <table id="notice_list_wbs_paid" class="display" style="width:100%">
            
            </table>
        `)

        // ดึงข้อมูลจาก Api
        // let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        let Peacode0 = data.userLoginDataResponce.Peacode.substring(0, 3) + "0"
        $.fn.dataTable.moment( 'DD/MM/YYYY' ); // sort by moment datetime
        let table = $('#notice_list_wbs_paid').DataTable({
            ajax: {
                url: '/api/cs_warehouse_step3/select_by_wh_id_only',
                type: "POST",
                data: {
                    warehouseId: Peacode0,
                    notWhAck : "3"
                }
            },
            // data : response_transfer.data,
            lengthChange: false,
            destroy: true,
            columns: [{
                    className: 'dt-control',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                {
                    title: "รหัสคลัง",
                    data: "tb_cs_warehouse_step3_belongs_to_zcsr181.officeId"
                },
                {
                    title: "ชื่อลูกค้า",
                    data: "tb_cs_warehouse_step3_belongs_to_zcsr181.requestorName"
                },
                {
                    title: "ที่อยู่ลูกค้า",
                    data: "tb_cs_warehouse_step3_belongs_to_zcsr181.requestorAddress"
                },
                {
                    title: "วันที่ชำระเงิน",
                    data: null,
                    render : function(data, type, row) {
                    return (data["tb_cs_warehouse_step3_belongs_to_zcsr181"]["debtSrDate"]).replaceAll('.','/')
                }
                },
                {
                    title: "หมายเลขงาน",
                    data: "tb_cs_warehouse_step3_belongs_to_zcsr181.wbsNumber",
                },
                {
                    title: "วันที่คลังพัสดุรับทราบ",
                    data: null ,
                    className:"text-center",
                    render : function(data, type, row) { 
                        if(data["whAck"]=="0"){
                            return "-"
                        }else if(data["whAck"]=="1"){
                            return moment(new Date(data["updatedAt"]), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                        }else if(data["whAck"]=="2"){
                            return moment(new Date(data["updatedAt"]), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                        }            
                    }
                },
                {
                    title:"สถานะของงาน",
                    data: null ,
                    className:"text-center",
                    render : function(data, type, row) { 
                        if(data["whAck"]=="0"){
                            return `<span class="text-black rounded-3 px-3" style="background-color:#b7b7b7">ยังไม่รับทราบ</span>`;
                        }else if(data["whAck"]=="1"){
                            return `<span class="text-black rounded-3 px-3" style="background-color:#93dbb7">รับทราบ</span>`;
                        }else if(data["whAck"]=="2"){
                            return `<span class="text-black rounded-3 px-3" style="background-color:#9ffbf9">ดำเนินการแล้ว</span>`;
                        }                                      
                    }
                }, 
            ],
            columnDefs: [
                {
                    targets:[1],
                    visible: false,
                }
            ],
            order : [[ 4, "desc" ]]
        });

        // เปิด column
        $('#notice_list_wbs_paid tbody').on('click', 'td.dt-control', function() {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                const d = row.data()
                const wbsNumber = d.tb_cs_warehouse_step3_belongs_to_zcsr181.wbsNumber
                let settings = {
                "url": "/api/cs_warehouse_step3/select_25data_by_181_id",
                "method": "POST",            
                "data": {
                    "wbsNumber": wbsNumber,
                    },
                };

                $.ajax(settings).done(function (response) {
                    row.child(subTable_set(response)).show();
                    tr.addClass('shown');
                })
            }
        });
    // })

    }

</script>




<!-- call create_notice_card function -->
<script>

$(document).ready(function () {

    create_notice_nonSet()

})

$('input[type=radio][name=inlineRadioOptions]').on('change', function() {
  switch ($(this).val()) {
    case '1':
        create_notice_nonSet()
        break;
    case '2':
        create_notice_card()
        break;
    case '3':
        create_notice_wbs()
        break;
    case '4':
        create_notice_wbs_paid()
        break;
  }
});
    
</script>

