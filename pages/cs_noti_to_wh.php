<!doctype html>
<html lang="en">
<head>
    <?php 
        include ('../include/head.php');
    ?>
    <title>เข้าสู่ระบบวางแผนและจัดการพัสดุ (งบผู้ใช้ไฟ) แบบเบ็ดเสร็จ</title>
</head>

<body>  
    <div class="container-fluid">        
        <div class="row flex-nowrap bg-dark">           
            <?php 
                include ('../include/sidemenu.php');
            ?>            
                      
            <div class="col p-0 mt-1 bg-light" >
            <?php 
                include ('../include/navbar.php');
            ?>            
            <!-- content start-->  
            <div class= "p-4">
                <h3>แจ้งความต้องการพัสดุ</h3>                
                <div class="mt-4">                    
                    <span class="text-black">ระบุรายการ :</span>                   
                    <button id="add_notice_to_wh" class="btn btn-light bg-dark bg-opacity-10 rounded-pill">เลือกชุดเซ็ต</button>
                    <button id="add_notice_wbs_to_wh" class="btn btn-light bg-dark bg-opacity-10 rounded-pill">แจ้งเตือนราย WBS</button>                    
                </div>
                <div id="notice_list">

                </div>
                <table id="example" class="display" style="width:100%">

                </table>
            </div>
            <!-- content end-->
            </div>

            <!-- modal alert noti equipment set -->
            <div class="modal fade" id="add_notice_to_wh_form" data-bs-keyboard="false" tabindex="-1" aria-labelledby="alert-transfer-equipment-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="mb-3">
                                    <label for="itemSet" class="col-form-label">เลือกชุดเซ็ต <span class="text-danger">*</span></label>
                                    <select id="itemSet" class="form-select" aria-label="Default select example">
                                        <!-- <option selected disabled>เลือก Set</option> -->
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="notice_eq_value" class="col-form-label">จำนวน <span class="text-danger">*</span></label>
                                    <input type="number" class="form-control" id="notice_eq_value" value="1" min="0">
                                </div>
                                <div class="mb-3">
                                    <label for="notice_customer_name" class="col-form-label">หมายเลขคำร้อง (เลข 12)</label>
                                    <input type="text" class="form-control" id="notice_jobOrder_number">
                                </div>
                                <div class="mb-3">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="jobType0" value="0" checked>
                                        <label class="form-check-label" for="jobType0">เอกชน</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="jobType1" value="1">
                                        <label class="form-check-label" for="jobType1">ราชการ</label>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="notice_job_description" class="col-form-label">หมายเหตุ <span class="text-danger">*</span></label>
                                    <textarea  class="form-control" id="notice_job_description" rows="3"></textarea>
                                </div>                                
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button id="add_notice_to_wh_form_submit" type="button" class="btn btn-danger">แจ้งความต้องการ</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal alert noti equipment set -->

            <!-- modal alert noti WBS -->
            <div class="modal fade" id="add_notice_to_wh_form_wbs" data-bs-keyboard="false" tabindex="-1" aria-labelledby="alert-noti-wbs" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <h4>แจ้งเตือนราย WBS</h4>  
                                <div class="row">
                                    <div class="mb-3 col-9">
                                        <input type="text" class="form-control" placeholder="หมายเลข WBS" id="inputWbsNumber">
                                    </div>
                                    <div class="col-3">
                                        <button type="button" class="btn btn-secondary" id="searchWBS"><span class="m-3">ค้าหา</span></button>   
                                    </div>
                                </div>
                                <div class="mt-1" id="showWbsData" style="display:none">
                                    <div><span id="ShowWbsNumber"></span></div> 
                                    <div><span id="ShowRequestNumber"></span></div>      
                                    <div><span id="ShowRequestType"></span></div>
                                    <div><span id="requestorName"></span></div> 
                                </div>
                                                             
                                
                                <!-- <div class="mb-3">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="jobType0" value="0" checked>
                                        <label class="form-check-label" for="jobType0">เอกชน</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="jobType1" value="1">
                                        <label class="form-check-label" for="jobType1">ราชการ</label>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="notice_job_description" class="col-form-label">หมายเหตุ <span class="text-danger">*</span></label>
                                    <textarea  class="form-control" id="notice_job_description" rows="3"></textarea>
                                </div>                                 -->
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button id="add_notice_wbs_to_wh_form_submit" type="button" class="btn btn-danger" style="display:none">แจ้งความต้องการ</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal alert transfer WBS -->

            <!-- modal alert notice to warehouse -->
            <div class="modal fade" id="alert_notice_to_wh_form" data-bs-keyboard="false" tabindex="-1" aria-labelledby="alert-transfer-equipment-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content text-center">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <image src="../images/icons/accept64.png"></image>
                        <h3 class="mt-4">แจ้งเตือนไปยังคลังพัสดุสำเร็จ</h3>
                        <h5 id="itemSelect"class="mt-4"></h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ปิด</button>
                    </div>
                    </div>
                </div>
            </div>
            <!-- modal alert notice to warehouse -->

        </div>
    </div>
    
</body>
</html>


<!-- Login check -->
<script>
    // let data = JSON.parse(localStorage.getItem("data"))
    console.log(data)
    if(data===null)
    {
        window.location.replace("./login.php");
        if(data.LoginResponse.ResponseCode != "WSV0000"){
            window.location.replace("./login.php");
        } 
    }
       
</script>
<!-- Login check -->

<!-- Hide Notice button on navbar -->
<script>
    $("#transferNoticeButton").attr("style", "display:none");
    $("#NoticeButton").attr("style", "display:none");
</script>



<!-- remove and add class active -->
<script>
    $(".nav-link").even().removeClass("active");
    $("#cs_noti_to_wh").even().addClass("active");
</script>

<!-- Click button add notice to wh -->
<script>
    // $(document).ready(function(){
        $('#add_notice_to_wh').on('click', function () {
            // let alertTransferEquipment = new bootstrap.Modal(document.getElementById('add_notice_to_wh_form'))
            // alertTransferEquipment.show()
            $('#add_notice_to_wh_form').modal('toggle');  

            //add_notice_to_wh_form_submit
            $('#add_notice_to_wh_form_submit').on('click', function () {
                let notice_eq_value = $("#notice_eq_value").val() // *
                let notice_job_description = $("#notice_job_description").val() // *      
                let notice_jobOrder_number = $("#notice_jobOrder_number").val()               
                let itemSet = $("#itemSet").val()
                let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
                let jobType = $( "input:checked" ).val()
                

                if(notice_eq_value != "" && notice_eq_value != "0" && notice_job_description != ""){
                    // console.log(notice_job_name +" "+ notice_customer_name + " " +itemSet)
                    let settings = {
                        "url": "/api/cs_warehouse_step1/add_cswarehouse_step1/",
                        "method": "POST",            
                        "data": {
                            "setId":itemSet,
                            "jobName": notice_job_description,
                            "userId": data.userLoginDataResponce.Username,
                            "warehouseId": Bacode0,
                            "orderId": notice_jobOrder_number,
                            "needValue": notice_eq_value,
                            "jobType":jobType
                        },
                    };

                    $.ajax(settings).done(function (response_transfer) {
                        // $('#notice_eq_value').css('border-color', '');
                        // $('#notice_job_description').css('border-color', '');

                        // $('#notice_eq_value').val('');
                        // $('#notice_job_description').val('');
                        // $('#notice_jobOrder_number').val('');
                        // $('#jobType0' ).attr('checked', 'checked');
                        // $('#itemSet').val($("#itemSet option:first").val());
                        $('#add_notice_to_wh_form').modal('toggle');                  

                        // create_notice_card()
                        $('#alert_notice_to_wh_form').modal('toggle');
                        setTimeout(function(){$('#alert_notice_to_wh_form').modal('hide')},1200);

                    })

                    

                    
                }else{
                    $('#notice_eq_value').css('border-color', 'red');
                    $('#notice_job_description').css('border-color', 'red');
                }

                $('#notice_eq_value').css('border-color', '');
                $('#notice_job_description').css('border-color', '');

                $('#notice_eq_value').val(1);
                $('#notice_job_description').val('');
                $('#notice_jobOrder_number').val('');
                $("#jobType0").prop("checked", true);
                $('#itemSet').val($("#itemSet option:first").val());
            })
        })
    // })
</script>

<!-- select PEA code -->
<script>

let settings = {
  "url": "/api/tb_estimate_set/get_all_set",
  "method": "GET",
  "timeout": 0,
};

$.ajax(settings).done(function (response) {
    let item = response.data;
    console.log(item)
    let select = document.getElementById("itemSet");
    for(let i = 0; i < item.length; i++)
    {
        let option = document.createElement("option")
        option.text = item[i].setName;
        option.value = item[i].setId;
        select.appendChild(option, select.length-1);
    }
});

</script>

<!-- add_notice_to_wh_form_submit -->
<script>
    // $(document).ready(function(){
        
    // })
</script>

<script>
    $('#add_notice_to_wh_form').on('hide.bs.modal', function(event)
    {
        let btnNotiToWearhouseSet = $(this).find('#add_notice_to_wh_form_submit');
        btnNotiToWearhouseSet.unbind("click");

        $('#notice_eq_value').css('border-color', '');
        $('#notice_job_description').css('border-color', '');

        $('#notice_eq_value').val(1);
        $('#notice_job_description').val('');
        $('#notice_jobOrder_number').val('');
        $("#jobType0").prop("checked", true);
        $('#itemSet').val($("#itemSet option:first").val());
        // console.log("clear")
    });
</script>


<!-- show modal noti from wbs -->
<script>
    // $(document).ready(function(){
        $('#add_notice_wbs_to_wh').on('click', function () {
            // let alertNotiWBS = new bootstrap.Modal(document.getElementById('add_notice_to_wh_form_wbs'))
            // alertNotiWBS.show()
            $('#add_notice_to_wh_form_wbs').modal('toggle');
            // --- reset modal ---
            $("#inputWbsNumber").val("") 
            $('#inputWbsNumber').css('border-color', '');
            $("#ShowWbsNumber").html("") 
            $("#ShowRequestNumber").html("")      
            $("#ShowRequestType").html("")
            $("#requestorName").html("") 
            $("#add_notice_wbs_to_wh_form_submit").attr("style", "display:none");
            $("#showWbsData").attr("style", "display:none");
            // -------------------
        // })

        // searchWbs button click
        $('#searchWBS').on('click', function () {            
            let wbsNumber = $('#inputWbsNumber').val()
            if(wbsNumber == ""){
                $('#inputWbsNumber').css('border-color', 'red');
                $("#add_notice_wbs_to_wh_form_submit").attr("style", "display:none");
                $("#ShowWbsNumber").html("") 
                $("#ShowRequestNumber").html("")      
                $("#ShowRequestType").html("")
                $("#requestorName").html("") 
                $("#showWbsData").attr("style", "display:block");
            }else{
                $('#inputWbsNumber').css('border-color', '');
                let settings = {
                "url": "/api/zcsr181/searchwbs",
                "method": "POST",
                "data": {
                    "wbsNumber": wbsNumber
                    }
                };

                $.ajax(settings).done(function (response) {        
                    // console.log(response)
                    if(response.message == "พบข้อมูล"){
                        $("#ShowWbsNumber").html("หมายเลข WBS : "+response.data.wbsNumber) 
                        $("#ShowWbsNumber").val(response.data.wbsNumber)
                        $("#ShowRequestNumber").html("หมายเลขคำร้อง : "+response.data.requestNumber)      
                        $("#ShowRequestType").html("ประเภทงาน : "+response.data.requestType)
                        $("#requestorName").html("ลูกค้า : "+response.data.requestorName) 
                        $("#add_notice_wbs_to_wh_form_submit").attr("style", "display:block");
                        $("#showWbsData").attr("style", "display:block");
                         
                    }else{   
                        $("#ShowWbsNumber").html("ไม่พบข้อมูล") 
                        $("#ShowRequestNumber").html("")      
                        $("#ShowRequestType").html("")
                        $("#requestorName").html("") 
                        $("#add_notice_wbs_to_wh_form_submit").attr("style", "display:none");
                        $("#showWbsData").attr("style", "display:block");
                    }                      
                });
            }
        })  
        // searchWbs button click

        $('#add_notice_wbs_to_wh_form_submit').on('click', function () {            
            // let wbsNumber = $('#inputWbsNumber').val()            
            
            let insertWbsNumber = $("#ShowWbsNumber").val()
            let userId = data.userLoginDataResponce.Username
            let timeStamp = moment(new Date(), 'YYYY-mm-dd HH:mm').format("YYYY-MM-DD HH:mm")
            let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
            let settings = {
            "url": "/api/cs_warehouse_step2/add_cswarehouse_step2",
            "method": "POST",
            "data": {
                    // "wbsNumber": wbsNumber.toUpperCase(),
                    "wbsNumber": insertWbsNumber,
                    "userId": userId,
                    "timeStamp": timeStamp,
                    "warehouseId": Bacode0
                }
            }

            $.ajax(settings).done(function (response_noti_wbs_add) {   
                console.log(response_noti_wbs_add)  
                $('#add_notice_to_wh_form_wbs').modal('toggle');     
                
                $('#alert_notice_to_wh_form').modal('toggle');
                setTimeout(function(){$('#alert_notice_to_wh_form').modal('hide')},1200);
            });
            
        }) 
    })
    // })
</script>


<script>
    $('#add_notice_to_wh_form_wbs').on('hide.bs.modal', function(event)
    {
        let btnNotiToWearhouse2 = $(this).find('#add_notice_wbs_to_wh_form_submit');
        btnNotiToWearhouse2.unbind("click");
        // console.log("clear")
    });
</script>

<script>
    $('#add_notice_to_wh_form_wbs').on('hide.bs.modal', function(event)
    {
        let btnNotiToWearhouse3 = $(this).find('#searchWBS');
        btnNotiToWearhouse3.unbind("click");
        // console.log("clear")
    });
</script>




