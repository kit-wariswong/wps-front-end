<!doctype html>
<html lang="en">
<head>
    <?php 
        include ('../include/head.php');
    ?>
    <title>เข้าสู่ระบบจัดการพัสดุอัจริยะ</title>
</head>



<body class="text-center bg-light">
    <div class="container">
        <div class="row align-items-center vh-100 justify-content-md-center ">
            <div class="col-xl-4">
                <form id="form_login" method="post" name="form_login">
                    <div class="row justify-content-center">
                        <!-- <div class="col-12">
                            <a class="text-warning"><h4>ระบบสำหรับทดสอบและทดลองใช้งาน</h4></a>
                        </div> -->
                        <div class="col-12">
                            <img class="mb-4" src="../images/icons/warehouse512.png" alt="" width="72" height="57">
                            <h1 class="h3 mb-3 fw-normal">เข้าสู่ระบบวางแผนจัดการพัสดุ</h1>
                        </div>
                        <div class="col-8">
                            <div class="form-floating mt-1">
                            <input type="number" class="form-control" id="username" name="username" placeholder="รหัสพนักงาน">
                            <label for="username">รหัสพนักงาน</label>
                            </div>
                            <div class="form-floating mt-1">
                            <input type="password" class="form-control" id="password" name="password" placeholder="รหัสผ่านเข้าคอมพิวเตอร์">
                            <label for="password">รหัสผ่านเข้าคอมพิวเตอร์</label>
                            </div>
                            <!-- สำหรับทดสอบ -->
                            <div class="form-floating mt-1">
                            <select class="" name="useruser" id="useruser">
                                <option value="me">ตนเอง</option>
                                <option value="501128">คลัง นครราชสีมา</option>
                                <option value="500738">บริการลูกค้า นครราชสีมา</option>    
                                <option value="508063">คลัง บุรีรัม</option> 
                                <option value="506207">บริการลูกค้า บุรีรัม</option>
                                <option value="486620">คลัง อุบล</option>
                                <option value="497718">บริการลูกค้า อุบล</option>    
                                <option value="455718">คลัง ศรีสะเกษ</option> 
                                <option value="503455">บริการลูกค้า ศรีสะเกษ</option>        
                                <option value="498364">คลัง หนองคาย</option> 
                                <option value="498923">บริการลูกค้า หนองคาย</option>  
                                <option value="493627">คลัง สุรินทร์</option> 
                                <option value="501070">บริการลูกค้า สุรินทร์</option>
                                <option value="498886">บริการลูกค้า ขก2</option>  
                                <option value="505547">บริการลูกค้า อด2</option>  
                                <option value="509067">บริการลูกค้า สดด</option>  
                                <option value="507404">บริการลูกค้า ตผ</option>  
                                <option value="485103">บริการลูกค้า วชร</option>    
                                <option value="496793">บริการลูกค้า หทล</option>  
                                <option value="499697">บริการลูกค้า สรน</option>  
                                <option value="496790">บริการลูกค้า ภข</option>  
                                <option value="508165">บริการลูกค้า ปธ</option>  
                                <option value="499702">บริการลูกค้า ปส</option>
                                <option value="323115">ทดสอบ</option>
                                <option value="509246">ทดสอบ</option>
                            </select>
                            </div>
                            <!-- *********** -->
                            <input class="w-100 btn btn-lg btn-primary mt-4" type="button" id="submit" value="เข้าสู่ระบบ" onclick="login()"></input>
                            <p class="mt-3 mb-3 text-muted">&copy; 2022</p>
                        </div>
                    </div>
                </form>
                <div id="alert_box" class="alert alert-warning align-items-center" role="alert" style="display:none">
                    <i class="bi bi-exclamation-triangle" aria-label="Warning:"></i>
                    <div id="alert_message"></div>
                </div>
                <div id="alert_info_box" class="alert alert-info align-items-center" role="alert" style="display:none">
                    <i class="bi bi-info-circle" aria-label="Warning:"></i>
                    <div id="alert_info_message"></div>
                </div>
            </div>
            <footer>
                <div class="alert alert-warning" role="alert">
                    ท่านสารมารถเข้าใช้งานระบบสำหรับการทดสอบ ผ่านลิ้งค์ :     
                    <a href="../../wps/pages/login.php">ระบบทดสอบ</a>                    
                </div>
            </footer>
        </div>
    </div>
</body>

<!-- import jquery -->
<script src="../assets/jquery-3.6.0/jquery-3.6.0.min.js"></script>

<!-- script login and validation -->
<script>

async function login(){

    let username = $("#username").val();
    let password = $("#password").val();
    let test = $("#useruser").val();
    // let test = "me";
    let userIP = "127.0.0.1"
    let dateNow = new Date()

    if(username=="" && password ==""){
        // document.getElementById("alert_box").style.display = "block";
        $("#alert_message").html("กรุณากรอกกรอก รหัสประจำตัว และ รหัสผ่าน")
        $("#alert_box").fadeIn(1000);        
    }else if(username==""){
        $("#alert_box").fadeOut(500); 
        $("#alert_message").html("กรุณากรอกกรอก รหัสประจำตัว")
        $("#alert_box").fadeIn(1000);    
    }else if(password==""){
        $("#alert_box").fadeOut(500); 
        $("#alert_message").html("กรุณากรอกกรอก รหัสผ่าน")
        $("#alert_box").fadeIn(1000);    
    }else{
        $("#alert_box").fadeOut(500);
        $("#alert_info_message").html("กำลังเข้าสู่ระบบ...")
        $("#alert_info_box").fadeIn(1000); 
        // res = await loginIDM(username,password)
        // console.log(res)
        var settings = {
            "url": "/api/login",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            "data": {
                "empid": username,
                "password": password,
                "useruser": test,
                "userIP": userIP,
                "userLastLogin": dateNow
            }
            };
            $.ajax(settings).done(function (response) {
            // console.log(response.message);
            // console.log(JSON.stringify(response.data));

            localStorage.setItem("loginStatus", JSON.stringify(response.message));
            localStorage.setItem("data", JSON.stringify(response.data));    
            
            if(response.message === "login_success")
            {
                if(response.data.userLoginDataResponce.DepartmentShortName ==="ผบค.")    
                {
                    window.location.replace("./cs_safety_stock.php");
                }else if(response.data.userLoginDataResponce.DepartmentShortName ==="ผคพ."){
                    window.location.replace("./wh_safety_stock_location.php");
                }else{
                    window.location.replace("./other_user.php");
                }
            }else{
                console.log(response.data.ResponseMsg)
                $("#alert_info_box").fadeOut(500); 
                $("#alert_message").html(response.data.ResponseMsg)
                $("#alert_box").fadeIn(1000);
            }
        });
    }

}
</script>

</html>