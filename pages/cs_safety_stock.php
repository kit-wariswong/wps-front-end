<!doctype html>
<html lang="en">
<head>
    <?php 
        include ('../include/head.php');
    ?>
    <title>เข้าสู่ระบบวางแผนและจัดการพัสดุ (งบผู้ใช้ไฟ) แบบเบ็ดเสร็จ</title>
</head>

<body>  
    <div class="container-fluid">        
        <div class="row flex-nowrap bg-dark">           
            <?php 
                include ('../include/sidemenu.php');
            ?>            
                      
            <div class="col p-0 mt-1 bg-light" >
            <?php 
                include ('../include/navbar.php');
            ?>            
            <!-- content start-->  
            <div class= "p-4">
                <h3>รายการพัสดุ Safety Stock งบผู้ใช้ไฟ (C)</h3>
                <div class="row mt-3">
                    <div class="col-2">
                        <select id="selectEquipType" class="form-select bg-primary text-white" aria-label="Default select example">
                            <option value="หม้อแปลง แคแปซิเตอร์ โวลเตจเรกูเลเตอร์" selected>หม้อแปลง</option>
                            <option value="เสา คอน คาน สมอบกคอนกรีต">เสา คอน คาน</option>
                            <option value="สายไฟและอุปกรณ์ประกอบ">สายไฟและอุปกรณ์ประกอบ</option>
                            <option value="ลูกถ้วยและอุปกรณ์ประกอบ">ลูกถ้วยและอุปกรณ์ประกอบ</option>
                            <option value="มิเตอร์และอุปกรณ์ประกอบ">มิเตอร์และอุปกรณ์ประกอบ</option>
                        </select>
                    </div>
                </div>
                <div id="table">
                    <table id="safety_stock_table" class="cell-border " style="width:100%"></table>
                </div>
            </div>
            <!-- content end-->
            </div>

            <!-- modal alert notice to warehouse -->
            <div class="modal fade" id="alert_notice_to_wh_form" data-bs-keyboard="false" tabindex="-1" aria-labelledby="alert-transfer-equipment-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content text-center">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <image src="../images/icons/accept64.png"></image>
                        <h3 class="mt-4">แจ้งเตือนไปยังคลังพัสดุสำเร็จ</h3>
                        <h5 id="itemSelect"class="mt-4"></h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ปิด</button>
                    </div>
                    </div>
                </div>
            </div>
            <!-- modal alert notice to warehouse -->

            <!-- modal add notice to warehouse -->
            <div class="modal fade" id="add_notice_to_wh_form" data-bs-keyboard="false" tabindex="-1" aria-labelledby="alert-transfer-equipment-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="mb-3">
                                    <h5>แจ้งเตือนคลังพัสดุ</h5>
                                </div>
                                <div class="mb-3">
                                    <label for="notice_eq_value" class="col-form-label">จำนวน <span class="text-danger">*</span></label>
                                    <input type="number" class="form-control" id="notice_eq_value" value="1" min="0">
                                </div>
                                <div class="mb-3">
                                    <label for="notice_customer_name" class="col-form-label">หมายเลขคำร้อง (เลข 12)</label>
                                    <input type="text" class="form-control" id="notice_jobOrder_number">
                                </div>
                                <div class="mb-3">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="jobType0" value="0" checked>
                                        <label class="form-check-label" for="jobType0">เอกชน</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="jobType1" value="1">
                                        <label class="form-check-label" for="jobType1">ราชการ</label>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="notice_job_description" class="col-form-label">หมายเหตุ <span class="text-danger">*</span></label>
                                    <textarea  class="form-control" id="notice_job_description" rows="3"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button id="add_notice_to_wh_form_submit" type="button" class="btn btn-danger">แจ้งความต้องการ</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal add notice to warehouse -->

        </div>
    </div>
    
</body>
</html>


<!-- Login check -->
<script>
    // let data = JSON.parse(localStorage.getItem("data"))
    // console.log(data)
    if(data===null)
    {
        window.location.replace("./login.php");
        if(data.LoginResponse.ResponseCode != "WSV0000"){
            window.location.replace("./login.php");
        } 
    }
       
</script>
<!-- Login check -->

<!-- Hide Notice button on navbar -->
<script>
    $("#transferNoticeButton").attr("style", "display:none");
    $("#NoticeButton").attr("style", "display:none");
</script>


<!-- remove and add class active -->
<script>
    $(".nav-link").even().removeClass("active");
    $("#cs_safety_stock").even().addClass("active");
</script>


<!-- create Datatable -->
<script>
let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"

// console.log(Bacode0)
function create_safety_stock_table(Bacode0,equipType){
    $('#safety_stock_table').DataTable( {
        ajax: {
            url: '/api/mb52fetch/stock_pea_all',
            type: "POST",
            data:{
                invId:Bacode0,
                equipType:equipType
            }
        },
        lengthChange: false,
        destroy: true,
        columns: 
        [
            // {
            //     title:"ประเภท",
            //     data:"mb52_fetch_belongs_to_tb_all_stuff.stuffGroup",
            //     className:"text-break"
            // },
            {
                title:"รหัสพัสดุ",
                data:"equipmentId"
            },
            {
                title:"รายการ",
                data:"mb52_fetch_belongs_to_tb_all_stuff.stuffNameTh",
                className:"text-break"
            },            
            {
                title:"หน่วยนับ",
                data:"mb52_fetch_belongs_to_tb_all_stuff.counter",
                className:"text-center"                
            },
            {
                title:"ระดับ Safety ที่กำหนด",
                data: null,
                className:"text-end",
                render : function(data, type, row) { 
                    if(data["tb_mb52_fetch_belongs_to_tb_displayed_safety"] === null){
                        
                        return "-"
                    }
                    else{
                        return (data["tb_mb52_fetch_belongs_to_tb_displayed_safety"]["safety_0021"]).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")

                    }              
                }
            },
            {
                title:"ความต้องการพัสดุ",
                data: null,
                className:"text-end",
                render : function(data, type, row) { 
                    if(data["tb_mb52_fetch_belongs_to_tb_displayed_safety"] === null){
                        return "-"
                    }
                    else{
                        return (data["tb_mb52_fetch_belongs_to_tb_displayed_safety"]["needed_value"]).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }                    
                }
            },
            {
                title:"จำนวนคงคลัง",
                data:null,
                className:"text-end",
                render : function(data, type, row) {                     
                    return (data["sumStockAvailable"]).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")        
                }
            },
            {
                title:"จำนวนคงคลังภาพรวมทั้งเขต",
                data: null ,
                className:"text-end",
                render : function(data, type, row) { 
                    return (data["allStockAvailable"] - data["sumStockAvailable"]).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
            },            
            {
                data:null,
                render: function (data, type, row) {
                    return '<button class="btn btn-danger bi bi-bell-fill rounded-circle" style="font-size: 1.2rem;" id="btn-noti-to-warehoue" data-bs-toggle="tooltip" data-bs-placement="top" title="แจ้งเตือนคลังพัสดุ"></button>';
                }

            },
        ],
        order : [[ 4, "desc" ]]
        // columnDefs: [
        //             {
        //                 render: function (data, type, full, meta) {
        //                     return "<div class='text-break'>" + data + "</div>";
        //                 },
        //                 targets: 2
        //             }
        //          ]
    });
}

// select equipment type
$("#selectEquipType").change(function() {
    
    let equipType = $( "#selectEquipType option:selected" ).val();
    create_safety_stock_table(Bacode0, equipType)
    // console.log(equipType)
});

// select equipment type default
let equipType = $( "#selectEquipType option:selected" ).val();
create_safety_stock_table(Bacode0, equipType)

//<!-- transfer button click -->

$('#safety_stock_table').on('click', 'button#btn-noti-to-warehoue', function () {
    let cellData = $(this).closest('tr').find('td')
    let equipmentId = cellData[0].innerText
    console.log(equipmentId)
    let notiEquipment = new bootstrap.Modal(document.getElementById('add_notice_to_wh_form'))
    notiEquipment.show()

    $('#add_notice_to_wh_form_submit').on('click', function () {
        let notice_eq_value = $("#notice_eq_value").val() // *
        let notice_job_description = $("#notice_job_description").val() // *      
        let notice_jobOrder_number = $("#notice_jobOrder_number").val()          
        let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        let jobType = $( "input:checked" ).val()
                    

        if(notice_eq_value != "" && notice_eq_value != "0" && notice_job_description != ""){

            let settings = {
                "url": "/api/cs_warehouse_step1/add_cswarehouse_step1_nonset/",
                "method": "POST",            
                "data": {
                    "equipmentId": equipmentId,
                    "jobName": notice_job_description,
                    "userId": data.userLoginDataResponce.Username,
                    "warehouseId": Bacode0,
                    "orderId": notice_jobOrder_number,
                    "needValue": notice_eq_value,
                    "jobType":jobType
                },
            };

            console.log(settings)

            $.ajax(settings).done(function (response_transfer) {
                $('#notice_eq_value').css('border-color', '');
                $('#notice_job_description').css('border-color', '');

                $('#notice_eq_value').val('1');
                $('#notice_job_description').val('');
                $('#notice_jobOrder_number').val('');
                $('#jobType0' ).attr('checked', 'checked');
                $('#add_notice_to_wh_form').modal('toggle');

                console.log(response_transfer)

                // create_notice_card()
                $('#alert_notice_to_wh_form').modal('toggle');
                setTimeout(function(){$('#alert_notice_to_wh_form').modal('hide')},1200);

            })

            
        }else{
            $('#notice_eq_value').css('border-color', 'red');
            $('#notice_job_description').css('border-color', 'red');
        }
    })

        
 });

$('#add_notice_to_wh_form').on('hide.bs.modal', function(event)
{
    let btnNotiToWearhouse = $(this).find('#add_notice_to_wh_form_submit');
    btnNotiToWearhouse.unbind("click");
    // console.log("clear")
});


</script>
<!-- create Datatable END -->


<!-- get noti transfer count number -->
<!-- <script>    
    function getNotiTransferCount(Bacode0){
        let settings = {
            "url": "/api/tb_transfer/select_count_transfer/",
            "method": "POST",            
            "data": {
                "warehouseIdRequester": Bacode0
            },
        };
        $.ajax(settings).done(function (response_transfer_count) {
            let NotiTransferCount = response_transfer_count.total
            console.log(NotiTransferCount)
            if(NotiTransferCount != 0){
                $("#transferNotice").html(NotiTransferCount)
                $("#transferNotice").attr("style", "display:block")
                $("#side_noti_transfer_req").html(NotiTransferCount)
                $("#side_noti_transfer_req").show()
            }
            else{
                $("#transferNotice").attr("style", "display:none")
            }
        });
    }

    getNotiTransferCount(Bacode0)
</script> -->

<!-- get noti transfer count number for owner -->
<!-- <script>
        function getNotiTransferCountOwner(Bacode0){
            let settings = {
                "url": "/api/tb_transfer/select_count_transfer_owner/",
                "method": "POST",            
                "data": {
                    "warehouseIdOwner": Bacode0
                },
            };
            $.ajax(settings).done(function (response_transfer_count_owner) {
                let NotiTransferCountOwner = response_transfer_count_owner.total
                if(NotiTransferCountOwner !=0){
                    $("#side_noti_transfer_own").html(NotiTransferCountOwner)
                    $("#side_noti_transfer_own").show()
                }
                else{
                    $("#side_noti_transfer_own").hide()
                }
            });
        }    
    getNotiTransferCountOwner(Bacode0)    
</script> -->

