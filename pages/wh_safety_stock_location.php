<!doctype html>
<html lang="en">
<head>
    <?php 
        include ('../include/head.php');
    ?>
    <title>เข้าสู่ระบบวางแผนและจัดการพัสดุ (งบผู้ใช้ไฟ) แบบเบ็ดเสร็จ</title>
</head>

<body>  
    <div class="container-fluid">        
        <div class="row flex-nowrap bg-dark">           
            <?php 
                include ('../include/sidemenu.php');
            ?>            
                      
            <div class="col p-0 mt-1 bg-light" >
            <?php 
                include ('../include/navbar.php');
            ?>            
            <!-- content start-->  
            <div class= "p-4">
                <h3>รายการพัสดุ Safety Stock งบผู้ใช้ไฟ (C)</h3>
                <div class="row">
                    <div class="col-2">
                        <select id="selectLocation" class="form-select bg-secondary text-white" aria-label="Default select example">
                            <!-- <option class="text-white" selected>เลือก เขต</option> -->
                            <option id="NE1" value="D">กฟฉ.1</option>
                            <option id="NE2" value="E">กฟฉ.2</option>
                            <option id="NE3" value="F">กฟฉ.3</option>
                        </select>
                    </div>
                </div>
                <!-- table content -->
                <div id="table">
                </div>
                <!-- table content -->
            </div>
            <!-- content end-->
            </div>

            <!-- modal confirm alert transfer equipment -->
            <div class="modal fade" id="confirm-alert-transfer-equipment" data-bs-keyboard="false" tabindex="-1" aria-labelledby="confirm-alert-transfer-equipment-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content text-center">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <h3 class="mt-4">ยืนยันขอรับโอนพัสดุ</h3>
                        <h5 id="itemSelect"class="mt-4"></h5>
                    </div>
                    <div class="modal-footer">
                        <button id="confirm-alert-transfer-equipment-btn" type="button" class="btn btn-primary me-auto col-5 ms-4" >ยืนยัน</button>
                        <button id="cancel-alert-transfer-equipment-btn" type="button" class="btn btn-secondary col-5 me-4" data-bs-dismiss="modal">ยกเลิก</button>
                    </div>
                    </div>
                </div>
            </div>
            <!-- modal confirm alert transfer equipment -->

            <!-- modal alert transfer equipment -->
            <div class="modal fade" id="alert-transfer-equipment" data-bs-keyboard="false" tabindex="-1" aria-labelledby="alert-transfer-equipment-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content text-center">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <image src="../images/icons/accept64.png"></image>
                        <h3 class="mt-4">คุณได้เพิ่มพัสดุในรายการขอรับโอนแล้ว</h3>
                        <h5 id="itemSelect"class="mt-4"></h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ปิด</button>
                    </div>
                    </div>
                </div>
            </div>
            <!-- modal alert warning equipment -->

            <!-- modal confirm buy equipment -->
            <div class="modal fade" id="confirm-buy-equipment" data-bs-keyboard="false" tabindex="-1" aria-labelledby="confirm-buy-equipment-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content text-center">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <h3 class="mt-4">ยืนยันขอจัดซื้อพัสดุพัสดุ</h3>
                        <h5 id="itemSelect"class="mt-4"></h5>
                    </div>
                    <div class="modal-footer">
                        <button id="confirm-buy-equipment-btn" type="button" class="btn btn-primary me-auto col-5 ms-4" >ยืนยัน</button>
                        <button id="cancel-buy-equipment-btn" type="button" class="btn btn-secondary col-5 me-4" data-bs-dismiss="modal">ยกเลิก</button>
                    </div>
                    </div>
                </div>
            </div>
            <!-- modal confirm buy equipment -->

            <!-- modal alert buy equipment -->
            <div class="modal fade" id="alert-buy-equipment" data-bs-keyboard="false" tabindex="-1" aria-labelledby="alert-buy-equipment-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content text-center">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <image src="../images/icons/accept64.png"></image>
                        <h3 class="mt-4">คุณได้เพิ่มพัสดุในรายการขอจัดซื้อแล้ว</h3>
                        <h5 id="itemSelect"class="mt-4"></h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ปิด</button>
                    </div>
                    </div>
                </div>
            </div>
            <!-- modal alert buy equipment -->

            <!-- modal alert equipment -->
            <div class="modal fade" id="alert-warning-equipment" data-bs-keyboard="false" tabindex="-1" aria-labelledby="alert-warning-equipment-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="text-danger me-auto">แจ้งเตือน !</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <h5 class="">หมวดที่ 1: หม้อแปลง</h5>
                        <p id="eT1" class="ms-5"></p>
                        <br>
                        <h5 class="">หมวดที่ 2: มิเตอร์</h5>
                        <p id="eT2" class="ms-5"></p>
                        <br>
                        <h5 class="">หมวดที่ 3: สายไฟ</h5>
                        <p id="eT3" class="ms-5"></p>
                        <br>
                        <h5 class="">หมวดที่ 4: ลูกถ้วย</h5>
                        <p id="eT4" class="ms-5"></p>
                        <br>
                        <h5 class="">หมวดที่ 5: เสา</h5>
                        <p id="eT5" class="ms-5"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ปิด</button>
                    </div>
                    </div>
                </div>
            </div>
            <!-- modal alert warning equipment -->

        </div>
    </div>
    
</body>
</html>


<!-- Login check -->
<script>
    // let data = JSON.parse(localStorage.getItem("data"))
    // console.log(data)
    if(data===null)
    {
        window.location.replace("./login.php");
        if(data.LoginResponse.ResponseCode != "WSV0000"){
            window.location.replace("./login.php");
        } 
    }
    
    if((data.userLoginDataResponce.BaCode).includes("E")){
        $("#selectLocation :nth-child(2)").prop('selected', true);
    }else if((data.userLoginDataResponce.BaCode).includes("D")){
        $("#selectLocation :nth-child(1)").prop('selected', true);
    }else if((data.userLoginDataResponce.BaCode).includes("F")){
        $("#selectLocation :nth-child(3)").prop('selected', true);
    }
</script>
<!-- Login check -->

<!-- create Datatable -->
<script>
// let firstSelectedPEA = $("#selectPEA :nth-child(1)").val();
// create_safety_stock_table(Bacode0, firstSelectedPEA)

function create_safety_stock_table(thisWarehouse, location){
    function subTable_set ( d ) {
        // console.log(d)
        // `d` is the original data object for the row
        const other_pea_data = d.otherPea
        const other_pea_count = Object.keys(other_pea_data).length;
        // console.log(other_pea_count)
        let sub_pea_table=``
            for(j=0;j<other_pea_count;j++){
                if(d != 0){
                    const equipmentId = d.stuffId
                    const warehouseOwnId = other_pea_data[j]["wareHouseId"+j]
                    const warehouseSafety = other_pea_data[j]["warehouseSafety"+j]
                    const warehouseNeed = other_pea_data[j]["warehouseNeed"+j]
                    const warehouseStock = other_pea_data[j]["warehouseStock"+j]
                    const warehouseReq = other_pea_data[j]["warehouseReq"+j]
                    const warehouseOwn = other_pea_data[j]["warehouseOwn"+j]
                    // const bgColor = warehouseStock <= warehouseNeed ? 'style="background-color:#FCAEC3;"' : 'style="background-color:#9AF3CF;"'
                    const bgColor = warehouseStock < 1 ? 'style="background-color:#FCAEC3;"' : 'style="background-color:#9AF3CF;"'
                    // const disableTransfer = warehouseStock <= warehouseNeed ? 'disabled' : ''
                    const disableTransfer = warehouseStock < 1 ? 'disabled' : ''
                    sub_pea_table+=
                    `<tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="text-end">${(warehouseSafety).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                        <td class="text-end">${(warehouseNeed).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                        <td class="text-end" ${bgColor}>${(warehouseStock).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                        <td class="text-end" style="color:#32945b;">${(warehouseReq).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                        <td class="text-end" style="color:#CC4466;">${(warehouseOwn).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                        <td class="text-center">${other_pea_data[j]["wareHouseName"+j]}</td>
                        <td class="text-center"><button class="btn btn-primary bi bi-cart3 rounded-circle" style="font-size: 0.9rem;" id="btn-alert-transfer-equipment" data-equipmentId="${equipmentId}" data-warehouseOwnId="${warehouseOwnId}" ${disableTransfer} data-bs-toggle="tooltip"></button></td>
                    </tr>`
                }
            }

           return $(sub_pea_table).appendTo($("#subTable"));

    }

    $("#table").empty()
    $("#table").append(`
        <table id="safety_stock_table" class="display" style="width:100%">
            <thead>
                <tr>
                    <th></th>
                    <th class="text-center">รหัสพัสดุ</th>
                    <th class="text-center">รายการ</th>
                    <th class="text-center">หน่วยนับ</th>
                    <th class="text-center">ระดับ safety ที่กำหนด</th>
                    <th class="text-center">ความต้องการพัสดุ</th>
                    <th class="text-center">จำนวนพัสดุคงคลัง</th>
                    <th class="text-center">STO ระหว่างทาง รับโอน</th>
                    <th class="text-center">STO ระหว่างทาง จ่ายโอน</th>
                    <th class="text-center">คลัง</th>                    
                    <th></th>
                </tr>
                <tbody id="subTable"></tbody>
            </thead>
        </table>
    `)

    const table = $('#safety_stock_table').DataTable( {
        ajax: {
            url: '/api/safetyne2/compare_location/',
            type: "POST",
            data:{
                wh1:thisWarehouse,
                location:location
            }
        },
        lengthChange: false,
        destroy: true,
        columns: 
        [
            {
                className:      'dt-control',
                orderable:      false,
                data:           null,
                defaultContent: ''
            },  
            {
                // title:"รหัสพัสดุ",
                data:"stuffId"
            },
            {
                // title:"รายการ",
                data:"stuffNameTh",
                className:"text-break"
            },
            {
                // title:"หน่วยนับ",
                data:"counter",
                className:"text-center"
            },
            {
                // title:"ระดับ safety ที่กำหนด",
                data:null,
                className:"text-end",
                render: function (data, type, row ) {        
                    const thisValue = data["safety_0021"]            
                    return Number(thisValue).toLocaleString(undefined, {minimumFractionDigits: 2,})

                    
                },
            },
            {
                // title:"ความต้องการพัสดุ",
                data:null,
                className:"text-end",
                render: function (data, type, row ) {        
                    const thisValue = data["needed_value"]            
                    return Number(thisValue).toLocaleString(undefined, {minimumFractionDigits: 2,})
                },
            },
            {
                // title:"จำนวนพัสดุคงคลัง",
                data:null,
                className:"text-end",
                render: function (data, type, row ) {        
                    const thisValue = data["thisWarehouseStock"]            
                    return Number(thisValue).toLocaleString(undefined, {minimumFractionDigits: 2,})
                },
                createdCell: function(td, cellData, rowData, row, col){
                    // var color = (cellData === 'm') ? 'blue' : 'red';
                    $(td).css('background-color', '#F0F0F0');
                }
            },
            {
                // title:"จำนวนพัสดุคงคลัง",
                data:null,
                className:"text-end",
                render: function (data, type, row ) {        
                    const thisValue = data["thisWarehouseReq"]            
                    return Number(thisValue).toLocaleString(undefined, {minimumFractionDigits: 2,})
                },
                createdCell: function(td, cellData, rowData, row, col){
                    // var color = (cellData === 'm') ? 'blue' : 'red';
                    $(td).css('color', '#32945b');
                }
            },
            {
                // title:"จำนวนพัสดุคงคลัง",
                data:null,
                className:"text-end",
                render: function (data, type, row ) {        
                    const thisValue = data["thisWarehouseOwn"]            
                    return Number(thisValue).toLocaleString(undefined, {minimumFractionDigits: 2,})
                },
                createdCell: function(td, cellData, rowData, row, col){
                    // var color = (cellData === 'm') ? 'blue' : 'red';
                    $(td).css('color', '#CC4466');
                }
            },
            {
                // title:"คลัง",
                data:null,
                className:"text-center",
                render: function (data, type, row ) {       
                    return ''
                },
            },
            {
                data:null,
                render: function ( data, type, row ) {                    
                    return '<button class="btn btn-success bi bi-cash-coin rounded-circle" style="font-size: 1.2rem;" id="btn-buy-equipment"></button>';
                }

            }    
        ],
        order: [[ 6, "asc" ]],
    } );

    // Add event listener for opening and closing details
    $('#safety_stock_table tbody').on('click', 'td.dt-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( subTable_set(row.data()) ).show();
            tr.addClass('shown');
        }
    });

    //  transfer button click
    $('#safety_stock_table').on('click', '#btn-alert-transfer-equipment', function () {
        let cellData = $(this).closest('tr').find('td')
        let equipmentId = $(this).attr('data-equipmentId');
        let warehouseOwnId = $(this).attr('data-warehouseOwnId');

        // console.log(equipmentId, warehouseOwnId)

        let settings = {
            "url": "/api/tb_transfer/add_tb_transfer/",
            "method": "POST",            
            "data": {
                "warehouseIdRequester":Bacode0,
                "warehouseIdEquipOwner":warehouseOwnId,
                "userId":data.userLoginDataResponce.Username,
                "sessionId":data.LoginResponse.ResultObject.SessionId,
                "transferStatus":1
            },
        };
        // console.log(settings)
        let confirmAlertTransferEquipment = new bootstrap.Modal(document.getElementById('confirm-alert-transfer-equipment'))
        confirmAlertTransferEquipment.show()
         
        $('#confirm-alert-transfer-equipment-btn').on('click', function () {   
        //    $("#itemSelect").html(cellData[1].innerText)
            $.ajax(settings).done(function (response_transfer) {
                // console.log(response_transfer)
                let transferId = response_transfer.data.transferId
         
                // equipmentID = `${equipmentID[0]}-${equipmentID[1]}${equipmentID[2]}-${equipmentID[3]}${equipmentID[4]}${equipmentID[5]}-${equipmentID[6]}${equipmentID[7]}${equipmentID[8]}${equipmentID[9]}`
                let settings = {
                    "url": "/api/tb_transfer_list/add_tb_transfer_list/",
                    "method": "POST",                      
                    "data": {
                        "transferId":transferId,
                        "stuffToAdd":equipmentId,
                        "equipmentValue":0,
                    },
                };
                
                $.ajax(settings).done(function (response_transfer_list) {
                    // console.log(response_transfer_list)
                    getNotiTransferCount(Bacode0)
                    getNotiTransferCountOwner(Bacode0) 

                    //clear all data
                    $('#confirm-alert-transfer-equipment').modal('toggle');
                    cellData =''
                    settings =''
                    // transferId =''
                    // equipmentID =''
                    //clear all data
                    
                    // let alertTransferEquipment = new bootstrap.Modal(document.getElementById('alert-transfer-equipment'))
                    // alertTransferEquipment.show()
                    $('#alert-transfer-equipment').modal('toggle');
                    setTimeout(function(){$('#alert-transfer-equipment').modal('hide')},1200);
                });
            });
            
            
        })

    });


    //  buy button click
    $('#safety_stock_table').on('click', '#btn-buy-equipment', function () {
        let cellData = $(this).closest('tr').find('td')
        

        let equepmentId = cellData[1].innerText

        let settings = {
            "url": "/api/tb_purchase_list/add_stuff_list_to_tb_purchase/",
            "method": "POST",            
            "data": {
                "userId" : data.userLoginDataResponce.Username,
                "stuffToAdd" : equepmentId, 
                "invId" : Bacode0,
            },
        };
        
        $('#confirm-buy-equipment').modal('toggle');
        
        $('#confirm-buy-equipment-btn').on('click', function () {  
            console.log(settings) 
            $('#confirm-buy-equipment').modal('toggle')
        // //    $("#itemSelect").html(cellData[1].innerText)
            $.ajax(settings).done(function (response_buy) {
        
                

                $('#alert-buy-equipment').modal('toggle')
                setTimeout(function(){$('#alert-buy-equipment').modal('hide')},1200);

            });
            
            
        })

    });

}

</script>


<!-- Select Main location -->
<script>
$("#selectLocation").change(function() {
    let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
    let selectedLocation = $( "#selectLocation option:selected" ).val();
    // console.log(Bacode0)
    // console.log(selectedLocation)
    // addOption_selectPEA(selectedLocation,Bacode0)
    create_safety_stock_table(Bacode0, selectedLocation)

});
</script>
<!-- Select Main location -->

<!-- remove and add class active -->
<script>
    $(".nav-link").even().removeClass("active");
    $("#wh_safety_stock").even().addClass("active");
</script>

<!-- select PEA code -->
<script>
    let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
    let locationPEA = Bacode0.substring(0,1)
    // addOption_selectPEA(locationPEA,Bacode0)

    create_safety_stock_table(Bacode0, locationPEA)


    function addOption_selectPEA(locationPEA,currentPEA){
        let settings = {
        "url": "/api/warehouse_info/select_by_location/",
        "method": "POST",
        "data" : {
                location : locationPEA,
                currentPEA : Bacode0,
            }
        };

        $.ajax(settings).done(function (response) {
            let item = response.data;
            let select = document.getElementById("selectPEA");
            select.options.length = 0;
            for(let i = 0; i < item.length; i++)
            {
                let option = document.createElement("option")
                option.text = item[i].warehouseName;
                option.value = item[i].warehouseId;
                select.appendChild(option, select.length-1);
            }

            let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
            let firstSelectedPEA = $("#selectPEA :nth-child(1)").val();
            // console.log(Bacode0)
            // console.log(firstSelectedPEA)

            create_safety_stock_table(Bacode0, firstSelectedPEA)
        });
    }
</script>



<!-- get noti transfer count number -->
<script>    
    function getNotiTransferCount(Bacode0){
        let settings = {
            "url": "/api/tb_transfer/select_count_transfer/",
            "method": "POST",            
            "data": {
                "warehouseIdRequester": Bacode0,
                "transferStatus": 1
            },
        };
        $.ajax(settings).done(function (response_transfer_count) {
            let NotiTransferCount = response_transfer_count.total
            // console.log(NotiTransferCount)
            if(NotiTransferCount != 0){
                $("#transferNotice").html(NotiTransferCount)
                $("#transferNotice").attr("style", "display:block")
                $("#side_noti_transfer_req").html(NotiTransferCount)
                $("#side_noti_transfer_req").show()
            }
            else{
                $("#transferNotice").attr("style", "display:none")
            }
        });
    }

    getNotiTransferCount(Bacode0)
</script>

<!-- get noti transfer count number for owner -->
<script>
        function getNotiTransferCountOwner(Bacode0){
            let settings = {
                "url": "/api/tb_transfer/select_count_transfer_owner/",
                "method": "POST",            
                "data": {
                    "warehouseIdOwner": Bacode0,
                    "transferStatus": 2
                },
            };
            $.ajax(settings).done(function (response_transfer_count_owner) {
                let NotiTransferCountOwner = response_transfer_count_owner.total
                if(NotiTransferCountOwner !=0){
                    $("#side_noti_transfer_own").html(NotiTransferCountOwner)
                    $("#side_noti_transfer_own").show()
                }
                else{
                    $("#side_noti_transfer_own").hide()
                }
            });
        }    
    getNotiTransferCountOwner(Bacode0)    
</script>


<!-- get cs noti count number -->
<script>
    getNotiTransferCountOwner(Bacode0)

    function getNotiTransferCountOwner(Bacode0){
        let settings = {
            "url": "/api/cs_warehouse_step1/select_by_wh_id_count/",
            "method": "POST",            
            "data": {
                "warehouseId": Bacode0
            },
        };
        $.ajax(settings).done(function (response_cs_notice_count) {
            let csNotiCount = response_cs_notice_count.total
            if(csNotiCount !=0){
                $("#Notice").html(csNotiCount)
                $("#Notice").attr("style", "display:block")
                $("#side_noti_cs").html(csNotiCount)
                $("#side_noti_cs").show()
            }
            else{
                $("#Notice").attr("style", "display:none")
                $("#side_noti_cs").hide()
            }
        });
    }    
</script>

<script>
    $('#confirm-alert-transfer-equipment').on('hide.bs.modal', function(event)
    {
        let confirmEquipTransfer = $(this).find('#confirm-alert-transfer-equipment-btn');
        confirmEquipTransfer.unbind("click");
        // console.log("clear")
    });
</script>

<script>
    $('#confirm-buy-equipment').on('hide.bs.modal', function(event)
    {
        let confirmEquipTransfer = $(this).find('#confirm-buy-equipment-btn');
        confirmEquipTransfer.unbind("click");
        // console.log("clear")
    });
</script>

<script>
    $(document).ready(function(){

        let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        var settings = {
        "url": "/api/safetyne2/select_min_equipment",
        "method": "POST",
        "data": {
            "thisWarehouse": Bacode0
        }
        };

        $.ajax(settings).done(function (response) {
            // console.log(response);

            $("#eT1").html(`<p class="text-danger me-auto">${response.data[0].TransformerData[0].tb_displayed_safety_belongs_to_tb_all_stuff.stuffNameTh} คงเหลือ ${response.data[0].TransformerData[0].min_stock_value} ${response.data[0].TransformerData[0].tb_displayed_safety_belongs_to_tb_all_stuff.counter}</p>`)
            $("#eT2").html(`<p class="text-danger me-auto">-</p>`)
            $("#eT3").html(`<p class="text-danger me-auto">${response.data[2].lineData[0].tb_displayed_safety_belongs_to_tb_all_stuff.stuffNameTh} คงเหลือ ${response.data[2].lineData[0].min_stock_value} ${response.data[2].lineData[0].tb_displayed_safety_belongs_to_tb_all_stuff.counter}</p>`)
            $("#eT4").html(`<p class="text-danger me-auto">${response.data[3].insulationData[0].tb_displayed_safety_belongs_to_tb_all_stuff.stuffNameTh} คงเหลือ ${response.data[3].insulationData[0].min_stock_value} ${response.data[3].insulationData[0].tb_displayed_safety_belongs_to_tb_all_stuff.counter}</p>`)
            $("#eT5").html(`<p class="text-danger me-auto">${response.data[4].poleData[0].tb_displayed_safety_belongs_to_tb_all_stuff.stuffNameTh} คงเหลือ ${response.data[4].poleData[0].min_stock_value} ${response.data[4].poleData[0].tb_displayed_safety_belongs_to_tb_all_stuff.counter}</p>`)
        });

        let alertWarningEquipment = new bootstrap.Modal(document.getElementById('alert-warning-equipment'))
        alertWarningEquipment.show()

        // let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        // let firstSelectedPEA = $("#selectPEA :nth-child(1)").val();
        // console.log(Bacode0)
        // console.log(firstSelectedPEA)

        // create_safety_stock_table(Bacode0, firstSelectedPEA)
    })
</script>


