<!doctype html>
<html lang="en">
<head>
    <?php 
        include ('../include/head.php');
    ?>
    <title>เข้าสู่ระบบวางแผนและจัดการพัสดุ (งบผู้ใช้ไฟ) แบบเบ็ดเสร็จ</title>
</head>

<body>   
    <div class="container-fluid">
        <div class="row flex-nowrap bg-dark">
            
            <?php 
                include ('../include/sidemenu.php');
            ?>            
                      
            <div class="col p-0 mt-1 bg-light" >
            <?php 
                include ('../include/navbar.php');
            ?>
            <!-- content start-->  
            <div class="p-4">
                <h3>รายการร้องขอพัสดุ</h3>
                <div class="mt-4">   
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="history3" value="3" checked>
                        <label class="form-check-label" for="history3">รออนุมัติโอนพัสดุ</label>
                    </div>    
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="history5" value="5">
                        <label class="form-check-label" for="history5">ผู้รับยืนยันรับของแล้ว</label>
                    </div>  
                </div>
                <div id="transfer_list">

                </div>
            </div>
            <!-- content end-->
            </div>
            <!-- modal alert transfer equipment -->
            <div class="modal fade" id="save-transfer-data" data-bs-keyboard="false" tabindex="-1" aria-labelledby="save-transfer-data-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content text-center">
                        <div class="modal-header">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <image src="../images/icons/accept64.png"></image>
                            <h4 class="mt-4">บันทึกข้อมูลคำร้องในรายการขอรับโอนแล้ว</h4>
                            <h5 id="itemSelect"class="mt-4"></h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ปิด</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal alert transfer equipment -->

            <!-- modal confirm alert transfer equipment -->
            <div class="modal fade" id="reject-alert-transfer-equipment" data-bs-keyboard="false" tabindex="-1" aria-labelledby="reject-alert-transfer-equipment-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content text-center">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <h3 class="mt-4">ปฏิเสธการขอโอนพัสดุ</h3>
                        <h5 id="itemSelect"class="mt-4"></h5>
                    </div>
                    <div class="modal-footer">
                        <button id="reject-alert-transfer-equipment-btn" type="button" class="btn btn-danger me-auto col-5 ms-4">ยืนยัน</button>
                        <button id="cancel-alert-transfer-equipment-btn" type="button" class="btn btn-secondary col-5 me-4" data-bs-dismiss="modal">ยกเลิก</button>
                    </div>
                    </div>
                </div>
            </div>
            <!-- modal confirm alert transfer equipment -->

            <!-- modal pdf create transfer equipment -->
            <div class="modal fade" id="pdf-create-transfer-equipment" data-bs-keyboard="false" tabindex="-1" aria-labelledby="pdf-create-transfer-equipment-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content text-center">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <h3 class="mt-4">สร้างเอกสารการขอโอนพัสดุ</h3>
                        <!-- <h5 id="itemSelect"class="mt-4"></h5> -->
                    </div>
                    <div class="modal-footer">
                        <button id="pdf-create-transfer-equipment-btn" type="button" class="btn btn-success me-auto col-5 ms-4">ยืนยัน</button>
                        <button id="cancel-create-transfer-equipment-btn" type="button" class="btn btn-secondary col-5 me-4" data-bs-dismiss="modal">ยกเลิก</button>
                    </div>
                    </div>
                </div>
            </div>
            <!-- modal pdf create transfer equipment -->
        </div>
    </div>
    
</body>
</html>


<!-- Login check -->
<script>
    // let data = JSON.parse(localStorage.getItem("data"))
    console.log(data)
    if(data===null)
    {
        window.location.replace("./login.php");
        if(data.LoginResponse.ResponseCode != "WSV0000"){
            window.location.replace("./login.php");
        } 
    }
</script>
<!-- Login check -->


<!-- remove and add class active -->
<script>
    $(".nav-link").even().removeClass("active");
    // $("#wh_transfer_own_list").even().addClass("active");
</script>


<!-- function Show transfer group -->
<script>
function create_transfer_card(){
    let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"

    let settings = {
        "url": "/api/tb_transfer/select_transfer_owner",
        "method": "POST",
        "data": {
            "warehouseIdOwner": Bacode0,
            "transferStatus" : 3
        }
    };

    $.ajax(settings).done(function (response) {
        // console.log(response);
        $("div#transfer_list").empty()
        let transfer_count = response.total
    
        //   console.log(transfer_list_count)
        for(i=0;i<transfer_count;i++){
            try {
                let transfer_list_count = response.data[i].tb_transfer_tb_transfer_list_associate.length
                let transfer_list_table=""
                for(j=0;j<transfer_list_count;j++){
                    if(response.data[i].tb_transfer_tb_transfer_list_associate.length != 0){
                        const ownStock = (response.data[i].tb_transfer_tb_transfer_list_associate[j].transfer_list_belongs_to_tb_displayed_safety.stock_value)
                        let deleteTransferListBtn = response.data[i].tb_transfer_tb_transfer_list_associate.length == 1 ? `` : `<td>
                                <button id="delete_transfer_list" class="btn btn-white text-danger bi bi-trash-fill" style="font-size: 1.1rem;">ลบ</button>
                            </td>` 
                        transfer_list_table+=
                        `<tr>
                            <td style="display:none;">${response.data[i].tb_transfer_tb_transfer_list_associate[j].transferListId}</td>
                            <td class="text-center">${j+1}</td>
                            <td>${response.data[i].tb_transfer_tb_transfer_list_associate[j].equipmentId}</td>
                            <td>${response.data[i].tb_transfer_tb_transfer_list_associate[j].transfer_list_belongs_to_tb_displayed_safety.tb_displayed_safety_belongs_to_tb_all_stuff.stuffNameTh}</td>
                            <td class="text-center">${response.data[i].tb_transfer_tb_transfer_list_associate[j].transfer_list_belongs_to_tb_displayed_safety.tb_displayed_safety_belongs_to_tb_all_stuff.counter}</td>
                            <td class="text-center">${response.data[i].tb_transfer_tb_transfer_list_associate[j].transfer_list_belongs_to_tb_displayed_safety.needed_value}</td>
                            <td class="text-center">${(ownStock).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td class="text-center"><input class="text-center" type="number" value="${response.data[i].tb_transfer_tb_transfer_list_associate[j].equipmentValue}" style="width: 5em" min="0" max="${ownStock}" style="width: 5em" oninput="this.value = Math.abs(this.value)"></input></td>
                            ${deleteTransferListBtn}                            
                        </tr>`
                    }
                }

                $("div#transfer_list").append(
                    `<div class="card mt-3 ${(i % 2  != 0) ? "bg-info bg-opacity-10" : ""}">
                        <div class="card-header ${(i % 2  != 0) ? "bg-white bg-opacity-10" : "bg-white"}">
                            <div class="row">
                                <div class="col-11">
                                    <h4 class="mt-2">คำร้องขอโอนพัสดุจาก : ${response.data[i].tb_transfer_belongs_to_tb_warehouse_info_req.warehouseName}</h4>
                                </div>
                                <div class="col-1">
                                    <button id="delete_transfer" type="button" class="btn btn-outline-danger float-end  rounded-5rem ms-4 pt-0 pb-1" style="border: 0px solid; font-size:1.5rem" value="${i}">ยกเลิก</button>
                                </div>             
                            </div>
                        
                            <h4 id="transferId${i}" style="display:none;">${response.data[i].transferId}</h4>
                            <h4 id="warehouseIdEquipOwner${i}" style="display:none;">${response.data[i].warehouseIdEquipOwner}</h4>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table" id="transfer_transfer_list${i}">
                                    <thead>
                                        <tr>
                                            <th class="text-center">ลำดับ</th>
                                            <th>รหัสพัสดุ</th>
                                            <th>รายการ</th>
                                            <th class="text-center">หน่วยนับ</th>
                                            <th class="text-center">ความต้องการพัสดุ</th>
                                            <th class="text-center">จำนวนพัสดุคงคลัง</th>
                                            <th class="text-center">จำนวน</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        ${transfer_list_table}      

                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive">
                            <!--    <button id="create_pdf" class="btn btn-success bi bi-filetype-pdf float-end me-2 rounded-circle" value="${i}" style="font-size: 1.6rem;"></button> -->
                            <!--    <button id="save_data" class="btn btn-primary bi bi-journal-check float-end me-2 rounded-circle" value="${i}" style="font-size: 1.6rem;"></button> -->

                                <button id="create_pdf" class="btn btn-success bi bi-filetype-pdf float-end me-2 " value="${i}" > อนุมัติและสร้างเอกสาร</button> 
                                <button id="save_data" class="btn btn-primary bi bi-journal-check float-end me-2 " value="${i}" > บันทึก</button> 
                            </div>
                            <!-- <div class="row mt-2">
                                <div class="col-3">
                                
                                </div> 
                                <div class="col-9">
                                    <iframe id="myIframe" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="850px" style="display:none;"></iframe>
                                </div>                           
                            </div> -->                            
                        </div>
                    </div>`
                )
            } catch (error) {
                
            }
        }

        $( ":input[type='number']" ).keyup(function() {
            let thisValue = parseInt($(this).val())
            let thisMaxValue = parseInt($(this).attr('max'))
            let thisMinValue = parseInt($(this).attr('min'))
            // console.log(thisValue)
            if(thisValue > thisMaxValue ){
                $(this).val(thisMaxValue)
            }
        });
    
        // <!-- save and send transfer request --> ต้องสร้าง function ของปุ่ม ขึ้นมาพร้อมกับ element ต่างๆ
        // $(document).ready(function(){
            $('div.card-body').on('click', 'button#save_data' , function () {
                let tableID = $(this).val()
                let transferId = $(`#transferId${tableID}`).html()
                // console.log(tableID)
                // loop over each table row (tr)
                $(`#transfer_transfer_list${tableID} tr`).each(function(){
                        let currentRow=$(this);
                    
                        let transferListId=currentRow.find("td:eq(0)").text();
                        let newTransferListValue=currentRow.find("td:eq(7) input[type='number']").val();

                        newTransferListValue = newTransferListValue < 1 ? 1 : currentRow.find("td:eq(7) input[type='number']").val();

                        var settings = {
                            "url": "/api/tb_transfer_list/edit_tb_transfer_list_value/",
                            "method": "POST",
                            "timeout": 0,
                            "headers": {
                                "Content-Type": "application/x-www-form-urlencoded"
                            },
                            "data": {
                                "transferListId": transferListId,
                                "newEquipmentValue": newTransferListValue
                            }
                        };

                        $.ajax(settings).done(function (response) {
                            create_transfer_card()
                        });
                });

                // let settings = {
                //     "url": "/api/tb_transfer/edit_tb_transfer_status",
                //     "method": "POST",
                //     "data": {
                //         "transferId": transferId,
                //         "transferStatus" : 2
                //     }
                // };

                // $.ajax(settings).done(function (response) {
                //     create_transfer_card()
                // })

                // let saveTransferData = new bootstrap.Modal(document.getElementById('save-transfer-data'))
                // saveTransferData.show()
                $('#save-transfer-data').modal('toggle');
                setTimeout(function(){$('#save-transfer-data').modal('hide')},1200);

                // alert("Save AND Send")

            })
        // })

        // <!-- delete transfer list --> ต้องสร้าง function ของปุ่ม ขึ้นมาพร้อมกับ element ต่างๆ
        // $(document).ready(function(){
            $('table').on('click', 'button#delete_transfer_list' , function () {
                let cellData = $(this).closest('tr').find('td')
                let transferListId = cellData[7].innerText
                let settings = {
                    "url": `/api/tb_transfer_list/${transferListId}`,
                    "method": "DELETE",
                };

                $.ajax(settings).done(function (response) {
                    
                    let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
                    let settings = {
                        "url": "/api/tb_transfer/select_transfer_owner",
                        "method": "POST",
                        "data": {
                            "warehouseIdOwner": Bacode0,
                            "transferStatus" : 3
                        }
                    };

                    $.ajax(settings).done(function (response) {

                        create_transfer_card()
                    
                    });

                });
            })
        // })

        // <!-- Create PDF transfer request --> ต้องสร้าง function ของปุ่ม ขึ้นมาพร้อมกับ element ต่างๆ
        // $(document).ready(function(){
            $('div.card-body').on('click', 'button#create_pdf' , function () {
                let cardID = $(this).val()
                // console.log(cardID)
                // loop over each table row (tr)
                let transferId = $(`#transferId${cardID}`).html()
                let warehouseIdEquipOwner = $(`#warehouseIdEquipOwner${cardID}`).html()

                // let rejectTransferEquipment = new bootstrap.Modal(document.getElementById('pdf-create-transfer-equipment'))
                // rejectTransferEquipment.show()   
                $('#pdf-create-transfer-equipment').modal('toggle');

                $('#pdf-create-transfer-equipment-btn').on('click', function () {

                    $(`#transfer_transfer_list${cardID} tr`).each(function(){
                        let currentRow=$(this);
                    
                        let transferListId=currentRow.find("td:eq(0)").text();
                        let newTransferListValue=currentRow.find("td:eq(6) input[type='number']").val();

                        newTransferListValue = newTransferListValue < 1 ? 1 : currentRow.find("td:eq(6) input[type='number']").val();

                        var settings = {
                            "url": "/api/tb_transfer_list/edit_tb_transfer_list_value/",
                            "method": "POST",
                            "timeout": 0,
                            "headers": {
                                "Content-Type": "application/x-www-form-urlencoded"
                            },
                            "data": {
                                "transferListId": transferListId,
                                "newEquipmentValue": newTransferListValue
                            }
                        };

                        $.ajax(settings).done(function (response) {
                            // let settings = {
                            //     "url": "/api/tb_transfer/edit_tb_transfer_status",
                            //     "method": "POST",
                            //     "data": {
                            //         "transferId": transferId,
                            //         "transferStatus" : 3
                            //     }
                            // };
                            
                            // $.ajax(settings).done(function (response) {
                            //     // console.log(response);
                            //     window.open(`../pdf/form.php?transferId=${transferId}&warehouseOwnName=${warehouseIdEquipOwner}`)
                            //     create_transfer_card()
                            //     getNotiTransferCount(Bacode0)
                            //     getNotiTransferCountOwner(Bacode0)
                            //     // let url = `../pdf/form.php?transferId=${transferId}&warehouseOwnName=${warehouseIdEquipOwner}`
                            //     // myIframe.src = url
                            //     // $("#myIframe").attr("style", "display:block");
                            //     $('#pdf-create-transfer-equipment').modal('toggle');
                            //     // setTimeout(function(){$('#pdf-create-transfer-equipment').modal('hide')},1200);
                            // })
                        });
                    });

                    let settings = {
                        "url": "/api/tb_transfer/edit_tb_transfer_status",
                        "method": "POST",
                        "data": {
                            "transferId": transferId,
                            "transferStatus" : 3
                        }
                    };

                    $.ajax(settings).done(function (response) {
                        create_transfer_card()
                        window.open(`../pdf/form.php?transferId=${transferId}&warehouseOwnName=${warehouseIdEquipOwner}`)
                        // let url = `../pdf/form.php?transferId=${transferId}&warehouseOwnName=${warehouseIdEquipOwner}`
                        // myIframe.src = url
                        // $("#myIframe").attr("style", "display:block");
                        $('#pdf-create-transfer-equipment').modal('toggle');
                    })

                })

                // $('#pdf-create-transfer-equipment').on('click', 'button#cancel-create-transfer-equipment-btn', function () {
                //     $('#pdf-create-transfer-equipment').modal('toggle');
                //     cardID =''
                //     transferId =''
                //     settings =''
                //     warehouseIdEquipOwner=''
                // })

                // console.log(response)
                // window.open(`../pdf/form.php?transferId=${transferId}&warehouseOwnName=${warehouseIdEquipOwner}`)
                

            })
        // })

        // button reject click
        // $(document).ready(function(){
            $('#delete_transfer').on('click', function () {
                let cardID = $(this).val()
                let transferId = $(`#transferId${cardID}`).html()
                console.log(transferId)
                let rejectTransferEquipment = new bootstrap.Modal(document.getElementById('reject-alert-transfer-equipment'))
                rejectTransferEquipment.show()   

                $('#reject-alert-transfer-equipment-btn').on('click', function () {

                    let settings = {
                        "url": "/api/tb_transfer/edit_tb_transfer_status",
                        "method": "POST",
                        "data": {
                            "transferId": transferId,
                            "transferStatus" : 4
                        }
                    };

                    $.ajax(settings).done(function (response) {
                        create_transfer_card()

                        $('#reject-alert-transfer-equipment').modal('toggle');
                        setTimeout(function(){$('#reject-alert-transfer-equipment').modal('hide')},1200);
                    })

                })

                // $('#reject-alert-transfer-equipment').on('click', 'button#cancel-alert-transfer-equipment-btn', function () {
                //     $('#reject-alert-transfer-equipment').modal('toggle');
                //     cardID =''
                //     transferId =''
                //     settings =''
                // })
            })
        // })

    });
}
</script>

<script>
    function create_transfer_history_table(transferStatus){
        function subTable_set ( d ) {
            // `d` is the original data object for the row
            let transfer_list_item_count = d.tb_transfer_tb_transfer_list_associate.length
                let transfer_list_item_table=`<tr>
                                                <td class="text-center">ลำดับ</td>
                                                <td>รหัสพัสดุ</td>
                                                <td>รายการ</td>
                                                <td class="text-end">จำนวน</td>
                                                <td class="text-center">หน่วยนับ</td>
                                            </tr>`
                for(j=0;j<transfer_list_item_count;j++){
                    if(d.transfer_list_item_count != 0){
                        transfer_list_item_table+=
                        `<tr>
                            <td class="text-center">${j+1}</td>
                            <td>${d.tb_transfer_tb_transfer_list_associate[j].equipmentId}</td>
                            <td>${d.tb_transfer_tb_transfer_list_associate[j].transfer_list_belongs_to_tb_displayed_safety.tb_displayed_safety_belongs_to_tb_all_stuff.stuffNameTh}</td>
                            <td class="text-end">${d.tb_transfer_tb_transfer_list_associate[j].equipmentValue}</td>
                            <td class="text-center">${d.tb_transfer_tb_transfer_list_associate[j].transfer_list_belongs_to_tb_displayed_safety.tb_displayed_safety_belongs_to_tb_all_stuff.counter}</td>
                        </tr>`
                    }
                }

            return `<table class="table" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                        
                        

                            ${transfer_list_item_table}

                        
                    </table>`;
        }

        $("div#transfer_list").empty()
        $("div#transfer_list").append(`
            <table id="transfer_history_list" class="display" style="width:100%">
                
            </table>
        `)
        
        let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        $.fn.dataTable.moment( 'DD/MM/YYYY เวลา HH:mm น.' ); // sort by moment datetime
        let table = $('#transfer_history_list').DataTable( {
            ajax: {
                url: '/api/tb_transfer/select_transfer_owner',
                type: "POST",
                data:{
                    warehouseIdOwner: Bacode0,
                    transferStatus: transferStatus
                }
            },
            lengthChange: false,
            destroy: true,
            columns: [    
                {
                    className:      'dt-control',
                    orderable:      false,
                    data:           null,
                    defaultContent: ''
                },            
                { 
                    title:"คลังพัสดุ",
                    data: "tb_transfer_belongs_to_tb_warehouse_info_req.warehouseName" 
                },            
                { 
                    title:"จำนวนรายการพัสดุ",
                    data: "tb_transfer_tb_transfer_list_associate.length" 
                },
                { 
                    title:"วันที่แจ้งเตือน",
                    data: null,
                    render : function(data, type, row) {
                        return moment(new Date(data.createdAt), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                    }
                },
                {
                    title:"วันที่คลังพัสดุรับทราบ",
                    data: null ,
                    className:"text-center",
                    render : function(data, type, row) { 
                        if(data["transferStatus"]=="2"){
                            return "-"
                        }else if(data["transferStatus"]=="3"){
                            return moment(new Date(data["updatedAt"]), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                        }else if(data["transferStatus"]=="4"){
                            return moment(new Date(data["updatedAt"]), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                        }else if(data["transferStatus"]=="5"){
                            return moment(new Date(data["updatedAt"]), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                        }             
                    }
                },
                {
                    title:"สถานะการขอรับโอน",
                    data: null ,
                    className:"text-center",
                    render : function(data, type, row) { 
                        if(data["transferStatus"]=="2"){
                            return `<span class="text-black rounded-3 px-3" style="background-color:#b7b7b7">รอยืนยันสถานะ</span>`
                        }else if(data["transferStatus"]=="3"){
                            return `<span class="text-black rounded-3 px-3" style="background-color:#93dbb7">ดำเนินการสร้างเอกสาร</span>`
                        }else if(data["transferStatus"]=="4"){
                            return `<span class="text-black rounded-3 px-3" style="background-color:#e688a9">ปฏิเสธ</span>`
                        }else if(data["transferStatus"]=="5"){
                            return `<span class="text-black rounded-3 px-3" style="background-color:#9ffbf9">ยืนยันรับของ</span>`
                        }                 
                    }
                }, 
                
                
                
            ],
            order: [[3, 'desc']]
        } );
        
        // Add event listener for opening and closing details
        $('#transfer_history_list tbody').on('click', 'td.dt-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( subTable_set(row.data()) ).show();
                tr.addClass('shown');
            }
        } );

    }
</script>


<script>
$('input[type=radio][name=inlineRadioOptions]').on('change', function() {
  switch ($(this).val()) {
    case '3':
        create_transfer_card()
        break;
    case '5':
        create_transfer_history_table(5)
        break;
  }
});
</script>



<!-- get noti transfer count number -->
<script>
    // $(document).ready(function(){
    //     let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
    //     getNotiTransferCount(Bacode0)

        function getNotiTransferCount(Bacode0){
            let settings = {
                "url": "/api/tb_transfer/select_count_transfer/",
                "method": "POST",            
                "data": {
                    "warehouseIdRequester": Bacode0,
                    "transferStatus": 1
                },
            };
            $.ajax(settings).done(function (response_transfer_count) {
                let NotiTransferCount = response_transfer_count.total
                if(NotiTransferCount !=0){
                    $("#transferNotice").html(NotiTransferCount)
                    $("#transferNotice").attr("style", "display:block")
                    $("#side_noti_transfer_req").html(NotiTransferCount)
                    $("#side_noti_transfer_req").show()
                }
                else{
                    $("#transferNotice").attr("style", "display:none")
                    $("#side_noti_transfer_req").hide()
                }
            });
        }    
    // })
</script>

<!-- get noti transfer count number for owner -->
<script>
    // $(document).ready(function(){
    //     let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
    //     getNotiTransferCountOwner(Bacode0)

        function getNotiTransferCountOwner(Bacode0){
            let settings = {
                "url": "/api/tb_transfer/select_count_transfer_owner/",
                "method": "POST",            
                "data": {
                    "warehouseIdOwner": Bacode0,
                    "transferStatus": 2
                },
            };
            $.ajax(settings).done(function (response_transfer_count_owner) {
                let NotiTransferCountOwner = response_transfer_count_owner.total
                if(NotiTransferCountOwner !=0){
                    $("#side_noti_transfer_own").html(NotiTransferCountOwner)
                    $("#side_noti_transfer_own").show()
                }
                else{
                    $("#side_noti_transfer_own").hide()
                }
            });
        }    
    // })
</script>


<!-- call function Show transfer group -->
<script>
    let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
    create_transfer_card()
    getNotiTransferCount(Bacode0)
    getNotiTransferCountOwner(Bacode0)
</script>

<script>
    $('#reject-alert-transfer-equipment').on('hide.bs.modal', function(event)
    {
        let confirmEquipTransfer = $(this).find('#reject-alert-transfer-equipment-btn');
        confirmEquipTransfer.unbind("click");
        // console.log("clear")
    });
</script>

<script>
    $('#pdf-create-transfer-equipment').on('hide.bs.modal', function(event)
    {
        let confirmEquipTransfer = $(this).find('#pdf-create-transfer-equipment-btn');
        confirmEquipTransfer.unbind("click");
        // console.log("clear")
    });
</script>


