<!doctype html>
<html lang="en">

<head>
    <?php
include '../include/head.php';
?>
    <title>เข้าสู่ระบบวางแผนและจัดการพัสดุ (งบผู้ใช้ไฟ) แบบเบ็ดเสร็จ</title>
</head>

<body>
    <div class="container-fluid">
        <div class="row flex-nowrap bg-dark">
            <?php
include '../include/sidemenu.php';
?>

            <div class="col p-0 mt-1 bg-light">
                <?php
include '../include/navbar.php';
?>
                <!-- content start-->
                <div class="p-4">
                    <h3>ประวัติรายการจัดซื้อพัสดุ</h3>
                </div>
                <input type="hidden" id="finalPurchaseIdKey">
                <input type="hidden" id="purchaseId">
                <div class="container">
                    <div class="row mt-2">
                        <div class="card">
                            <div class="card-body">
                                <table id="example" class="display" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center">วันที่ทำรายการ</th>
                                            <th class="text-center">ประเภทงาน</th>
                                            <th class="text-center">เงินงบประมาณ</th>
                                            <th class="text-center">จำนวนรายการ</th>
                                            <th class="text-center">เพื่อใช้งาน</th>
                                            <th class="text-center">วันที่สร้างใบ PR</th>
                                            <th class="text-center">หมายเหตุ</th>
                                            <th class="text-center">#</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">รายละเอียดการจัดซื้อ</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="row mt-2">
                                <div class="col">
                                    <h5>เพื่อใช้งาน :</h5>
                                </div>
                                <div class="col" id="middleJobNumber"></div>
                            </div>
                            <div class="row mt-2">
                                <div class="col">
                                    <h5>ประเภทเอกสาร :</h5>
                                </div>
                                <div class="col" id="approvalNumber"></div>
                            </div>
                            <div class="row mt-2">
                                <div class="col">
                                    <h5>วงเงินงบประมาณ :</h5>
                                </div>
                                <div class="col" id="approvalBudget"></div>
                            </div>
                            <hr>
                            <div class=" row mt-2">
                                <h5>ประเภทเอกสาร</h5>
                            </div>
                            <div class=" row mt-2">
                                <div class="col">
                                    <input class="form-check-input" type="radio" name="DocumentType" id="DocumentType1"
                                        value="PR มาตรฐาน (ZNB1)" checked>
                                    <label class="form-check-label" for="DocumentType1">
                                        PR มาตรฐาน (ZNB1)
                                    </label>
                                </div>
                                <div class="col">
                                    <input class="form-check-input" type="radio" name="DocumentType" id="DocumentType"
                                        value="PR จาก MRP (ZNB2)">
                                    <label class="form-check-label" for="DocumentType">
                                        PR จาก MRP (ZNB2)
                                    </label>
                                </div>
                                <div class="col">
                                    <input class="form-check-input" type="radio" name="DocumentType" id="DocumentType"
                                        value="PR จากปฏิบัติงาน (ZNB3)">
                                    <label class="form-check-label" for="DocumentType">
                                        PR จากปฏิบัติงาน (ZNB3)
                                    </label>
                                </div>
                                <div class="col">
                                    <input class="form-check-input" type="radio" name="DocumentType" id="DocumentType"
                                        value="PR สัญญาล่วงหน้า (ZRV1)">
                                    <label class="form-check-label" for="DocumentType">
                                        PR สัญญาล่วงหน้า (ZRV1)
                                    </label>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col">
                                    <h5>หมวดรายการ (Item Category)</h5>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col">
                                    <input class="form-check-input" type="radio" name="itemCategory" id="itemCategory1"
                                        value="มาตรฐาน ()" checked>
                                    <label class="form-check-label" for="itemCategory1">
                                        มาตรฐาน ()
                                    </label>
                                </div>
                                <div class="col">
                                    <input class="form-check-input" type="radio" name="itemCategory" id="itemCategory1"
                                        value="การรับเหมาช่วง(L)">
                                    <label class="form-check-label" for="itemCategory1">
                                        การรับเหมาช่วง(L)
                                    </label>
                                </div>
                                <div class="col"></div>
                                <div class="col"></div>
                                <div class="col"></div>
                                <div class="col"></div>
                            </div>
                            <div class="row mt-2">
                                <div class="col">
                                    <h5>หมวดการกำหนดบัญชี (Account Assignment Category ;A)</h5>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col">
                                    <input class="form-check-input" type="checkbox" value="พัสดุสำรองคลัง ( )"
                                        name="accountAssignmentCat">
                                    <label class="form-check-label" for="accountAssignmentCat">
                                        พัสดุสำรองคลัง ( )
                                    </label>
                                </div>
                                <div class="col">
                                    <input class="form-check-input" type="checkbox" value="คชจ.เข้าหน่วยงาน (K)"
                                        name="accountAssignmentCat">
                                    <label class="form-check-label" for="accountAssignmentCat">
                                        คชจ.เข้าหน่วยงาน (K)
                                    </label>
                                </div>
                                <div class="col">
                                    <input class="form-check-input" type="checkbox" value="ทรัพย์สินถาวรพร้อมใช้งาน (Z)"
                                        name="accountAssignmentCat">
                                    <label class="form-check-label" for="accountAssignmentCat">
                                        ทรัพย์สินถาวรพร้อมใช้งาน (Z)
                                    </label>
                                </div>
                                <div class="col">
                                    <input class="form-check-input" type="checkbox" value="งานจ้างเหมาเบ็ดเสร็จ (P)"
                                        name="accountAssignmentCat">
                                    <label class="form-check-label" for="accountAssignmentCat">
                                        งานจ้างเหมาเบ็ดเสร็จ (P)
                                    </label>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col">
                                    <input class="form-check-input" type="checkbox" value="พัสดุงานโครงการที่มีแผน (Q)"
                                        name="accountAssignmentCat">
                                    <label class="form-check-label" for="accountAssignmentCat">
                                        พัสดุงานโครงการที่มีแผน (Q)
                                    </label>
                                </div>
                                <div class="col">
                                    <input class="form-check-input" type="checkbox"
                                        value="คชจ.เข้าใบสั่งซ่อม/งานบริการ (F)" name="accountAssignmentCat">
                                    <label class="form-check-label" for="accountAssignmentCat">
                                        คชจ.เข้าใบสั่งซ่อม/งานบริการ (F)
                                    </label>
                                </div>
                                <div class="col">
                                    <input class="form-check-input" type="checkbox" value="งานจ้างเหมาบางส่วน (N)"
                                        name="accountAssignmentCat">
                                    <label class="form-check-label" for="accountAssignmentCat">
                                        งานจ้างเหมาบางส่วน (N)
                                    </label>
                                </div>
                                <div class="col"></div>
                            </div>
                            <div class="row mt-2">
                                <div class="col text-end">เลขที่ใบขอเสนอซื้อ/จ้าง (PR) :</div>
                                <div class="col">
                                    <input type="text" class="form-control" id="prDocNumber">
                                </div>
                                <div class="col"></div>
                            </div>
                            <div class="row mt-2">
                                <div class="card">
                                    <div class="card-body">
                                        <table id="example1" class="table table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">ลำดับ</th>
                                                    <th class="text-center">รหัสพัสดุ</th>
                                                    <th class="text-center">รายการ</th>
                                                    <th class="text-center">คุณลักษณะ (Spec No.)</th>
                                                    <th class="text-center">หน่วยนับ</th>
                                                    <th class="text-center">จำนวน</th>
                                                    <th class="text-center">ราคากลาง</th>
                                                    <th class="text-center">เงินงบประมาณ</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col text-end">ผู้อนุมัติ :</div>
                                <div class="col">
                                    <input type="text" class="form-control" id="approvalName">
                                </div>
                                <div class="col text-end">ตำแหน่งผู้อนุมัติ :</div>
                                <div class="col">
                                    <input type="text" class="form-control" id="approvalTitle">
                                </div>
                                <div class="col"></div>
                                <div class="col"></div>
                            </div>
                            <div class="row mt-2">
                                <div class="col text-end">ผู้บันทึก :</div>
                                <div class="col">
                                    <input type="text" class="form-control" id="recorderName">
                                </div>
                                <div class="col text-end">ตำแหน่งผู้บันทึก :</div>
                                <div class="col">
                                    <input type="text" class="form-control" id="recorderTitle">
                                </div>
                                <div class="col"></div>
                                <div class="col"></div>
                            </div>
                            <div class="row mt-2">
                                <div class="col text-end text-danger">หมายเหตุ :</div>
                                <div class="col-4">
                                    <input type="text" class="form-control" id="note">
                                </div>
                                <div class="col"></div>
                                <div class="col"></div>
                                <div class="col"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="savePR">บันทึก</button>
                            <button type="button" class="btn btn-success" id="getPR">สร้างใบ PR</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content end-->
        </div>
    </div>
    </div>

</body>

</html>


<!-- Login check -->

<script>
$(document).ready(function() {

    let Username = JSON.parse(localStorage.getItem("data"));
    $.ajax({
        type: "POST",
        url: "http://localhost:3000/tb_purchase/purchase_by_user",
        data: {
            userId: Username.userLoginDataResponce.Username
        },
        success: function(data) {
            var table = $('#example').DataTable({
                "responsive": true,
                "scrollX": true,
                select: true,
                ajax: {
                    "url": "http://localhost:3000/tb_purchase/purchase_by_user_pr",
                    "type": "POST",
                    "data": {
                        userId: Username.userLoginDataResponce.Username
                    },
                },
                "columns": [{
                        "data": "purchaseId",
                        "className": "text-center",
                    },
                    {
                        "data": "middleJobNumber",
                        "className": "text-center",
                    },
                    {
                        "data": "approvalBudget",
                        "className": "text-center",
                    },
                    {
                        "data": null,
                        "render": function(data) {
                            var cc = data.tb_purchase_tb_purchase_list_associate
                                .length;
                            return cc;
                        },
                        "className": "text-center",
                    },
                    {
                        "data": "toUse",
                        "className": "text-center",
                    },
                    {
                        "data": null,
                        'render': function(data) {
                            return '<button id="' + data
                                .purchaseId +
                                '" class="onePdfButton btn btn-success" value="' +
                                data.purchaseId +
                                '" ><i class="bi bi-file-earmark-pdf"></i>PDF ใบปะหน้า</button>'
                        },
                        "className": "text-center",
                    },
                    {
                        "data": "purchase_belongs_to_tb_final_purchase.note",
                        "className": "text-center",
                    },
                    {
                        "data": null,
                        'render': function(data) {
                            return '<button id="' + data
                                .purchaseId +
                                '" class="showButton btn btn-danger" value="' +
                                data.purchaseId +
                                '" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="bi bi-file-earmark-pdf"></i>PDF</button>'
                        },
                        "className": "text-center",
                    },
                ],
            }).columns.adjust();
        }
    });


    $('#example').on('click', 'button.onePdfButton', function() {
        var purchaseId = $(this).attr('id');
        window.open(
            `../pdf/form2.php?purchaseId=${purchaseId}`
        )
    });

    $('#example').on('click', 'button.showButton', function() {
        var table1 = $('#example1').DataTable().columns.adjust();
        table1.destroy();
        var id = $(this).attr('id');
        // $('#exampleModalLabel').text(id);

        $.ajax({
            type: "GET",
            url: "http://localhost:3000/tb_purchase/get_purchase_by_id?purchase_id=" + id,
            success: function(data) {
                $("#middleJobNumber").text(data.data.middleJobNumber);
                $("#approvalNumber").text(data.data.approvalNumber);
                $("#approvalBudget").text(data.data.approvalBudget);
                $('#finalPurchaseIdKey').val(data.data.finalPurchaseIdKey);
                $('#purchaseId').val(data.data.purchaseId);
                $('#prDocNumber').val(data.data.purchase_belongs_to_tb_final_purchase
                    .prDocNumber);
                $('#approvalName').val(data.data.purchase_belongs_to_tb_final_purchase
                    .approvalName);
                $('#approvalTitle').val(data.data.purchase_belongs_to_tb_final_purchase
                    .approvalTitle);
                $('#recorderName').val(data.data.purchase_belongs_to_tb_final_purchase
                    .recorderName);
                $('#recorderTitle').val(data.data.purchase_belongs_to_tb_final_purchase
                    .recorderTitle);
                $('#note').val(data.data.purchase_belongs_to_tb_final_purchase
                    .note);
                $("#getPR").val(data.data.purchaseId);

                $('input:radio[name="DocumentType"]').filter('[value="' + data.data
                    .purchase_belongs_to_tb_final_purchase.documentType + '"]').attr(
                    'checked',
                    true);

                $('input:radio[name="itemCategory"]').filter('[value="' + data.data
                    .purchase_belongs_to_tb_final_purchase.itemCategory + '"]').attr(
                    'checked',
                    true);

                var checkCat = data.data.purchase_belongs_to_tb_final_purchase.accountAssignmentCat
                // console.log(checkCat)
                if(!checkCat){
                    $('input:checkbox[name="accountAssignmentCat"]').attr(
                            'checked',
                            false);
                }else {
                    checkCat = data.data
                        .purchase_belongs_to_tb_final_purchase.accountAssignmentCat.split(",");

                    $.each(checkCat, function(i) {
                        $('input:checkbox[name="accountAssignmentCat"]').filter(
                            '[value="' + checkCat[i] +
                            '"]').attr(
                            'checked',
                            true);
                    });
                }
                

                var table = $('#example1').DataTable({
                    "aaData": data.data.tb_purchase_tb_purchase_list_associate,
                    "columns": [{
                            "data": null,
                            "render": function(data, type, full, meta) {
                                return meta.row + 1;
                            },
                            "className": "text-center",
                        }, {
                            "data": "purchase_list_stuff_belongs_to_mb52.equipmentId",
                            "className": "text-center",
                        },
                        {
                            "data": "purchase_list_stuff_belongs_to_mb52.stuffName",
                            "className": "text-center",
                        },
                        {
                            "data": "specNo",
                            "className": "text-center",
                        },
                        {
                            "data": "purchase_list_stuff_belongs_to_mb52.counter",
                            "className": "text-center",
                        },
                        {
                            "data": "equipmentValue",
                            "className": "text-center",
                        },
                        {
                            "data": "middlePrice",
                            "className": "text-center",
                        },
                        {
                            "data": "budgetPrice",
                            "className": "text-center",
                        },
                    ],
                }).columns.adjust();
                $("#getPR").click(function() {
                    var purchaseId = $("#purchaseId").val();
                    window.open(
                        `../pdf/formPR.php?purchaseId=${purchaseId}`
                    )
                });
            }
        })

    });

    $('#savePR').on('click', function() {
        var documentType = $("input[name=DocumentType]:checked").val();
        var itemCategory = $("input[name=itemCategory]:checked").val();
        var accountAssignmentCat = $("input[name=accountAssignmentCat]:checked").map(function(_, el) {
            return $(el).val();
        }).get();
        let accountAssignmentCattext = accountAssignmentCat.toString();
        var prDocNumber = $('#prDocNumber').val();
        var approvalName = $('#approvalName').val();
        var approvalTitle = $('#approvalTitle').val();
        var recorderName = $('#recorderName').val();
        var recorderTitle = $('#recorderTitle').val();
        var note = $('#note').val();
        var finalPurchaseIdKey = $('#finalPurchaseIdKey').val();

        const dataEditPR = {
            finalPurchaseId: finalPurchaseIdKey,
            documentType: documentType,
            itemCategory: itemCategory,
            accountAssignmentCat: accountAssignmentCattext,
            approvalName: approvalName,
            approvalTitle: approvalTitle,
            recorderName: recorderName,
            recorderTitle: recorderTitle,
            note: note,
            prDocNumber: prDocNumber
        }

        $.ajax({
            type: "POST",
            url: "http://localhost:3000/tb_final_purchase/edit_tb_final_purchase",
            dataType: "json",
            data: dataEditPR,
            success: function(data) {
                // console.log(data);
                if (data.message == 'แก้ไขข้อมูลสำเร็จ') {
                    Swal.fire({
                        title: "บันทึกข้อมูลสำเร็จ",
                        icon: 'success',
                    })
                } else {
                    Swal.fire({
                        title: "ระบบผิดพลาด",
                        icon: 'error',
                    })
                }
            }
        })
    });
});
</script>
<!-- Login check -->