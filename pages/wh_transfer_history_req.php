<!doctype html>
<html lang="en">
<head>
    <?php 
        include ('../include/head.php');
    ?>
    <title>เข้าสู่ระบบวางแผนและจัดการพัสดุ (งบผู้ใช้ไฟ) แบบเบ็ดเสร็จ</title>
</head>

<body>  
    <div class="container-fluid">        
        <div class="row flex-nowrap bg-dark">           
            <?php 
                include ('../include/sidemenu.php');
            ?>            
                      
            <div class="col p-0 mt-1 bg-light" >
            <?php 
                include ('../include/navbar.php');
            ?>            
            <!-- content start-->  
            <div class= "p-4">
                <h3>ประวัติการขอรับโอนพัสดุ</h3>                
                <div class="mt-4">    
                    <!-- <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="history1" value="1" checked>
                        <label class="form-check-label" for="history1">ประวัติการขอรับโอนพัสดุทั้งหมด</label>
                    </div> -->
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="history2" value="2" checked>
                        <label class="form-check-label" for="history2">รอยืนยันสถานะ</label>
                    </div>               
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="history3" value="3">
                        <label class="form-check-label" for="history3">ยินยอม</label>
                    </div>   
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="history4" value="4">
                        <label class="form-check-label" for="history4">ปฏิเสธ</label>
                    </div>   
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="history5" value="5">
                        <label class="form-check-label" for="history5">ยืนยันรับของ</label>
                    </div>  
                </div>
                <div id="transfer_list">

                </div>
            </div>
            <!-- content end-->
            </div>
            
        </div>
    </div>
    
</body>
</html>


<!-- Login check -->
<script>
    // let data = JSON.parse(localStorage.getItem("data"))
    console.log(data)
    if(data===null)
    {
        window.location.replace("./login.php");
        if(data.LoginResponse.ResponseCode != "WSV0000"){
            window.location.replace("./login.php");
        } 
    }
       
</script>
<!-- Login check -->

<!-- Hide Notice button on navbar -->
<script>
    $("#transferNoticeButton").attr("style", "display:none");
    $("#NoticeButton").attr("style", "display:none");
</script>



<!-- remove and add class active -->
<script>
    $(".nav-link").even().removeClass("active");
    // $("#cs_noti_to_wh").even().addClass("active");
</script>

<!-- function create card notice to warehouse -->
<script>

function create_transfer_history_table(transferStatus){
    function subTable_set ( d ) {
        // `d` is the original data object for the row
        // console.log(d)
        let transfer_list_item_count = d.transferListId.length
            let transfer_list_item_table=`<tr>
                                            <td class="text-center">ลำดับ</td>
                                            <td>รหัสพัสดุ</td>
                                            <td>รายการ</td>
                                            <td class="text-end">จำนวน</td>
                                            <td class="text-center">หน่วยนับ</td>
                                        </tr>`
            for(j=0;j<transfer_list_item_count;j++){
                if(d.transfer_list_item_count != 0){
                    transfer_list_item_table+=
                    `<tr>
                        <td class="text-center">${j+1}</td>
                        <td>${d.transferListId[j].equipmentId}</td>
                        <td>${d.transferListId[j].stuffNameTh}</td>
                        <td class="text-end">${d.transferListId[j].equipmentValue}</td>
                        <td class="text-center">${d.transferListId[j].counter}</td>
                    </tr>`
                }
            }

        return `<table class="table" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                    
                    

                        ${transfer_list_item_table}

                    
                </table>`;
    }

    $("div#transfer_list").empty()
    $("div#transfer_list").append(`
        <table id="transfer_history_list" class="display" style="width:100%">
            
        </table>
    `)
    
    let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
    $.fn.dataTable.moment( 'DD/MM/YYYY เวลา HH:mm น.' );
    let table = $('#transfer_history_list').DataTable( {
        ajax: {
            url: '/api/tb_transfer/select_transfer_sql',
            type: "POST",
            data:{
                warehouseIdRequester: Bacode0,
                transferStatus: transferStatus
            }
        },
        lengthChange: false,
        destroy: true,
        columns: [    
            {
                className:      'dt-control',
                orderable:      false,
                data:           null,
                defaultContent: ''
            },            
            { 
                title:"คลังพัสดุ",
                data: null,
                render : function(data, type, row) {
                    return data["transferListId"][0]["own_name"]
                }
            },            
            { 
                title:"จำนวนรายการพัสดุ",
                data: "transferListId.length" 
            },
            { 
                title:"วันที่แจ้งเตือน",
                data: null,
                render : function(data, type, row) {
                    return moment(new Date(data.transferListId[0].createdAt), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                }
            },
            {
                title:"วันที่คลังพัสดุรับทราบ",
                data: null ,
                className:"text-center",
                render : function(data, type, row) { 
                    if(data["transferListId"][0]["transferStatus"]=="2"){
                        return "-"
                    }else if(data["transferListId"][0]["transferStatus"]=="3"){
                        return moment(new Date(data["transferListId"][0]["updatedAt"]), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                    }else if(data["transferListId"][0]["transferStatus"]=="4"){
                        return moment(new Date(data["transferListId"][0]["updatedAt"]), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                    }else if(data["transferListId"][0]["transferStatus"]=="5"){
                        return moment(new Date(data["transferListId"][0]["updatedAt"]), 'YYYY-mm-dd HH:mm').format("DD/MM/YYYY เวลา HH:mm น.")
                    }             
                }
            },
            {
                title:"สถานะการขอรับโอน",
                data: null ,
                className:"text-center",
                render : function(data, type, row) { 
                    if(data["transferListId"][0]["transferStatus"]=="2"){
                        return `<span class="text-black rounded-3 px-3" style="background-color:#b7b7b7">รอยืนยันสถานะ</span>`
                    }else if(data["transferListId"][0]["transferStatus"]=="3"){
                        return `<span class="text-black rounded-3 px-3" style="background-color:#93dbb7">ดำเนินการสร้างเอกสาร</span>`
                    }else if(data["transferListId"][0]["transferStatus"]=="4"){
                        return `<span class="text-black rounded-3 px-3" style="background-color:#e688a9">ปฏิเสธ</span>`
                    }else if(data["transferListId"][0]["transferStatus"]=="5"){
                        return `<span class="text-black rounded-3 px-3" style="background-color:#9ffbf9">ยืนยันรับของ</span>`
                    }                 
                }
            }, 
            
            
            
        ],
        order: [[3, 'desc']]
    } );
    
    // Add event listener for opening and closing details
    $('#transfer_history_list tbody').on('click', 'td.dt-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( subTable_set(row.data()) ).show();
            tr.addClass('shown');
        }
    } );

}



</script>




<!-- call create_notice_card function -->
<script>

$(document).ready(function () {

    create_transfer_history_table(2)

})

$('input[type=radio][name=inlineRadioOptions]').on('change', function() {
  switch ($(this).val()) {
    case '2':
        create_transfer_history_table(2)
        break;
    case '3':
        create_transfer_history_table(3)
        break;
    case '4':
        create_transfer_history_table(4)
        break;
    case '5':
        create_transfer_history_table(5)
        break;
  }
});
    
</script>

<!-- get noti transfer count number -->
<script>
    $(document).ready(function() {
        let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        getNotiTransferCount(Bacode0)

        function getNotiTransferCount(Bacode0) {
            let settings = {
                "url": "/api/tb_transfer/select_count_transfer/",
                "method": "POST",
                "data": {
                    "warehouseIdRequester": Bacode0,
                    "transferStatus": 1
                },
            };
            $.ajax(settings).done(function(response_transfer_count) {
                let NotiTransferCount = response_transfer_count.total
                if (NotiTransferCount != 0) {
                    $("#transferNotice").html(NotiTransferCount)
                    $("#transferNotice").attr("style", "display:block")
                    $("#side_noti_transfer_req").html(NotiTransferCount)
                    $("#side_noti_transfer_req").show()
                } else {
                    $("#transferNotice").attr("style", "display:none")
                    $("#side_noti_transfer_req").hide()
                }
            });
        }
    })
</script>

<!-- get noti transfer count number for owner -->
<script>
    $(document).ready(function() {
        let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
        getNotiTransferCountOwner(Bacode0)

        function getNotiTransferCountOwner(Bacode0) {
            let settings = {
                "url": "/api/tb_transfer/select_count_transfer_owner/",
                "method": "POST",
                "data": {
                    "warehouseIdOwner": Bacode0,
                    "transferStatus": 2
                },
            };
            $.ajax(settings).done(function(response_transfer_count_owner) {
                let NotiTransferCountOwner = response_transfer_count_owner.total
                if (NotiTransferCountOwner != 0) {
                    $("#side_noti_transfer_own").html(NotiTransferCountOwner)
                    $("#side_noti_transfer_own").show()
                } else {
                    $("#side_noti_transfer_own").hide()
                }
            });
        }
    })
</script>