<!doctype html>
<html lang="en">
<head>
    <?php 
        include ('../include/head.php');
    ?>
    <title>เข้าสู่ระบบวางแผนและจัดการพัสดุ (งบผู้ใช้ไฟ) แบบเบ็ดเสร็จ</title>
</head>

<body>   
    <div class="container-fluid">
        <div class="row flex-nowrap bg-dark">
            
            <?php 
                include ('../include/sidemenu.php');
            ?>            
                      
            <div class="col p-0 mt-1 bg-light" >
            <?php 
                include ('../include/navbar.php');
            ?>
            <!-- content start-->  
            <div class="p-4">
                <h3>รายการขอโอนพัสดุ</h3>
                <div id="transfer_list">

                </div>
            </div>
            <!-- content end-->
            </div>
            <!-- modal alert transfer equipment -->
            <div class="modal fade" id="alert-transfer-equipment" data-bs-keyboard="false" tabindex="-1" aria-labelledby="alert-transfer-equipment-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content text-center">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <image src="../images/icons/accept64.png"></image>
                        <h3 class="mt-4">คุณได้ส่งคำร้องในรายการขอรับโอนแล้ว</h3>
                        <h5 id="itemSelect"class="mt-4"></h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ปิด</button>
                    </div>
                    </div>
                </div>
            </div>
            <!-- modal alert transfer equipment -->

            <!-- modal confirm alert transfer equipment -->
            <div class="modal fade" id="reject-alert-transfer-equipment" data-bs-keyboard="false" tabindex="-1" aria-labelledby="reject-alert-transfer-equipment-Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content text-center">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <h3 class="mt-4">ยกเลิกการขอโอนพัสดุ</h3>
                        <h5 id="itemSelect"class="mt-4"></h5>
                    </div>
                    <div class="modal-footer">
                        <button id="reject-alert-transfer-equipment-btn" type="button" class="btn btn-danger me-auto col-5 ms-4" >ยืนยัน</button>
                        <button id="cancel-alert-transfer-equipment-btn" type="button" class="btn btn-secondary col-5 me-4" data-bs-dismiss="modal">ยกเลิก</button>
                    </div>
                    </div>
                </div>
            </div>
            <!-- modal confirm alert transfer equipment -->
        </div>
    </div>
    
</body>
</html>


<!-- Login check -->
<script>
    // let data = JSON.parse(localStorage.getItem("data"))
    console.log(data)
    if(data===null)
    {
        window.location.replace("./login.php");
        if(data.LoginResponse.ResponseCode != "WSV0000"){
            window.location.replace("./login.php");
        } 
    }
    
</script>
<!-- Login check -->



<!-- remove and add class active -->
<script>
    $(".nav-link").even().removeClass("active");
    $("#wh_transfer_req_list").even().addClass("active");
</script>


<!-- function Show transfer group -->
<script>
function create_transfer_card(){
    let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"

    let settings = {
        "url": "/api/tb_transfer/select_transfer_sql",
        "method": "POST",
        "data": {
            "warehouseIdRequester": Bacode0,
            "transferStatus": 1
        }
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
        $("div#transfer_list").empty()
        let transfer_count = response.transferCount
        
        console.log(transfer_count)
        for(i=0;i<transfer_count;i++){
            try {

                let transfer_list_count = response.data[i].transferListId.length
                let transfer_list_table=""
                for(j=0;j<transfer_list_count;j++){
                    if(response.data[i].transferListId.length != 0){
                        const ownStock = (response.data[i].transferListId[j].own_stock)
                        const reqStock = (response.data[i].transferListId[j].req_stock) == null ? 0 : (response.data[i].transferListId[j].req_stock)
                        console.log(ownStock, reqStock)
                        transfer_list_table+=
                        `<tr>
                            <td class="text-center">${j+1}</td>
                            <td>${response.data[i].transferListId[j].equipmentId}</td>
                            <td>${response.data[i].transferListId[j].stuffNameTh}</td>
                            <td>${response.data[i].transferListId[j].counter}</td>
                            <td>${(reqStock).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td>${(ownStock).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                            <td><input class="text-center" type="number" value="${response.data[i].transferListId[j].equipmentValue}" min="0" max="${ownStock}" style="width: 5em" oninput="this.value = Math.abs(this.value)"></input></td>
                            <td>
                                <button id="delete_transfer_list" class="btn btn-white text-danger bi bi-trash-fill" style="font-size: 1.1rem;">ลบ</button>
                            </td>
                            <td style="display:none;">${response.data[i].transferListId[j].transferListId}</td>
                        </tr>`
                    }
                }

                $("div#transfer_list").append(
                    `<div class="card mt-3 ${(i % 2  != 0) ? "bg-info bg-opacity-10" : ""}">
                        <div class="card-header ${(i % 2  != 0) ? "bg-white bg-opacity-10" : "bg-white"}">
                            <div class="row">
                                <div class="col-11">
                                    <h4 class="mt-2">รายการขอโอนพัสดุไปยังคลังพัสดุ : ${response.data[i].transferListId[0].own_name}</h4>
                                    <h4 id="transferId${i}" class="mt-2" style="display:none;">${response.data[i].transferId}</h4>
                                </div>
                                <div class="col-1">
                                    <button id="delete_transfer" type="button" class="btn btn-outline-danger float-end rounded-5rem ms-4 pt-0 pb-1" style="border: 0px solid; font-size:1.5rem" value="${i}">ยกเลิก</button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table" id="transfer_transfer_list${i}">
                                    <thead>
                                        <tr>
                                            <th class="text-center">ลำดับ</th>
                                            <th>รหัสพัสดุ</th>
                                            <th>รายการ</th>
                                            <th>หน่วยนับ</th>
                                            <th>จำนวนพัสดุคงคลัง</th>
                                            <th>จำนวนพัสดุคงคลัง ${response.data[i].transferListId[0].own_name}</th>
                                            <th><p class="ms-3">จำนวน</p></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        ${transfer_list_table}      

                                    </tbody>
                                </table>
                            </div>

                            <button id="save_data" class="btn btn-primary float-end me-2" value="${i}">ส่งคำขอโอนพัสดุ</button>

                        </div>
                    </div>`
                )
                
            } catch (error) {
                console.log(error)
            }
            
        }

        $( ":input[type='number']" ).keyup(function() {
            let thisValue = parseInt($(this).val())
            let thisMaxValue = parseInt($(this).attr('max'))
            let thisMinValue = parseInt($(this).attr('min'))
            console.log(thisValue)
            if(thisValue > thisMaxValue || thisValue < thisMinValue){
                $(this).val(thisMaxValue)
            }
        });
    
        // <!-- save and send transfer request --> ต้องสร้าง function ของปุ่ม ขึ้นมาพร้อมกับ element ต่างๆ
        // $(document).ready(function(){
            $('div.card-body').on('click', 'button#save_data' , function () {
                let tableID = $(this).val()
                let transferId = $(`#transferId${tableID}`).html()
                console.log(tableID)
                // loop over each table row (tr)
                $(`#transfer_transfer_list${tableID} tr`).each(function(){
                        let currentRow=$(this);
                    
                        let transferListId=currentRow.find("td:eq(8)").text();
                        let newTransferListValue=currentRow.find("td:eq(6) input[type='number']").val();

                        let settings = {
                            "url": "/api/tb_transfer_list/edit_tb_transfer_list_value/",
                            "method": "POST",
                            "timeout": 0,
                            "headers": {
                                "Content-Type": "application/x-www-form-urlencoded"
                            },
                            "data": {
                                "transferListId": transferListId,
                                "newEquipmentValue": newTransferListValue
                            }
                        };

                        $.ajax(settings).done(function (response) {
                            
                        });
                })

                // console.log(transferId)

                let settings = {
                    "url": "/api/tb_transfer/edit_tb_transfer_status",
                    "method": "POST",
                    "data": {
                        "transferId": transferId,
                        "transferStatus" : 2
                    }
                };

                $.ajax(settings).done(function (response) {
                    getNotiTransferCount(Bacode0)
                    create_transfer_card()
                })

                // let alertTransferEquipment = new bootstrap.Modal(document.getElementById('alert-transfer-equipment'))
                // alertTransferEquipment.show()    
                $('#alert-transfer-equipment').modal('toggle');
                setTimeout(function(){$('#alert-transfer-equipment').modal('hide')},1200);
                // alert("Save AND Send")

            })
        // })

        // <!-- delete transfer list --> ต้องสร้าง function ของปุ่ม ขึ้นมาพร้อมกับ element ต่างๆ
        // $(document).ready(function(){
            $('table').on('click', 'button#delete_transfer_list' , function () {
                let cellData = $(this).closest('tr').find('td')
                let transferListId = cellData[8].innerText
                let settings = {
                    "url": `/api/tb_transfer_list/${transferListId}`,
                    "method": "DELETE",
                };

                $.ajax(settings).done(function (response) {
                    
                    let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
                    let settings = {
                        "url": "/api/tb_transfer/select_transfer",
                        "method": "POST",
                        "data": {
                            "warehouseIdRequester": Bacode0,
                            "transferStatus": 1
                        }
                    };

                    $.ajax(settings).done(function (response) {

                        getNotiTransferCount(Bacode0)
                        create_transfer_card()
                    
                    });

                });
            })
        // })

        // button reject click
        // $(document).ready(function(){
            $('div.card-header').on('click', 'button#delete_transfer', function () {
                let cardID = $(this).val()
                let transferId = $(`#transferId${cardID}`).html()
                // console.log(transferId)
                let rejectTransferEquipment = new bootstrap.Modal(document.getElementById('reject-alert-transfer-equipment'))
                rejectTransferEquipment.show()   

                $('#reject-alert-transfer-equipment-btn').on('click', function () {

                    let settings = {
                        "url": "/api/tb_transfer/",
                        "method": "DELETE",
                        "data": {
                            "transferId": transferId,
                            // "transferStatus" : 4
                        }
                    };

                    $.ajax(settings).done(function (response) {
                        let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
                        create_transfer_card()
                        getNotiTransferCount(Bacode0)  

                        $('#reject-alert-transfer-equipment').modal('toggle');
                        // cardID =''
                        // transferId =''
                        // settings =''
                    })

            })
        })

    });
}
</script>



<!-- get noti transfer count number -->
<script>
    // $(document).ready(function(){

        function getNotiTransferCount(Bacode0){
            let settings = {
                "url": "/api/tb_transfer/select_count_transfer/",
                "method": "POST",            
                "data": {
                    "warehouseIdRequester": Bacode0,
                    "transferStatus": 1
                },
            };
            $.ajax(settings).done(function (response_transfer_count) {
                let NotiTransferCount = response_transfer_count.total
                if(NotiTransferCount !=0){
                    $("#transferNotice").html(NotiTransferCount)
                    $("#transferNotice").attr("style", "display:block")
                    $("#side_noti_transfer_req").html(NotiTransferCount)
                    $("#side_noti_transfer_req").show()
                }
                else{
                    $("#transferNotice").attr("style", "display:none")
                    $("#side_noti_transfer_req").hide()
                }
            });
        }    
    // })
</script>

<!-- get noti transfer count number for owner -->
<script>
    // $(document).ready(function(){

        function getNotiTransferCountOwner(Bacode0){
            let settings = {
                "url": "/api/tb_transfer/select_count_transfer_owner/",
                "method": "POST",            
                "data": {
                    "warehouseIdOwner": Bacode0,
                    "transferStatus": 2
                },
            };
            $.ajax(settings).done(function (response_transfer_count_owner) {
                let NotiTransferCountOwner = response_transfer_count_owner.total
                if(NotiTransferCountOwner !=0){
                    $("#side_noti_transfer_own").html(NotiTransferCountOwner)
                    $("#side_noti_transfer_own").show()
                }
                else{
                    $("#side_noti_transfer_own").hide()
                }
            });
        }    
    // })
</script>

<!-- call function Show transfer group -->
<script>
    let Bacode0 = data.userLoginDataResponce.BaCode.substring(0, 3) + "0"
    create_transfer_card()
    getNotiTransferCount(Bacode0)
    getNotiTransferCountOwner(Bacode0)
</script>

<script>
    $('#reject-alert-transfer-equipment').on('hide.bs.modal', function(event)
    {
        let confirmEquipTransfer = $(this).find('#reject-alert-transfer-equipment-btn');
        confirmEquipTransfer.unbind("click");
        // console.log("clear")
    });
</script>


