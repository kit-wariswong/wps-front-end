<!doctype html>
<html lang="en">

<head>
    <?php
include '../include/head.php';
?>
    <title>เข้าสู่ระบบวางแผนและจัดการพัสดุ (งบผู้ใช้ไฟ) แบบเบ็ดเสร็จ</title>
</head>

<body>
    <div class="container-fluid">
        <div class="row flex-nowrap bg-dark">
            <?php
include '../include/sidemenu.php';
?>

            <div class="col p-0 mt-1 bg-light">
                <?php
include '../include/navbar.php';
?>
                <!-- content start-->
                <div class="p-4">
                    <h3>รายการจัดซื้อพัสดุ</h3>
                </div>
                <div class="container">
                    <!-- Content here -->
                    <div class="row mt-2">
                        <div class="col">ประเภทงาน :</div>
                        <div class="col">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1"
                                value="1" checked>
                            <label class="form-check-label" for="exampleRadios1">
                                มีราคากลาง
                            </label>
                        </div>
                        <div class="col">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1"
                                value="0">
                            <label class="form-check-label" for="exampleRadios1">
                                ไม่มีราคากลาง
                            </label>
                        </div>
                        <div class="col"></div>
                        <div class="col"></div>
                        <div class="col"></div>
                    </div>
                    <hr>
                    <div class="row mt-2">
                        <div class="col"><u>1. เรื่องเดิม</u></div>
                    </div>
                    <div class="row g-6 align-items-center mt-1">
                        <div class="col-auto"></div>
                        <div class="col-auto">
                            1.1 ตามที่ กฟภ ได้รับการจัดสรรงบประมาณ ตาม อนุมัติเลขที่
                        </div>
                        <div class="col-4">
                            <input type="text" class="form-control" id="approvalNumber" value="">
                        </div>
                        <div class="col-auto">
                            งบประมาณจำนวน
                        </div>
                        <div class="col-2">
                            <input type="text" class="form-control" id="inputapprovalBudget">
                        </div>
                        <div class="col-auto">
                            บาท
                        </div>
                    </div>
                    <div class="row g-4 align-items-center mt-1">
                        <div class="col-auto">(ไม่รวมภาษีมูลค่าเพิ่ม) อ้างอิงตามหมายเลขงานกลางที่</div>
                        <div class="col-auto">
                            <input type="text" class="form-control" id="inputmiddleJobNumber">
                        </div>
                        <div class="col-auto">ใหจัดซื้อ พัสดุ-อุปกรณ์ เพื่อใช้งาน</div>
                        <div class="col-3">
                            <input type="text" class="form-control" id="toUse">
                        </div>
                    </div>
                    <div class="row g-4 align-items-center mt-1">
                        <div class="col-auto"></div>
                        <div class="col-auto">1.2 ผคพ ได้ตรวจสอบปริมาณความต้องการใช้ พัสดุ-อุปกรณ์ งาน</div>
                        <div class="col-auto">
                            <input type="text" class="form-control" id="equipmentSupplyJob">
                        </div>
                        <div class="col-auto">
                            ในระบ SAP T-Code (ZCN52N) ณ วันที่
                        </div>
                        <div class="col-auto">
                            <input type="text" class="form-control" id="dateJob">
                        </div>
                    </div>
                    <div class="row g-4 align-items-center mt-1">
                        <div class="col-auto">
                            (ดังเอกสารความต้องการใช้งานพัสดุ-อุปกรณ์ จากระบบ SAP แนบท้ายนี้) พร้อมรายละเอียดคุณลักษณะ
                            (Specification) ของแต่ละรายการ ตามข้อกำหนด กฟภ. (เอกสารแนบท้าย)
                        </div>
                    </div>
                    <div class="row g-4 align-items-center mt-1">
                        <div class="col-auto">
                            <u>3.ข้อเสนอแนะ</u>
                        </div>
                    </div>
                    <div class="row g-4 align-items-center mt-1">
                        <div class="col-auto">
                            เพื่อการจัดซื้อให้มีพัสดุสำหรับใช้งานงบผู้ใช้ไฟ ได้อย่างรวดเร็วทันกับความต้องการใช้งาน
                            มีประสิทธิภาพประสิทธิผลในการดำเนินงาน ของ กฟภ และเพื่อป้องกันความเสียหายที่อาจจะเกิดขึ้นต่อ
                        </div>
                    </div>
                    <div class="row g-3 align-items-center mt-1">
                        <div class="col-auto">
                            กฟภ. จึงเห็นควรอนุมัติให้ ผบห เป็นผู้ดำเนินการจัดซื้อพัสดุ-อุปกรณ์
                            โดยให้จัดซื้อตามสเปคมาตรฐานที่ กฟภ. กำหนด
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="card">
                            <div class="card-body">
                                <table id="example" class="display" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center">รหัสพัสดุ</th>
                                            <th class="text-center">รายการ</th>
                                            <th class="text-center">Specification No.</th>
                                            <th class="text-center">หน่วยนับ</th>
                                            <th class="text-center">จำนวน</th>
                                            <th class="text-center">ราคากลาง</th>
                                            <th class="text-center">เงินงบประมาณ</th>
                                            <th class="text-center">#</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row g-6 align-items-center mt-1">
                        <div class="col-auto"></div>
                        <div class="col-auto">
                            แหล่งที่มาของราคากลางในการจัดซื้อครั้งนี้เป็นราคาที่คณะกรรมการกำหนดราคามาตรฐานและราคากลางพัสดุ
                            ประจำปี
                        </div>
                        <div class="col-1">
                            <input type="text" class="form-control" id="annual">
                        </div>
                        <div class="col-1">
                            ครั้งที่
                        </div>
                        <div class="col-1">
                            <input type="text" class="form-control" id="theTime">
                        </div>
                    </div>
                    <div class="row g-2 align-items-center mt-1" id="rowpriceRecordNumberDocument">
                        <div class="col-auto">ได้กำหนดไว้ตามบันทึกที่</div>
                        <div class="col-8">
                            <input type="text" class="form-control" id="inputpriceRecordNumberDocument">
                        </div>
                    </div>
                    <div id="rownotSpecifiedInThePricingRecord">
                        <div class="row g-2 align-items-center mt-1">
                            <div class="col-auto">ไม่ได้มีกำหนดไว้ ตามบันทึกเลขที่</div>
                            <div class="col-auto">
                                <input type="text" class="form-control" id="inputnotSpecifiedInThePricingRecord">
                            </div>
                            <div class="col-auto">ผคพ จึงขอใช้แหล่งที่มาของราคากลางในการประมาณการค่าซื้อครั้งนี้
                                เป็นราคาที่เคยซื้อหรือจ้างครั้งหลังสุด</div>
                            <div class="row g-2 align-items-center mt-1">
                                <div class="col-auto">
                                    ภายในระยะเวลาสองปีงบประมาณ โดยเป็นราคาที่จัดซื้อครั้งล่าสุดของ
                                </div>
                                <div class="col-auto">
                                    <input type="text" class="form-control" id="">
                                </div>
                                <div class="col-auto">
                                    ตามสัญญาใบสั่งซื้อเลขที่
                                </div>
                                <div class="col-auto">
                                    <input type="text" class="form-control" id="inputpurchaseContractDocumentNumber">
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="purchaseId">
                    <input type="hidden" id="finalPurchaseIdKey">
                    <input type="hidden" id="warehouseIdRequester">
                    <input type="hidden" id="warehouseIdEquipOwner">
                    <input type="hidden" id="purchaseStatus">
                    <input type="hidden" id="purchaseDocWithMiddlePrice">
                    <div class="row g-2 align-items-center mt-2">
                        <div class="col-auto"></div>
                        <div class="col-auto">จึงเรียนมาเพื่อโปรดพิจารณาอนุมัติ และแจ้ง ผบห. ดำเนินการต่อไป</div>
                    </div>
                    <div class="row g-2 align-items-center mt-2">
                        <div class="col-auto">ผู้ขอเสนอซื้อ/จ้าง:</div>
                        <div class="col-auto">
                            <input type="text" class="form-control" id="inputnameOfApproval">
                        </div>
                        <div class="col-auto">ตำแหน่งผู้ขอ เสนอซื้อ/จ้าง:</div>
                        <div class="col-auto">
                            <input type="text" class="form-control" id="inputpositionOfApproval">
                        </div>
                    </div>
                    <div class="row mt-2 d-flex justify-content-center">
                        <div class="col-auto">
                            <button type="button" class="btn btn-primary" id="btn_OnePdf">บันทึก</button>
                        </div>
                        <div class="col-auto">
                            <button type="button" class="pdfButton btn btn-success"
                                id="btn_GoPdf">สร้างใบปะหน้า</button>
                        </div>
                    </div>
                    <div class="row mt-2">
                    </div>
                </div>
            </div>
            <!-- content end-->
        </div>
    </div>
    </div>

</body>

</html>


<!-- Login check -->

<script>
$(document).ready(function() {

    let Username = JSON.parse(localStorage.getItem("data"));

    $("#rownotSpecifiedInThePricingRecord").hide();
    $(':radio[name="exampleRadios"]').change(function() {
        var radioValue = $("input[name='exampleRadios']:checked").val();
        if (radioValue === "1") {
            $("#rowpriceRecordNumberDocument").show();
            $("#rownotSpecifiedInThePricingRecord").hide();
            $('.inputmiddlePrice').prop('readonly', true);
            $("#purchaseDocWithMiddlePrice").val(1);
        } else {
            $("#rowpriceRecordNumberDocument").hide();
            $("#rownotSpecifiedInThePricingRecord").show();
            $('.inputmiddlePrice').prop('readonly', false);
            $("#purchaseDocWithMiddlePrice").val(0);
        }
    });


    $.ajax({
        type: "POST",
        url: "/api/tb_purchase/purchase_by_user",
        data: {
            userId: Username.userLoginDataResponce.Username
        },
        success: function(data) {
            // console.log(data.data[0].purchaseStatus);
            if (data.data.length === 0) {

                Swal.fire({
                    title: 'ยังไม่ได้เลือกวัสดุที่ต้องการจัดซื้อ?',
                    confirmButtonText: 'ตกลง',
                }).then((result) => {
                    window.location.href = "./wh_safety_stock_location.php";
                })

            } else {
                console.log(data.data[0]);
                $("#approvalNumber").val(data.data[0].approvalNumber);
                $("#inputapprovalBudget").val(data.data[0].approvalBudget);
                $("#inputmiddleJobNumber").val(data.data[0].middleJobNumber);
                $("#inputnameOfApproval").val(data.data[0].nameOfApproval);
                $("#inputpositionOfApproval").val(data.data[0].positionOfApproval);
                $("#purchaseId").val(data.data[0].purchaseId);
                $("#finalPurchaseIdKey").val(data.data[0].finalPurchaseIdKey);
                $("#warehouseIdRequester").val(data.data[0].warehouseIdRequester);
                $("#warehouseIdEquipOwner").val(data.data[0].warehouseIdEquipOwner);
                $("#purchaseStatus").val(data.data[0].purchaseStatus);
                $("#purchaseDocWithMiddlePrice").val(data.data[0].purchaseDocWithMiddlePrice);

                $('input:radio[name="exampleRadios"]').filter('[value="' + data.data[0]
                    .purchaseDocWithMiddlePrice + '"]').attr(
                    'checked',
                    true);

                $("#inputpriceRecordNumberDocument").val(data.data[0].priceRecordNumberDocument);
                $("#inputnotSpecifiedInThePricingRecord").val(data.data[0]
                    .notSpecifiedInThePricingRecord);
                $("#inputpurchaseContractDocumentNumber").val(data.data[0]
                    .purchaseContractDocumentNumber);
                $("#toUse").val(data.data[0].toUse);
                $("#equipmentSupplyJob").val(data.data[0].equipmentSupplyJob);
                $("#annual").val(data.data[0].annual);
                $("#theTime").val(data.data[0].theTime);
                $("#dateJob").val(data.data[0].dateJob);

                var table = $('#example').DataTable({
                    "responsive": true,
                    "scrollX": true,
                    select: true,
                    ajax: {
                        "url": "/api/tb_purchase/purchase_by_user",
                        "type": "POST",
                        "data": {
                            userId: Username.userLoginDataResponce.Username
                        },
                    },
                    "columns": [{
                            "data": "tb_purchase_tb_purchase_list_associate.purchase_list_stuff_belongs_to_mb52.equipmentId",
                            "className": "text-center",
                        },
                        {
                            "data": "tb_purchase_tb_purchase_list_associate.purchase_list_stuff_belongs_to_mb52.stuffName",
                            "className": "text-center",
                        },
                        {
                            "data": null,
                            "render": function(data) {
                                return '<input type="text" id="row-4-specNo" class="form-control" name="row-4-specNo" value="' +
                                    data.tb_purchase_tb_purchase_list_associate
                                    .specNo +
                                    '">'
                            },
                            "className": "text-center",
                        },
                        {
                            "data": "tb_purchase_tb_purchase_list_associate.purchase_list_stuff_belongs_to_mb52.counter",
                            "className": "text-center",
                        },
                        {
                            "data": null,
                            "render": function(data) {
                                return '<input class="inputequipmentValue form-control" type="number" oninput="this.value = Math.abs(this.value)" value="' +
                                    data.tb_purchase_tb_purchase_list_associate
                                    .equipmentValue +
                                    '">'
                            },
                            "className": "text-center",
                        },
                        {
                            "data": null,
                            "render": function(data) {
                                let radioValue = $("input[name='exampleRadios']:checked").val();
                                if (radioValue === "1") {
                                    return '<input class="inputmiddlePrice form-control" type="number" oninput="this.value = Math.abs(this.value)" value="' +
                                    data.tb_purchase_tb_purchase_list_associate
                                    .middlePrice +
                                    '" readonly>'
                                } else {
                                    return '<input class="inputmiddlePrice form-control" type="number" oninput="this.value = Math.abs(this.value)" value="' +
                                    data.tb_purchase_tb_purchase_list_associate
                                    .middlePrice +
                                    '" >'
                                }

                                // return '<input class="inputmiddlePrice form-control" type="number" oninput="this.value = Math.abs(this.value)" value="' +
                                //     data.tb_purchase_tb_purchase_list_associate
                                //     .middlePrice +
                                //     '" readonly>'
                            },
                            "className": "text-center",
                        },
                        {
                            "data": null,
                            "render": function(data) {
                                return '<input class="inputbudgetPrice form-control" type="number" oninput="this.value = Math.abs(this.value)" value="' +
                                    data.tb_purchase_tb_purchase_list_associate
                                    .budgetPrice +
                                    '" readonly>'
                            },
                            "className": "text-center",
                        },
                        {
                            "data": null,
                            'render': function(data) {
                                return '<button id="' + data
                                    .tb_purchase_tb_purchase_list_associate
                                    .purchaseListId +
                                    '" class="editButton btn btn-danger" value="' +
                                    data.tb_purchase_tb_purchase_list_associate
                                    .purchaseListId +
                                    '"><i class="bi bi-trash-fill"></i>ลบ</button>'
                            },
                            "className": "text-center",
                        },
                    ],
                }).columns.adjust();

                let radioValue = $("input[name='exampleRadios']:checked").val();
                if (radioValue === "1") {
                    $("#rowpriceRecordNumberDocument").show();
                    $("#rownotSpecifiedInThePricingRecord").hide();
                    // $('.inputmiddlePrice').prop('readonly', true);
                    $("#purchaseDocWithMiddlePrice").val(1);
                } else {
                    $("#rowpriceRecordNumberDocument").hide();
                    $("#rownotSpecifiedInThePricingRecord").show();
                    // $('.inputmiddlePrice').prop('readonly', false);
                    $("#purchaseDocWithMiddlePrice").val(0);
                }

                $("#tableCount").val(table.data().count());

                $("#example").on("change", ".inputequipmentValue", function() {
                    let currentRow = $(this).val();

                    var tr = $(this).closest("tr");
                    var tr2 = Number(tr.find(".inputmiddlePrice").val().toLocaleString(
                        undefined, {
                            minimumFractionDigits: 2,
                        }));
                    var total = currentRow * tr2;

                    tr.find(".inputbudgetPrice").val(total);


                });

                $("#example").on("change", ".inputmiddlePrice", function() {
                    let currentRow = $(this).val();

                    var tr = $(this).closest("tr");
                    var tr2 = Number(tr.find(".inputequipmentValue").val()
                        .toLocaleString(
                            undefined, {
                                minimumFractionDigits: 2,
                            }));
                    var total = currentRow * tr2;

                    tr.find(".inputbudgetPrice").val(total);


                });

                $('#example').on('click', 'button.editButton', function() {
                    var id = $(this).attr('id');
                    Swal.fire({
                        title: 'ต้องการจะลบข้อมูล?',
                        showCancelButton: true,
                        confirmButtonText: 'DELETE',
                    }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            $.ajax({
                                url: "/api/tb_purchase_list/" +
                                    id,
                                type: 'DELETE',
                                success: function(data) {
                                    $('#example').DataTable().ajax
                                        .reload();
                                }
                            })
                        }
                    })
                });

                $("#btn_GoPdf").click(function() {
                    var purchaseId = $("#purchaseId").val();
        
                    const dataEdittop = {
                        purchaseId: purchaseId,           
                        purchaseStatus: 2,
                        
                    }
                    $.ajax({
                        type: "POST",
                        url: "/api/tb_purchase/edit_tb_purchase",
                        dataType: "json",
                        data: dataEdittop,
                        success: function(message) {
                            console.log(message)
                            // if (message === 'แก้ไขข้อมูลสำเร็จ') {
                                window.open(
                                    `../pdf/form2.php?purchaseId=${purchaseId}`
                                )
                            // }
                        }
                    })        
                    
                });
            }
        }
    });

    $("#btn_OnePdf").click(function() {

        var approvalNumber = $("#approvalNumber").val();
        var inputapprovalBudget = $("#inputapprovalBudget").val();
        var inputmiddleJobNumber = $("#inputmiddleJobNumber").val();
        var inputnameOfApproval = $("#inputnameOfApproval").val();
        var inputpositionOfApproval = $("#inputpositionOfApproval").val();
        var purchaseId = $("#purchaseId").val();
        var finalPurchaseIdKey = $("#finalPurchaseIdKey").val();
        var warehouseIdRequester = $("#warehouseIdRequester").val();
        var warehouseIdEquipOwner = $("#warehouseIdEquipOwner").val();
        var purchaseStatus = $("#purchaseStatus").val();
        var purchaseDocWithMiddlePrice = $("#purchaseDocWithMiddlePrice")
            .val();
        var priceRecordNumberDocument = $("#inputpriceRecordNumberDocument")
            .val();
        var notSpecifiedInThePricingRecord = $(
            "#inputnotSpecifiedInThePricingRecord").val();
        var purchaseContractDocumentNumber = $(
            "#inputpurchaseContractDocumentNumber").val();
        var toUse = $("#toUse").val();
        var equipmentSupplyJob = $("#equipmentSupplyJob").val();
        var annual = $("#annual").val();
        var theTime = $("#theTime").val();
        var dateJob = $("#dateJob").val();


        const dataEdittop = {
            purchaseId: purchaseId,
            finalPurchaseIdKey: finalPurchaseIdKey,
            approvalNumber: approvalNumber,
            approvalBudget: inputapprovalBudget,
            middleJobNumber: inputmiddleJobNumber,
            nameOfApproval: inputnameOfApproval,
            positionOfApproval: inputpositionOfApproval,
            warehouseIdRequester: warehouseIdRequester,
            warehouseIdEquipOwner: warehouseIdEquipOwner,
            userId: Username.userLoginDataResponce.Username,
            purchaseStatus: 1,
            purchaseDocWithMiddlePrice: purchaseDocWithMiddlePrice,
            priceRecordNumberDocument: priceRecordNumberDocument,
            notSpecifiedInThePricingRecord: notSpecifiedInThePricingRecord,
            purchaseContractDocumentNumber: purchaseContractDocumentNumber,
            toUse: toUse,
            equipmentSupplyJob: equipmentSupplyJob,
            annual: annual,
            theTime: theTime,
            dateJob: dateJob,
        }
        console.log(dataEdittop);
        $.ajax({
            type: "POST",
            url: "/api/tb_purchase/edit_tb_purchase",
            dataType: "json",
            data: dataEdittop,
            success: function(message) {
                if (message === 'แก้ไขข้อมูลสำเร็จ') {
                    alert('ok')
                } else {
                    $('#example tr').each(function() {
                        let currentRow = $(this);
                        let purchaseListId = currentRow
                            .find(
                                "td:eq(7) button")
                            .val();
                        let specNo = currentRow.find(
                                "td:eq(2) input[type='text']"
                            )
                            .val();
                        let equipmentValue = currentRow
                            .find(
                                "td:eq(4) input[type='number']"
                            )
                            .val();
                        let middlePrice = currentRow
                            .find(
                                "td:eq(5) input[type='number']"
                            )
                            .val();
                        let budgetPrice = currentRow
                            .find(
                                "td:eq(6) input[type='number']"
                            )
                            .val();

                        const EditpurchaseList = {
                            purchaseListId: purchaseListId,
                            specNo: specNo,
                            equipmentValue: equipmentValue,
                            middlePrice: middlePrice,
                            budgetPrice: budgetPrice
                        }
                        // console.log(EditpurchaseList
                        //     .purchaseListId);

                        if (EditpurchaseList
                            .purchaseListId ===
                            undefined) {

                        } else {
                            $.ajax({
                                type: "POST",
                                url: "/api/tb_purchase_list/edit_tb_purchase_list/",
                                dataType: "json",
                                data: EditpurchaseList,
                                success: function(
                                    data) {
                                    Swal.fire({
                                        title: "บันทึกข้อมูลสำเร็จ",
                                        icon: 'success',
                                    })
                                }
                            })
                        }
                    });
                }
            }
        });
    });

});
</script>
<!-- Login check -->