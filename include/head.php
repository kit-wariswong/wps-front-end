<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">

    <!-- bootstrap -->
    <link href="../assets/bootstrap-5.1.3-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Prompt:wght@100;200;300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../assets/bootstrap-5.1.3-dist/font/bootstrap-icons.css">


    <!-- import jquery -->
    <script src="../assets/jquery-3.6.0/jquery-3.6.0.min.js"></script>

    <!-- import bootstrap js -->
    <script src="../assets/bootstrap-5.1.3-dist/js/bootstrap.min.js"></script>

    <!-- datatable css -->
    <link href="../assets/DataTables/datatables.min.css" rel="stylesheet">

    <!-- import datatable -->
    <script src="../assets/DataTables/datatables.min.js"></script>

    <!-- icon number css -->
    <link href="../assets/css/icon-number.css" rel="stylesheet">

    <!-- import moment -->
    <script src="../assets/moment/moment.min.js"></script>
    <script src="../assets/moment/datetime-moment.js"></script>

    <!-- import sweetalert2 -->
    <script src="../assets/sweetalert/sweetalert2.all.min.js"></script>

    <!-- title icon -->
    <link rel="icon" href="../images/icons/boxes24.ico" type="image/ico">

    <!-- bootstrap bundle  -->
    <!-- <script src="../assets/bootstrap-5.1.3-dist/js/bootstrap.bundle.js"></script> -->
</head>

<style>
body {
    font-family: 'Prompt', sans-serif;
}
</style>