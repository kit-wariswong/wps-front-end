<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">

    <div class="collapse navbar-collapse">
        <div class="navbar-nav ms-auto mb-2 me-5">
            <a class="nav-item nav-link
                text-white" href="#">ยินดีต้อนรับคุณ :</a>
            <a class="nav-link text-white" id="nav-fullname" href="#">ชื่อ - นามสกุล</a>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" style="font-size: 1rem;" href="#" id="nav-menu"
                    role="button" data-bs-toggle="dropdown" aria-expanded="false"></a>
                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="nav-menu">
                    <div id="nav-cs-menu" style="display:none">
                        <li><a class="dropdown-item" href="../pages/cs_noti_list.php">ประวัติการแจ้งเตือน</a></li>
                    </div>
                    <div id="nav-wh-menu" style="display:none">
                        <li><a class="dropdown-item" href="../pages/wh_transfer_history_req.php">ประวัติการแจ้งขอโอน</a>
                        </li>
                        <li><a class="dropdown-item" href="../pages/wh_transfer_history_own.php">ประวัติการแจ้งโอน</a>
                        </li>
                        <li><a class="dropdown-item"
                                href="../pages/wh_safety_purchase_pr.php">ประวัติรายการจัดซื้อพัสดุ</a></li>
                    </div>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="#" onclick="logout()">ออกจากระบบ</a></li>
                </ul>
            </li>
            <a class="nav-item nav-link text-white" id="nav-empid" href="#">(รหัสประจำตัว)</a>
            <a class="nav-item nav-link text-white" href="#">สังกัด :</a>
            <a class="nav-item nav-link text-white" id="nav-pea" href="#">xxxxxx</a>
            <div id="transferNoticeButton" class="icon-badge-container" style="display:block">
                <a href="../pages/wh_transfer_req_list.php" class="nav-link text-white">
                    <i class="bi bi-cart-fill"></i>
                    <div id="transferNotice" class="icon-badge" style="display:none"></div>
                </a>
            </div>
            <div id="NoticeButton" class="icon-badge-container" style="display:block">
                <a href="../pages/wh_noti_from_cs.php" class="nav-link text-white">
                    <i class="bi bi-bell-fill"></i>
                    <div id="Notice" class="icon-badge" style="display:none"></div>
                </a>
            </div>

        </div>
    </div>
</nav>

<!-- Show Menu by user DepartmentSAP-->
<script>
if (data.userLoginDataResponce.DepartmentShortName == "ผคพ.") {
    $("#nav-wh-menu").attr("style", "display:block");
} else if (data.userLoginDataResponce.DepartmentShortName == "ผบค.") {
    $("#nav-cs-menu").attr("style", "display:block");
} else {
    $("#nav-wh-menu").attr("style", "display:none");
    $("#nav-cs-menu").attr("style", "display:none");
}
</script>


<!-- insert user data to navbar -->
<script>
$("#nav-fullname").html(`${data.userLoginDataResponce.FirstName} ${data.userLoginDataResponce.LastName}`)
$("#nav-empid").html(`(${data.userLoginDataResponce.Username})`)
$("#nav-pea").html(`${data.userLoginDataResponce.BaName}`)
</script>