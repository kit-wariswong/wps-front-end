<div class="col-auto col-sm-2 col-md-2 col-xl-2 px-sm-2 px-0 bg-dark">
    <div class="d-flex flex-column flex-shrink-0 p-3 text-white bg-dark vh-100 sticky-top">
        <a href="#" class="text-center align-items-center text-white text-decoration-none ">
            <i class="bi bi-box-seam" style="font-size: 1.5rem;"></i>
            <span class="fs-4 ms-2 d-sm-inline">WPS</span>
        </a>
        <hr>
        <!-- คลังพัสดุ -->
        <ul class="nav nav-pills flex-column mb-auto">
            <div id="wh-menu" style="display:none">
                <li class="nav-item">
                    <a id="wh_safety_stock" href="../pages/wh_safety_stock_location.php" class="nav-link text-white"
                        aria-current="page">
                        <i class="bi bi-table"></i>
                        <span class="ms-1 d-none d-xl-inline">รายการพัสดุ Safety Stock</span>
                    </a>
                </li>
                <li>
                    <a id="wh_transfer_req_list" href="../pages/wh_transfer_req_list.php" class="nav-link text-white">
                        <i class="bi bi-cart3"></i>
                        <span class="ms-1 d-none d-xl-inline">รายการขอโอนพัสดุ</span> <span id="side_noti_transfer_req"
                            class="badge rounded-pill bg-danger" style="display:none"></span>
                    </a>
                </li>
                <li>
                    <a id="wh_transfer_req_list" href="../pages/wh_safety_purchase_item.php"
                        class="nav-link text-white">
                        <i class="bi bi-currency-dollar"></i>
                        <span class="ms-1 d-none d-xl-inline">รายการจัดซื้อพัสดุ</span> <span
                            id="side_noti_transfer_req" class="badge rounded-pill bg-danger"
                            style="display:none"></span>
                    </a>
                </li>
                <li>
                    <a id="wh_transfer_own_list" href="../pages/wh_transfer_own_list.php" class="nav-link text-white">
                        <i class="bi bi-list-ul"></i>
                        <span class="ms-1 d-none d-xl-inline">ความต้องการพัสดุจาก กฟฟ. อื่น</span> <span
                            id="side_noti_transfer_own" class="badge rounded-pill bg-danger"
                            style="display:none"></span>
                    </a>
                </li>
                <li>
                    <a id="wh_noti_from_cs" href="../pages/wh_noti_from_cs.php" class="nav-link text-white">
                        <i class="bi bi-exclamation-square"></i>
                        <span class="ms-1 d-none d-xl-inline">ผบค. แจ้งเตือน</span> <span id="side_noti_cs"
                            class="badge rounded-pill bg-danger" style="display:none"></span>
                    </a>
                </li>
                <li>
                    <a id="wh_req_accept" href="../pages/wh_transfer_req_accept.php" class="nav-link text-white">
                        <i class="bi bi-clipboard-check"></i>
                        <span class="ms-1 d-none d-xl-inline">ยืนยันรับของ</span> <span id="side_req_accept"
                            class="badge rounded-pill bg-danger" style="display:none"></span>
                    </a>
                </li>
                <!-- <li>
        <a href="#" class="nav-link text-white">
          <i class="bi bi-coin" ></i>
          <span class="ms-1 d-none d-xl-inline">รายการชำระเงินแล้ว</span>
        </a>
      </li> -->
            </div>
            <!-- คลังพัสดุ -->

            <!-- ผบค. -->
            <div id="cs-menu" style="display:none">
                <li class="nav-item">
                    <a id="cs_safety_stock" href="../pages/cs_safety_stock.php" class="nav-link text-white"
                        aria-current="page">
                        <i class="bi bi-table"></i>
                        <span class="ms-1 d-none d-xl-inline">รายการพัสดุ Safety Stock</span>
                    </a>
                </li>
                <li>
                    <a id="cs_noti_to_wh" href="../pages/cs_noti_to_wh.php" class="nav-link text-white">
                        <i class="bi bi-bell"></i>
                        <span class="ms-1 d-none d-xl-inline">แจ้งความต้องการพัสดุ</span>
                    </a>
                </li>
            </div>
            <!-- ผบค. -->

        </ul>
        <hr>
        <div class="text-center">
            <button class="btn btn-dark text-white bi bi-power" style="font-size: 1rem;" onclick="logout()"></button>
        </div>
    </div>
</div>
<!-- Logout -->
<script>
function logout() {
    localStorage.clear();
    window.location.replace("./login.php");
}
</script>

<!-- Show Menu by user DepartmentSAP-->
<script>
let data = JSON.parse(localStorage.getItem("data"))
console.log(data.userLoginDataResponce.DepartmentSap)
if (data.userLoginDataResponce.DepartmentShortName == "ผคพ.") {
    $("#wh-menu").attr("style", "display:block");
} else if (data.userLoginDataResponce.DepartmentShortName == "ผบค.") {
    $("#cs-menu").attr("style", "display:block");
} else {
    $("#wh-menu").attr("style", "display:none");
    $("#cs-menu").attr("style", "display:none");
}
</script>