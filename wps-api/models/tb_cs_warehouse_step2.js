'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_cs_warehouse_step2 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.tb_cs_warehouse_step2.belongsTo(models.tb_user_login_info, {
        as: 'tb_cs_warehouse_step2_belongs_to_tb_user_login_info',
        foreignKey: 'userId',
        sourceKey: 'userId',
      });
      models.tb_cs_warehouse_step2.belongsTo(models.zcsr181, {
        as: 'tb_cs_warehouse_step2_belongs_to_zcsr181',
        foreignKey: 'wbsNumber',
        sourceKey: 'id',
      });
    }
  };
  tb_cs_warehouse_step2.init({
    csWarehouseS2Id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    wbsNumber: {
      type: DataTypes.STRING,
      allowNull: false,
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION',
      references: {
        model: {
          tableName: 'zcsr181s'
        },
        key: 'id',
      }
    },
    userId: {
      type: DataTypes.INTEGER
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION',
      references: {
        model: {
          tableName: 'tb_user_login_infos'
        },
        key: 'userId',
      }
    },
    timeStamp: {
      type: DataTypes.STRING
    },
    warehouseId: {
      type: DataTypes.STRING
    },
  }, {
    sequelize,
    modelName: 'tb_cs_warehouse_step2',
  });
  return tb_cs_warehouse_step2;
};