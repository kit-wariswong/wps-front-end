'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_cs_warehouse_step1 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.tb_cs_warehouse_step1.belongsTo(models.tb_user_login_info, {
        as: 'tb_cs_warehouse_step1_belongs_to_tb_user_login_info',
        foreignKey: 'userId',
        sourceKey: 'userId',
      });
      models.tb_cs_warehouse_step1.belongsTo(models.tb_estimate_set, {
        as: 'tb_cs_warehouse_step1_belongs_to_tb_estimate_set',
        foreignKey: 'setId',
        sourceKey: 'setId',
      });
    }
  };
  tb_cs_warehouse_step1.init({
    csWarehouseS1Id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    setId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION',
      references: {
        model: {
          tableName: 'tb_estimate_sets'
        },
        key: 'setId',
      }
    },
    jobName: {
      type: DataTypes.STRING
    },
    jobCustomer: {
      type: DataTypes.STRING
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION',
      references: {
        model: {
          tableName: 'tb_user_login_infos'
        },
        key: 'userId',
      }
    },
    warehouseId: {
      type: DataTypes.STRING
    },
  }, {
    sequelize,
    modelName: 'tb_cs_warehouse_step1',
  });
  return tb_cs_warehouse_step1;
};