'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Zmb25 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Zmb25.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    inventory_id: {
      type: DataTypes.STRING,
      defaultValue: "ENULL"
    },
    stuff_id: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    stuff_name: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    date_required: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    mvt: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    qty_needed: {
      type: DataTypes.DOUBLE,
      defaultValue: 0
    },
    qty_removed: {
      type: DataTypes.DOUBLE,
      defaultValue: 0
    },
    qty_different: {
      type: DataTypes.DOUBLE,
      defaultValue: 0
    },
    bun: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    netword_number: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    wbs_number: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    po_id: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    reserve_number: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    rca: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    user_added: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    }
  }, {
    sequelize,
    modelName: 'Zmb25',
    underscored: true,
    timestamps: false,
  });
  return Zmb25;
};