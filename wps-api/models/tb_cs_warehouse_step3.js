'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_cs_warehouse_step3 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.tb_cs_warehouse_step3.belongsTo(models.zcsr181, {
        as: 'tb_cs_warehouse_step3_belongs_to_zcsr181',
        foreignKey: 'wbsNumber',
        sourceKey: 'id',
      });
    }
  };
  tb_cs_warehouse_step3.init({
    csWarehouseS3Id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    wbsNumber: {
      type: DataTypes.INTEGER,
      allowNull: false,
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION',
      references: {
        model: {
          tableName: 'zcsr181s'
        },
        key: 'id',
      }
    },
  }, {
    sequelize,
    modelName: 'tb_cs_warehouse_step3',
  });
  return tb_cs_warehouse_step3;
};