'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_transfer_list extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.tb_transfer_list.belongsTo(models.mb52, {
        as: 'stuff_belongs_to_mb52',
        foreignKey: 'equipmentId',
        sourceKey: 'id',
      });
      models.tb_transfer_list.belongsTo(models.tb_transfer, {
        as: 'transfer_list_belongs_to_tb_transfer',
        foreignKey: 'transferId',
        sourceKey: 'transferId',
      });
    }
  };
  tb_transfer_list.init({
    transferListId: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER
    },
    transferId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: {
          tableName: 'tb_transfers'
        },
        key: 'transferId'
      }
    },
    equipmentId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: {
          tableName: 'mb52s'
        },
        key: 'id'
      }
    },
    equipmentValue: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'tb_transfer_list',
  });
  return tb_transfer_list;
};