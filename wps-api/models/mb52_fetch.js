'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class mb52_fetch extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  mb52_fetch.init({
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    invId: {
      type: DataTypes.STRING,
      defaultValue: "ENULL"
    },
    stockHolder: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    equipmentId: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    stuffName: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    batch: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    counter: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    stockAvailable: {
      type: DataTypes.DOUBLE,
      defaultValue: 0
    },
    value: {
      type: DataTypes.DOUBLE,
      defaultValue: 0
    },
    transfering: {
      type: DataTypes.DOUBLE,
      defaultValue: 0
    },
    suspend: {
      type: DataTypes.DOUBLE,
      defaultValue: 0
    },
  }, {
    sequelize,
    modelName: 'mb52_fetch',
    timestamps: false,
  });
  return mb52_fetch;
};