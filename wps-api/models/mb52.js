'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class mb52 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.mb52.hasMany(models.tb_transfer_list, {
        as: 'stuff52_transfer_list_associate',
        foreignKey: 'equipmentId', //fk transfer table
        sourceKey: 'id', // pk of this model
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
      });
      models.mb52.hasMany(models.tb_estimate_list, {
        as: 'stuff52_estimate_list_associate',
        foreignKey: 'equipmentId',
        sourceKey: 'id',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
      });
    }
  };
  mb52.init({
    id: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER
    },
    invId: {
      type: DataTypes.STRING,
      defaultValue: "ENULL"
    },
    stockHolder: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    equipmentId: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    stuffName: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    batch: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    counter: {
      type: DataTypes.STRING,
      defaultValue: "Undefinded"
    },
    stockAvailable: {
      type: DataTypes.DOUBLE,
      defaultValue: 0
    },
    value: {
      type: DataTypes.DOUBLE,
      defaultValue: 0
    },
    transfering: {
      type: DataTypes.DOUBLE,
      defaultValue: 0
    },
    suspend: {
      type: DataTypes.DOUBLE,
      defaultValue: 0
    },
  }, {
    sequelize,
    modelName: 'mb52',
    timestamps: false,
  });
  return mb52;
};