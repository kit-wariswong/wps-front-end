'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_all_stuff extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  tb_all_stuff.init({
    stuffId: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      type: DataTypes.STRING
    },
    groupId: {
      type: DataTypes.STRING
    },
    stuffIdCut: {
      type: DataTypes.STRING
    },
    stuffNameEng: {
      type: DataTypes.STRING
    },
    stuffNameTh: {
      type: DataTypes.STRING
    },
    counter: {
      type: DataTypes.STRING
    },
    stuffGroup: {
      type: DataTypes.STRING
    },
  }, {
    sequelize,
    modelName: 'tb_all_stuff',
  });
  return tb_all_stuff;
};