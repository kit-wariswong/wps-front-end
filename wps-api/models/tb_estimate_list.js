'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_estimate_list extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.tb_estimate_list.belongsTo(models.mb52, {
        as: 'estimate_belongs_to_mb52',
        foreignKey: 'equipmentId',
        sourceKey: 'id',
      });
      models.tb_estimate_list.belongsTo(models.tb_estimate_set, {
        as: 'estimate_belongs_to_tb_estimate_set',
        foreignKey: 'setId',
        sourceKey: 'id',
      });
    }
  };
  tb_estimate_list.init({
    estimateSetId: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER
    },
    setId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION',
      references: {
        model: {
          tableName: 'tb_estimate_sets'
        },
        key: 'setId'
      }
    },
    equipmentId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: {
          tableName: 'mb52'
        },
        key: 'id'
      }
    },
  }, {
    sequelize,
    modelName: 'tb_estimate_list',
  });
  return tb_estimate_list;
};