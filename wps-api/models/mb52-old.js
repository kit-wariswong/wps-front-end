'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class mb52 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  mb52.init({
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    invId:{
      type: DataTypes.STRING
    },
    stockHolder: {
      type: DataTypes.INTEGER
    },
    equipmentId: {
      type: DataTypes.STRING
    },
    stuffName: {
      type: DataTypes.STRING
    },
    batch: {
      type: DataTypes.STRING
    },
    counter: {
      type: DataTypes.STRING
    },
    stockAvailable: {
      type: DataTypes.DOUBLE
    },
    value: {
      type: DataTypes.DOUBLE
    },
    transfering: {
      type: DataTypes.DOUBLE
    },
    suspend: {
      type: DataTypes.DOUBLE
    },
  }, {
    sequelize,
    modelName: 'mb52',
    tableName: 'mb52',
    timestamps: false,
  });
  return mb52;
};