'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_estimate_set extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.tb_estimate_set.hasMany(models.tb_estimate_list, {
        as: 'tb_estimate_set_tb_estimate_list_associate',
        foreignKey: 'setId', //fk destination table
        sourceKey: 'setId', // pk of this model
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
      });
      models.tb_estimate_set.hasMany(models.tb_cs_warehouse_step1, {
        as: 'tb_estimate_set_tb_cs_warehouse_step1_associate',
        foreignKey: 'setId', //fk destination table
        sourceKey: 'setId', // pk of this model
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
      });
    }
  };
  tb_estimate_set.init({
    setId: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    setName: {
      type: DataTypes.STRING
    },
  }, {
    sequelize,
    modelName: 'tb_estimate_set',
  });
  return tb_estimate_set;
};