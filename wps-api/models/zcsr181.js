'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class zcsr181 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.zcsr181.hasMany(models.tb_cs_warehouse_step2, {
        as: 'zcsr181_cs_warehouse_step2_associate',
        foreignKey: 'wbsNumber', //fk destination table
        sourceKey: 'id', // pk of this model
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
      });
      models.zcsr181.hasMany(models.tb_cs_warehouse_step3, {
        as: 'zcsr181_cs_warehouse_step3_associate',
        foreignKey: 'wbsNumber', //fk destination table
        sourceKey: 'id', // pk of this model
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
      });
    }
  };
  zcsr181.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    officeId: {
      type: DataTypes.STRING
    },
    contractAccountNumber: {
      type: DataTypes.INTEGER
    },
    requestNumber: {
      type: DataTypes.INTEGER
    },
    requestType: {
      type: DataTypes.STRING
    },
    dateRecievedRequest: {
      type: DataTypes.STRING
    },
    requestorName: {
      type: DataTypes.STRING
    },
    requestorAddress: {
      type: DataTypes.STRING
    },
    saleOrderNumber: {
      type: DataTypes.INTEGER
    },
    billOrderNumber: {
      type: DataTypes.INTEGER
    },
    archiveType: {
      type: DataTypes.STRING
    },
    stuffId: {
      type: DataTypes.STRING
    },
    stuffName: {
      type: DataTypes.STRING
    },
    amount: {
      type: DataTypes.INTEGER
    },
    unit: {
      type: DataTypes.STRING
    },
    standardPricePerUnit: {
      type: DataTypes.DOUBLE
    },
    sellingPricePerUnit: {
      type: DataTypes.DOUBLE
    },
    taxPerUnit: {
      type: DataTypes.DOUBLE
    },
    standardPrice: {
      type: DataTypes.DOUBLE
    },
    sellingPrice: {
      type: DataTypes.DOUBLE
    },
    tax: {
      type: DataTypes.DOUBLE
    },
    totalPrice: {
      type: DataTypes.DOUBLE
    },
    debtSrDate: {
      type: DataTypes.STRING
    },
    paymentDate: {
      type: DataTypes.STRING
    },
    jobOrder: {
      type: DataTypes.STRING
    },
    wbsNumber: {
      type: DataTypes.STRING
    },
  }, {
    sequelize,
    modelName: 'zcsr181',
  });
  return zcsr181;
};