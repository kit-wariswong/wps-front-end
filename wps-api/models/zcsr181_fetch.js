'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class zcsr181_fetch extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  zcsr181_fetch.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    officeId: {
      type: DataTypes.STRING
    },
    contractAccountNumber: {
      type: DataTypes.INTEGER
    },
    requestNumber: {
      type: DataTypes.INTEGER
    },
    requestType: {
      type: DataTypes.STRING
    },
    dateRecievedRequest: {
      type: DataTypes.STRING
    },
    requestorName: {
      type: DataTypes.STRING
    },
    requestorAddress: {
      type: DataTypes.STRING
    },
    saleOrderNumber: {
      type: DataTypes.INTEGER
    },
    billOrderNumber: {
      type: DataTypes.INTEGER
    },
    archiveType: {
      type: DataTypes.STRING
    },
    stuffId: {
      type: DataTypes.STRING
    },
    stuffName: {
      type: DataTypes.STRING
    },
    amount: {
      type: DataTypes.INTEGER
    },
    unit: {
      type: DataTypes.STRING
    },
    standardPricePerUnit: {
      type: DataTypes.DOUBLE
    },
    sellingPricePerUnit: {
      type: DataTypes.DOUBLE
    },
    taxPerUnit: {
      type: DataTypes.DOUBLE
    },
    standardPrice: {
      type: DataTypes.DOUBLE
    },
    sellingPrice: {
      type: DataTypes.DOUBLE
    },
    tax: {
      type: DataTypes.DOUBLE
    },
    totalPrice: {
      type: DataTypes.DOUBLE
    },
    debtSrDate: {
      type: DataTypes.STRING
    },
    paymentDate: {
      type: DataTypes.STRING
    },
    jobOrder: {
      type: DataTypes.STRING
    },
    wbsNumber: {
      type: DataTypes.STRING
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    modelName: 'zcsr181_fetch',
  });
  return zcsr181_fetch;
};