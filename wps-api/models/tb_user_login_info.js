'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_user_login_info extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.tb_user_login_info.hasMany(models.tb_cs_warehouse_step1, {
        as: 'tb_user_login_info_tb_cs_warehouse_step1_associate',
        foreignKey: 'userId', //fk destination table
        sourceKey: 'userId', // pk of this model
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
      });
      models.tb_user_login_info.hasMany(models.tb_transfer, {
        as: 'tb_user_login_info_tb_transfer_associate',
        foreignKey: 'userId', //fk destination table
        sourceKey: 'userId', // pk of this model
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
      });
      models.tb_user_login_info.hasMany(models.tb_cs_warehouse_step2, {
        as: 'tb_user_login_info_tb_cs_warehouse_step2_associate',
        foreignKey: 'userId', //fk destination table
        sourceKey: 'userId', // pk of this model
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
      });
    }
  };
  tb_user_login_info.init({
    userId: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    userLastLogin:{
      type: DataTypes.STRING
    },
    userIp: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'tb_user_login_info',
  });
  return tb_user_login_info;
};