'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_warehouse_info extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  tb_warehouse_info.init({
    warehouseId: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    warehouseName: {
      type: DataTypes.STRING
    },
    clusterList: {
      type: DataTypes.INTEGER
    },
    TRSG: {
      type: DataTypes.STRING
    },
  }, {
    sequelize,
    modelName: 'tb_warehouse_info',
  });
  return tb_warehouse_info;
};