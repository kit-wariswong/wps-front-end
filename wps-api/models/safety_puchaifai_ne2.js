'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class safety_puchaifai_ne2 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  safety_puchaifai_ne2.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    puchaifai_stuff_id: {
      type: DataTypes.STRING
    },
    puchaifai_stuff_name: {
      type: DataTypes.STRING
    },
    puchaifai_e010: {
      type: DataTypes.DOUBLE
    },
    puchaifai_e020: {
      type: DataTypes.DOUBLE
    },
    puchaifai_e030: {
      type: DataTypes.DOUBLE
    },
    puchaifai_e040: {
      type: DataTypes.DOUBLE
    },
    puchaifai_e050: {
      type: DataTypes.DOUBLE
    },
    puchaifai_e060: {
      type: DataTypes.DOUBLE
    },
    puchaifai_e070: {
      type: DataTypes.DOUBLE
    },
    puchaifai_e080: {
      type: DataTypes.DOUBLE
    },
    puchaifai_e090: {
      type: DataTypes.DOUBLE
    },
    puchaifai_e100: {
      type: DataTypes.DOUBLE
    },
    puchaifai_e110: {
      type: DataTypes.DOUBLE
    },
    puchaifai_e120: {
      type: DataTypes.DOUBLE
    },
    puchaifai_sum: {
      type: DataTypes.DOUBLE
    },
    puchaifai_middle_price: {
      type: DataTypes.DOUBLE
    },
    puchaifai_sum_price: {
      type: DataTypes.DOUBLE
    },
  }, {
    sequelize,
    modelName: 'safety_puchaifai_ne2',
  });
  return safety_puchaifai_ne2;
};