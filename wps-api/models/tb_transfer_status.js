'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_transfer_status extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.tb_transfer_status.hasMany(models.tb_transfer, {
        as: 'tb_transfer_status_tb_transfer_associate',
        foreignKey: 'transferStatus', //fk destination table
        sourceKey: 'transferStatusId', // pk of this model
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
      });
    }
  };
  tb_transfer_status.init({
    transferStatusId: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    transferStatus: {
      type: DataTypes.INTEGER,
    },
  }, {
    sequelize,
    modelName: 'tb_transfer_status',
  });
  return tb_transfer_status;
};