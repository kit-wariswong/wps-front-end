'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_transfer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.tb_transfer.hasMany(models.tb_transfer_list, {
        as: 'tb_transfer_tb_transfer_list_associate',
        foreignKey: 'transferId', //fk destination table
        sourceKey: 'transferId', // pk of this model
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
      });
      models.tb_transfer.belongsTo(models.tb_transfer_status, {
        as: 'tb_transfer_belongs_to_tb_transfer_status',
        foreignKey: 'transferId',
        sourceKey: 'transferStatus',
      });
      models.tb_transfer.belongsTo(models.tb_user_login_info, {
        as: 'tb_transfer_belongs_to_tb_user_login_info',
        foreignKey: 'userId',
        sourceKey: 'userId',
      });
    }
  };
  tb_transfer.init({
    transferId: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    warehouseIdRequester: {
      type: DataTypes.STRING
    },
    warehouseIdEquipOwner: {
      type: DataTypes.STRING
    },
    userId: {
      type: DataTypes.INTEGER
    },
    sessionId: {
      type: DataTypes.STRING
    },
    transferStatus: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: {
          tableName: 'tb_transfer_statuses'
        },
        key: 'transferId'
      }
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    modelName: 'tb_transfer',
  });
  return tb_transfer;
};