const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const passport = require('passport');
const cors = require('cors');

// enable .env
require('dotenv').config();

const indexRouter = require('./routes/index');
const uploadexcelRouter = require('./routes/excel_upload');
const mb52Router = require('./routes/mb52');
const zcsr181Router = require('./routes/zcsr181');
const safetyNe2Router = require('./routes/safety_puchaifai_ne2');
const zmb25 = require('./routes/zmb25');
const allStuff = require('./routes/all_stuff');
const userLoginInfo = require('./routes/user_login_info');
const csWarehouseStep1 = require('./routes/cs_warehouse_step1');
const tbEstimateSet = require('./routes/tb_estimate_set');
const tbEstimateList = require('./routes/tb_estimate_list');
const tbTransferList = require('./routes/tb_transfer_list');
const tbTransfer = require('./routes/tb_transfer');
const loginIDM = require('./routes/loginIDM')

const app = express();
app.use(cors());

app.use(logger('dev'));
// json body size
app.use(express.json({
    limit: '25mb'
}));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// init passportjs
app.use(passport.initialize());

app.use('/', indexRouter);
app.use('/excel_upload', uploadexcelRouter);
app.use('/mb52', mb52Router);
app.use('/zcsr181', zcsr181Router);
app.use('/safetyne2', safetyNe2Router);
app.use('/zmb25', zmb25);
app.use('/all_stuff', allStuff);
app.use('/user_login_info', userLoginInfo);
app.use('/cs_warehouse_step1', csWarehouseStep1);
app.use('/tb_estimate_set', tbEstimateSet);
app.use('/login',loginIDM);
app.use('/tb_estimate_list', tbEstimateList);
app.use('/tb_transfer_list', tbTransferList);
app.use('/tb_transfer', tbTransfer);

module.exports = app;
