module.exports ={ userLogin: async (empid, password) =>{
  var axios = require('axios');
  var data = `<?xml version="1.0" encoding="utf-8"?>\r\n  
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                  xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
                  xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">\r\n   
                  <soap:Body>\r\n    
                    <Login xmlns="http://idm.pea.co.th/">\r\n     
                      <request>\r\n       
                        <InputObject>\r\n          
                          <Username>`+ empid +`</Username>\r\n           
                          <Password>`+ password +`</Password>\r\n       
                        </InputObject>\r\n        
                        <WSAuthenKey>e3358fc1-99ad-4b21-8237-7c9c8ba1c5dc</WSAuthenKey>\r\n      
                      </request>\r\n    
                    </Login>\r\n  
                  </soap:Body>\r\n
                </soap:Envelope>`;
  
  var config = {
    method: 'post',
    url: 'https://idm.pea.co.th/webservices/IdmServices.asmx',
    headers: { 
      'Content-Type': 'text/xml'
    },
    data : data
  };
  
  let resp = await axios(config)
  .then(function (response) {
    // console.log(response.data)
    let xmlParser = require('xml2json');
    let LoginresponseJson = JSON.parse(xmlParser.toJson(response.data))
    let LoginresponseText = LoginresponseJson["soap:Envelope"]["soap:Body"]["LoginResponse"]["LoginResult"]["ResultObject"]["Result"];
    return LoginresponseText;
  })
  .catch(function (error) {
    return error;
  });
  return resp
  // const fetch = require("node-fetch");
  // let myHeaders = new fetch.Headers();
  // myHeaders.append("Content-Type", "text/xml");

  // let raw = `<?xml version=\"1.0\" encoding=\"utf-8\"?>
  //           \n<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
  //           \n  <soap:Body>
  //           \n    <Login xmlns=\"http://idm.pea.co.th/\">
  //           \n      <request>
  //           \n        <InputObject>
  //           \n          <Username>`+empid+`</Username>
  //           \n          <Password>`+password+`</Password>
  //           \n        </InputObject>
  //           \n        <WSAuthenKey>e3358fc1-99ad-4b21-8237-7c9c8ba1c5dc</WSAuthenKey>
  //           \n      </request>
  //           \n    </Login>
  //           \n  </soap:Body>
  //           \n</soap:Envelope>`;

  // let requestOptions = {
  //     method: 'POST',
  //     headers: myHeaders,
  //     body: raw,
  //     redirect: 'follow'
  // };



  //   let response = await fetch("/webservices/IdmServices.asmx", requestOptions)
  //     .then(response => response.text())
  //     .then((responseText)=>{
  //       let parser = new DOMParser(),
  //       xmlDoc = parser.parseFromString(responseText, "text/xml");
  //       responseText = xmlDoc.getElementsByTagName("ResultObject")[0].childNodes[0].innerHTML;  
  //       console.log(responseText)
  //       return responseText;
        
  //     })
  //     .catch((error)=>{
  //       return error
  //     })
  //     return response
  // return empid

}
}
