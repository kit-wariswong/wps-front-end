module.exports = { userLoginData: async (empid) => {
    var axios = require('axios');
    var data = `<?xml version="1.0" encoding="utf-8"?>
                  <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                  <soap:Body>
                    <GetEmployeeInfoByEmployeeId xmlns="http://idm.pea.co.th/">
                      <request>
                        <InputObject>
                          <EmployeeId>${empid}</EmployeeId>
                          </InputObject>
                        <WSAuthenKey>93567815-dfbb-4727-b4da-ce42c046bfca</WSAuthenKey>
                        </request>
                      </GetEmployeeInfoByEmployeeId>
                    </soap:Body>
                  </soap:Envelope>`;

    var config = {
      method: 'post',
      url: 'https://idm.pea.co.th/webservices/EmployeeServices.asmx',
      headers: { 
        'Content-Type': 'text/xml'
      },
      data : data
    };

    let resp = await axios(config)
    .then(function (response) {
      // console.log(response.data)
      let xmlParser = require('xml2json');
      let LoginresponseJson = JSON.parse(xmlParser.toJson(response.data))
      let LoginresponseText = LoginresponseJson["soap:Envelope"]["soap:Body"]["GetEmployeeInfoByEmployeeIdResponse"]["GetEmployeeInfoByEmployeeIdResult"]["ResultObject"];
      return LoginresponseText;
    })
    .catch(function (error) {
      return error;
    });
    return resp
  },
};
