const express = require('express');
const zcsr181Controller = require('../controllers/zcsr181-controllers');
const router = express.Router();

/* GET users listing. */
router.get('/', zcsr181Controller.index);


module.exports = router;
