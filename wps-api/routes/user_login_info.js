const express = require('express');
const userLoginController = require('../controllers/userLoginController');
const router = express.Router();

/* GET users listing. */
router.get('/', userLoginController.index);

router.post('/add_user_loggedin', userLoginController.add_user_loggedin);


module.exports = router;
