const express = require('express');
const userController = require('../controllers/user-controllers');
const router = express.Router();

const passportJWT = require('../middlewares/passport-jwt');
const checkAdmin = require('../middlewares/check-admin');

/* GET users listing. */
router.get('/', userController.index);

router.get('/search_query_string', userController.search_query_string);

router.get('/show_params/:id', userController.show_params);

// POST Method
router.post('/insert_body/', userController.insert_body);

// login
router.post('/login', userController.login);

router.get('/login_info', userController.login_info);

// view profile
router.get('/user_profile', [passportJWT.isLogin], userController.getProfile);

// delete
router.delete('/:id', [passportJWT.isLogin, checkAdmin.isAdmin], userController.destroy);

module.exports = router;
