const express = require('express');
const allStuffController = require('../controllers/allStuffController');
const router = express.Router();

/* GET users listing. */
router.get('/', allStuffController.index);


module.exports = router;
