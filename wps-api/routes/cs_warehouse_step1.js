const express = require('express');
const cswarehouseStep1Controller = require('../controllers/cswarehouseStep1Controller');
const router = express.Router();

/* GET users listing. */
router.get('/', cswarehouseStep1Controller.index);

router.post('/add_cswarehouse_step1', cswarehouseStep1Controller.add_cswarehouse_step1);

router.post('/edit_cswarehouse_step1', cswarehouseStep1Controller.edit_cswarehouse_step1);

// delete
router.delete('/:csWarehouseS1Id', cswarehouseStep1Controller.destroy_cswarehouse_step1);


module.exports = router;
