const express = require('express');
const tbTransferListController = require('../controllers/tbTransferListController');
const router = express.Router();

/* GET users listing. */
router.get('/', tbTransferListController.index);

router.post('/add_tb_transfer_list', tbTransferListController.add_tb_transfer_list);

router.post('/edit_tb_transfer_list', tbTransferListController.edit_tb_transfer_list);

// // delete
router.delete('/:transferListId', tbTransferListController.destroy_tb_transfer_list);


module.exports = router;
