const express = require('express');
const excelUploadController = require('../controllers/excelupload-controllers');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const uuidv4 = require('uuid'); 

// upload file mir_upload
const storage25 = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/zmb25/')
    },
    filename: function (req, file, cb) {
        const newFileName = Date.now() + '-' + uuidv4.v4() + path.extname(file.originalname);
        cb(null, newFileName);
    }
})

const storage52 = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/mb52/')
    },
    filename: function (req, file, cb) {
        const newFileName = Date.now() + '-' + uuidv4.v4() + path.extname(file.originalname);
        cb(null, newFileName);
    }
})

const upload_excel_zmb25 = multer({ storage: storage25, limits: { fileSize: 100 * 1024 * 1024 } }); //limit 100MB

const upload_excel_mb52 = multer({ storage: storage52, limits: { fileSize: 100 * 1024 * 1024 } }); //limit 100MB

/* GET users listing. */
router.get('/', excelUploadController.index);
// name of client's html upload form must be 'zmb25_excel_file'
router.post('/zmb25_upload', [upload_excel_zmb25.single('zmb25_excel_file')] ,excelUploadController.zmb25_upload_report);

router.post('/mb52_upload', [upload_excel_mb52.single('mb52_excel_file')] ,excelUploadController.mb52_upload_report);

module.exports = router;
