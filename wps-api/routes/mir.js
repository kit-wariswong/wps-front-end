const express = require('express');
const mirController = require('../controllers/mir-controllers');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const uuidv4 = require('uuid'); 
const passportJWT = require('../middlewares/passport-jwt');

// upload file mir_upload
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/mir/')
    },
    filename: function (req, file, cb) {
        const newFileName = Date.now() + '-' + uuidv4.v4() + path.extname(file.originalname);
        cb(null, newFileName);
    }
})

const upload_excel = multer({ storage: storage, limits: { fileSize: 50 * 1024 * 1024 } }); //limit 50MB

/* GET users listing. */
router.get('/', mirController.index);
// name of client's html upload form must be 'mir_excel_file'
router.post('/mir_upload', [passportJWT.isLogin, upload_excel.single('mir_excel_file')] ,mirController.mir_upload_report);

module.exports = router;
