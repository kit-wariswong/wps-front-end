const express = require('express');
const zmb25Controller = require('../controllers/zmb25-controller');
const router = express.Router();

/* GET users listing. */
router.get('/', zmb25Controller.index);


module.exports = router;
