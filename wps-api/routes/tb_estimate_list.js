const express = require('express');
const tbEstimateListController = require('../controllers/tbEstimateListController');
const router = express.Router();

/* GET users listing. */
router.get('/', tbEstimateListController.index);

router.post('/add_tb_estimate_list', tbEstimateListController.add_tb_estimate_list);

router.post('/edit_tb_estimate_list', tbEstimateListController.edit_tb_estimate_list);

// // delete
router.delete('/:estimateSetId', tbEstimateListController.destroy_tb_estimate_list);


module.exports = router;
