const express = require('express');
const tbTransferController = require('../controllers/tbTransferController');
const router = express.Router();

/* GET users listing. */
router.get('/', tbTransferController.index);

router.post('/add_tb_transfer', tbTransferController.add_tb_transfer);

router.post('/edit_tb_transfer', tbTransferController.edit_tb_transfer);

// // delete
router.delete('/:transferId', tbTransferController.destroy_tb_transfer);


module.exports = router;
