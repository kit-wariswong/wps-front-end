const express = require('express');
const isoController = require('../controllers/iso-controllers');
const router = express.Router();
const passportJWT = require('../middlewares/passport-jwt');

/* GET users listing. */

// localhost:3000/iso/
router.get('/', isoController.index);

router.post('/add', [passportJWT.isLogin], isoController.add);

router.get('/edit_show', [passportJWT.isLogin], isoController.edit_show);

router.post('/edit', [passportJWT.isLogin], isoController.edit);

module.exports = router;
