const express = require('express');
const loginIDMController = require('../controllers/loginIDM-controller');
const router = express.Router();

router.post('/', loginIDMController.index);

module.exports = router;
