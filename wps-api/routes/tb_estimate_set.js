const express = require('express');
const tbEstimateSetController = require('../controllers/tbEstimateSetController');
const router = express.Router();

/* GET users listing. */
router.get('/', tbEstimateSetController.index);

router.post('/add_tb_estimate_set', tbEstimateSetController.add_tb_estimate_set);

router.post('/edit_tb_estimate_set', tbEstimateSetController.edit_tb_estimate_set);

// delete
router.delete('/:setId', tbEstimateSetController.destroy_tb_estimate_set);


module.exports = router;
