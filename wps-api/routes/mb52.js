const express = require('express');
const mb52Controller = require('../controllers/mb52-controllers');
const router = express.Router();

/* GET users listing. */
router.get('/', mb52Controller.index);


module.exports = router;
