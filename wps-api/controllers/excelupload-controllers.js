const model = require('../models/index');
const path = require('path');

const XLSX = require('xlsx');

exports.index = async function (req, res, next) {
    try {
        const raw_data = await model.Zmb25.findAll({
            // attributes: ['id', 'fullname'],
            // where: {
            //     mir_interface: ['eth1', 'eth2', 'eth7', '10GE1/4/0/9', 'GE1/2/0/11', 'GE1/2/0/15'],
            // },
            order: [['inventory_id', 'asc']]
        });

        const totalRecord = Object.keys(raw_data).length;
        // let avg_availability = 0;
        // for (let i = 0; i < totalRecord; i++) {
        //     avg_availability += raw_data[i].mir_availability;
        // }

        return res.status(200).json({
            total_record: totalRecord,
            // average: avg_availability / totalRecord,
            zmb25_data: raw_data,
        });
    } catch (err) {
        return res.status(500).json({
            message: err,
        });
    }
}

// upload excel
exports.zmb25_upload_report = async function (req, res, next) {
    try {
        //req.file.filename
        if (req.file) {
            const { filename } = req.file;
            // look for uploaded file in folder
            const uploadPath = `${path.resolve('./')}/uploads/zmb25/${filename}`;

            // read excel file
            const workbook = await XLSX.readFile(uploadPath, { nullError: true });
            const worksheet = workbook.Sheets[workbook.SheetNames[0]];

            if (worksheet[`B1`].v === 'วัสดุ' && worksheet[`E1`].v === 'MvT' && worksheet[`N1`].v == 'RCa') {
                // นำข้อมูลจาก excel เข้า json ถ้าเอาเข้าตัวแปรเลยจะ error ตรง row ที่ ไม่ค่า (undefinded)
                const jsondata = XLSX.utils.sheet_to_json(worksheet);
                const range = XLSX.utils.decode_range(worksheet['!ref']).e.r;
                let zmb25_rep = [];
                for (const item of jsondata.values()) {
                    // add rows to array
                    let data = {
                        inventory_id: item["   Plnt"],
                        stuff_id: item["วัสดุ"],
                        stuff_name: item["คำอธิบายวัสดุ"],
                        date_required: item["วันต้องการ"],
                        mvt: item["MvT"],
                        qty_needed: item[" ปม.ต้องการ"],
                        qty_removed: item["RemovedQty"],
                        qty_different: item["  ปริมาณผลต่าง"],
                        bun: item["BUn"],
                        netword_number: item["โครงข่าย"],
                        wbs_number: item["          องค์ประกอบ WBS"],
                        po_id: item["ใบสั่ง"],
                        reserve_number: item["เลขที่จอง"],
                        rca: item["RCa"],
                        user_added: "test uploaded"
                    }
                    // เอาข้อมูลจาก json เข้าตัวแปร
                    zmb25_rep.push(data);

                }

                // truncate and insert all to db
                await model.Zmb25.destroy({ truncate: true });
                // วนลูป insert ข้อมูล ทีละ 500 รายการ
                for (let index = 0; index < zmb25_rep.length; index+=1000) {
                    let zmb25_store = [];
                    for (let j = index; j < index+1000; j++) {
                        // ลูป 2 ใช้เก็บข้อมูลทีละชุดเพื่อออกไป insert
                        if (j < zmb25_rep.length) {
                            if (j == index) {
                                let data = zmb25_rep[j];
                                zmb25_store.push(data);
                            } else {
                                let data = zmb25_rep[j];
                                zmb25_store.push(data);
                            }
                        }
                    }
                    // insert ข้อมูลลงใน db
                    await model.Zmb25.bulkCreate(zmb25_store, { raw: true });
                }

                return res.status(200).json({
                    message: `(ZMB25) นำเข้า Excel ${range} รายการ, Upload ${zmb25_rep.length} รายการ และ บันทึกลงฐานข้อมูลเรียบร้อย`,
                });
            } else {
                return res.status(400).json({
                    message: `ตรวจสอบความถูกต้องของไฟล์ที่อัพโหลด`,
                });
            }
        } else {
            return res.status(404).json({
                message: `ไม่พบ file ที่อัพโหลด`,
            });
        }

    } catch (err) {
        return res.status(500).json({
            message: `ตรวจสอบไฟล์ที่อัพโหลด ${err}`,
        });
    }
}

exports.mb52_upload_report = async function (req, res, next) {
    try {
        //req.file.filename
        if (req.file) {
            const { filename } = req.file;
            // look for uploaded file in folder
            const uploadPath = `${path.resolve('./')}/uploads/mb52/${filename}`;

            // read excel file
            const workbook = await XLSX.readFile(uploadPath, { nullError: true });
            const worksheet = workbook.Sheets[workbook.SheetNames[0]];

            if (worksheet[`B1`].v === 'ที่เก็บสินค้า' && worksheet[`E1`].v === 'แบทช์' && worksheet[`I1`].v == 'การส่ง/การโอน') {
                // นำข้อมูลจาก excel เข้า json ถ้าเอาเข้าตัวแปรเลยจะ error ตรง row ที่ ไม่ค่า (undefinded)
                const jsondata = XLSX.utils.sheet_to_json(worksheet);
                const range = XLSX.utils.decode_range(worksheet['!ref']).e.r;
                let mb52_rep = [];
                for (const item of jsondata.values()) {
                    // add rows to array
                    let data = {
                        invId: item["โรงงาน"],
                        stockHolder: item["ที่เก็บสินค้า"],
                        equipmentId: item["วัสดุ"],
                        stuffName: item["คำอธิบายวัสดุ"],
                        batch: item["แบทช์"],
                        counter: item["หน่วยนับพื้นฐาน"],
                        stockAvailable: item["ที่ใช้ได้"],
                        value: item["ไม่จำกัดค่า"],
                        transfering: item["การส่ง/การโอน"],
                        suspend: item["ระงับ"],
                    }
                    // เอาข้อมูลจาก json เข้าตัวแปร
                    mb52_rep.push(data);

                }

                // truncate and insert all to db
                await model.mb52_fetch.destroy({
                    truncate: true
                });
                // วนลูป insert ข้อมูล ทีละ 500 รายการ
                for (let index = 0; index < mb52_rep.length; index+=1000) {
                    let mb52_store = [];
                    for (let j = index; j < index+1000; j++) {
                        // ลูป 2 ใช้เก็บข้อมูลทีละชุดเพื่อออกไป insert
                        if (j < mb52_rep.length) {
                            if (j == index) {
                                let data = mb52_rep[j];
                                mb52_store.push(data);
                            } else {
                                let data = mb52_rep[j];
                                mb52_store.push(data);
                            }
                        }
                    }
                    // insert ข้อมูลลงใน db
                    // await model.mb52.bulkCreate(mb52_store, { raw: true });
                    await model.mb52_fetch.bulkCreate(mb52_store, { raw: true });
                }

                return res.status(200).json({
                    message: `(MB52) นำเข้า Excel ${range} รายการ, Upload ${mb52_rep.length} รายการ และ บันทึกลงฐานข้อมูลเรียบร้อย`,
                });
            } else {
                return res.status(400).json({
                    message: `ตรวจสอบความถูกต้องของไฟล์ที่อัพโหลด`,
                });
            }
        } else {
            return res.status(404).json({
                message: `ไม่พบ file ที่อัพโหลด`,
            });
        }

    } catch (err) {
        return res.status(500).json({
            message: `ตรวจสอบไฟล์ที่อัพโหลด ${err}`,
        });
    }
}