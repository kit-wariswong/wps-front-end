const model = require('../models/index');


exports.index = async function (req, res, next) {
    try {
        // Pagination
        // users?page=1&page_size=2
        const { page, page_size } = req.query;
        const myPage = page ? parseInt(page) : 1;
        // default 3 records
        const myPageSize = page_size ? parseInt(page_size) : 3;

        const data = await model.tb_estimate_set.findAll({
            offset: (myPage - 1) * myPageSize,
            limit: myPageSize,
            include: [
                {
                    model: model.tb_estimate_list,
                    as: 'tb_estimate_set_tb_estimate_list_associate',
                    attributes: ['estimateSetId', 'equipmentId'],
                    // ความสัมพันธ์ของ estimate list กับ mb52
                    include: [
                        {
                            model: model.mb52,
                            as: 'estimate_belongs_to_mb52',
                            attributes: ['invId', 'stockHolder', 'equipmentId', 'stuffName']
                        }
                    ],
                },
                {
                    model: model.tb_cs_warehouse_step1,
                    as: 'tb_estimate_set_tb_cs_warehouse_step1_associate',
                    attributes: ['jobName', 'jobCustomer', 'warehouseId'],
                    include: [
                        {
                            model: model.tb_user_login_info,
                            as: 'tb_cs_warehouse_step1_belongs_to_tb_user_login_info',
                            attributes: ['userId']
                        }
                    ],
                },
            ]
        });
        const totalRecord = await model.tb_estimate_set.count();
        return res.status(201).json({
            message: `จำนวนทั้งหมด ${totalRecord}`,
            total: totalRecord,
            data: data
        });
    } catch (err) {
        return res.status(500).json({
            message: err,
        });
    }
}

exports.add_tb_estimate_set = async function (req, res, next) {
    // รับค่าจาก req
    const {
        setName,
    } = req.body;

    // check duplicate
    try {
        const setDuplicate = await model.tb_estimate_set.findOne({
            where: {
                setName: setName,
            },
            include: [
                {
                    model: model.tb_estimate_list,
                    as: 'tb_estimate_set_tb_estimate_list_associate',
                    attributes: ['estimateSetId', 'equipmentId'],
                    include: [
                        {
                            model: model.mb52,
                            as: 'estimate_belongs_to_mb52',
                            attributes: ['invId', 'stockHolder', 'equipmentId', 'stuffName']
                        }
                    ],
                },
            ]
        });
        // check duplicate value
        if (setDuplicate) {
            try {
                return res.status(400).json({
                    message: 'มีข้อมูลในระบบแล้ว',
                    data: setDuplicate,
                });
            } catch (err) {
                return res.status(500).json({
                    message: 'เกิดข้อผิดพลาด',
                    error_messages: err
                });
            }
        }
    } catch (err) {
        return res.status(500).json({
            message: 'DB error',
            error_messages: err
        });
    }

    try {
        const newSet = await model.tb_estimate_set.create({
            setName: setName,
        });
        return res.status(201).json({
            message: 'บันทึกข้อมูลสำเร็จ',
            data: newSet,
        });
    } catch (err) {
        return res.status(500).json({
            info: `เกิดข้อผิดพลาด`,
            messages: err,
        });
    }
}

exports.edit_tb_estimate_set = async function (req, res, next) {
    // รับค่าจาก req
    const {
        setId,
        newSetName,
    } = req.body;

    // check duplicate
    try {
        const setToEdit = await model.tb_estimate_set.findOne({
            where: {
                setId: setId
            }
        });

        // check duplicate value
        if (setToEdit) {
            try {
                await model.tb_estimate_set.update(
                    {
                        setName: newSetName
                    },
                    { where: { setId: setToEdit.setId } }
                );

                const jobEdited = await model.tb_estimate_set.findOne({
                    where: {
                        setId: setToEdit.setId
                    }
                });

                return res.status(201).json({
                    message: 'แก้ไขข้อมูลสำเร็จ',
                    job_to_edit: setToEdit,
                    job_edited: jobEdited
                });
            } catch (err) {
                return res.status(500).json({
                    message: 'เกิดข้อผิดพลาด',
                    error_messages: err
                });
            }
        } else {
            return res.status(404).json({
                message: 'ไม่พบข้อมูลในระบบ',
            });
        }

    } catch (err) {
        return res.status(500).json({
            message: 'DB error',
            error_messages: err
        });
    }
}

// delete
exports.destroy_tb_estimate_set = async function (req, res, next) {
    const { setId } = req.params;

    try {
        const taskToDestroy = await model.tb_estimate_set.findOne({
            where: {
                setId: setId
            }
        });

        const resultNumber = await model.tb_estimate_set.destroy({
            where: {
                setId: setId
            },
        });

        if (resultNumber === 0) {
            return res.status(404).json({
                message: `ไม่พบข้อมูล ${setId}`
            })
        } else {
            return res.status(200).json({
                taskToDestroy: taskToDestroy,
                data: `ลบรายการ ID: ${setId} แล้ว`
            })
        }
    } catch (err) {
        return res.status(500).json({
            warning: 'กรุณาตรวจสอบความสัมพันธ์ของข้อมูล/ฐานข้อมูลมีปัญหา',
            message: err
        })
    }
}