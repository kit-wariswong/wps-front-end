const userLogin = require('../services/userLogin.js')
const userLoginData = require('../services/userLoginData.js')
// const test = require('../services/test.js')


exports.index = async function (req, res, next) {
    try {

        const { empid, password } = req.body;
        // let resp = await test.test(empid)
        let LoginResponse = await userLogin.userLogin(empid,password)
        // let LoginresponseJson = JSON.parse(LoginResponse)
        // let LoginresponseText = LoginresponseJson["soap:Envelope"]["soap:Body"]["LoginResponse"]["LoginResult"]["ResultObject"]["Result"];
        // console.log(LoginResponse)
        
        if(LoginResponse == "true")
        {
            let userLoginDataResponce = await userLoginData.userLoginData(empid)
            console.log(userLoginDataResponce)
            return res.status(201).json({
                message: `login_success`,
                data: userLoginDataResponce
            });
        }else{
            return res.status(201).json({
                message: `login_fail`,
                data: LoginResponse
            });
        }   
        // let LoginResponse = await userLogin.userLogin(empid,password)
        //     return res.status(201).json({
        //         message: `ยินดีต้อนรับ`,
        //         data: LoginResponse
        //     });     
        
        // return res.status(200).json({
        //     message: resp,
        // });
    } catch (err) {
        return res.status(500).json({
            message: err,
        });
    }
}