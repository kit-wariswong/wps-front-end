const model = require('../models/index');


exports.index = async function (req, res, next) {
    try {
        // Pagination
        // users?page=1&page_size=2
        const { page, page_size } = req.query;
        const myPage = page ? parseInt(page) : 1;
        // default 3 records
        const myPageSize = page_size ? parseInt(page_size) : 3;

        const order = await model.zcsr181.findAll({
            offset: (myPage - 1) * myPageSize,
            limit: myPageSize,
        });
        const totalRecord = await model.zcsr181.count();
        return res.status(201).json({
            message: `จำนวนทั้งหมด ${totalRecord}`,
            total: totalRecord,
            data: order
        });
    } catch (err) {
        return res.status(500).json({
            message: err,
        });
    }
}