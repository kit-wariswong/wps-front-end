const model = require('../models/index');


exports.index = async function (req, res, next) {
    try {
        // Pagination
        // users?page=1&page_size=2
        const { page, page_size } = req.query;
        const myPage = page ? parseInt(page) : 1;
        // default 3 records
        const myPageSize = page_size ? parseInt(page_size) : 3;

        const data = await model.tb_cs_warehouse_step1.findAll({
            offset: (myPage - 1) * myPageSize,
            limit: myPageSize,
        });
        const totalRecord = await model.tb_cs_warehouse_step1.count();
        return res.status(201).json({
            message: `จำนวนทั้งหมด ${totalRecord}`,
            total: totalRecord,
            data: data
        });
    } catch (err) {
        return res.status(500).json({
            message: err,
        });
    }
}

exports.add_cswarehouse_step1 = async function (req, res, next) {
    // รับค่าจาก req
    const {
        setId, //setId ต้องมีอยู่ใน tb_estimate_sets
        jobName,
        jobCustomer,
        userId, //ต้องมี userId ในตาราง tb_user_login_infos
        warehouseId
    } = req.body;

    // check duplicate
    try {
        const taskDuplicate = await model.tb_cs_warehouse_step1.findOne({
            where: {
                setId: setId,
                jobName: jobName,
                jobCustomer: jobCustomer,
                userId: userId,
                warehouseId: warehouseId
            }
        });
        // check duplicate value
        if (taskDuplicate) {
            try {
                return res.status(400).json({
                    message: 'มีข้อมูลในระบบแล้ว กรุณาใช้การแก้ไข',
                    data: taskDuplicate,
                });
            } catch (err) {
                return res.status(500).json({
                    message: 'เกิดข้อผิดพลาด',
                    error_messages: err
                });
            }
        }
    } catch (err) {
        return res.status(500).json({
            message: 'DB error',
            error_messages: err
        });
    }

    try {
        const newTask = await model.tb_cs_warehouse_step1.create({
            setId: setId,
            jobName: jobName,
            jobCustomer: jobCustomer,
            userId: userId,
            warehouseId: warehouseId
        });
        return res.status(201).json({
            message: 'บันทึกข้อมูลสำเร็จ',
            data: newTask,
        });
    } catch (err) {
        return res.status(500).json({
            info: `เกิดข้อผิดพลาด`,
            messages: err,
        });
    }
}

exports.edit_cswarehouse_step1 = async function (req, res, next) {
    // รับค่าจาก req
    const {
        csWarehouseS1Id,
        newSetId, //setId ต้องมีอยู่ใน tb_estimate_sets
        newJobName,
        newJobCustomer,
        newUserId, //ต้องมี userId ในตาราง tb_user_login_infos
        newWarehouseId
    } = req.body;

    // check duplicate
    try {
        const taskToEdit = await model.tb_cs_warehouse_step1.findOne({
            where: {
                csWarehouseS1Id: csWarehouseS1Id
            }
        });

        // check duplicate value
        if (taskToEdit) {
            try {
                await model.tb_cs_warehouse_step1.update(
                    {
                        setId: newSetId, //setId ต้องมีอยู่ใน tb_estimate_sets
                        jobName: newJobName,
                        jobCustomer: newJobCustomer,
                        userId: newUserId, //ต้องมี userId ในตาราง tb_user_login_infos
                        warehouseId: newWarehouseId
                    },
                    { where: { csWarehouseS1Id: taskToEdit.csWarehouseS1Id } }
                );

                const taskEdited = await model.tb_cs_warehouse_step1.findOne({
                    where: {
                        csWarehouseS1Id: taskToEdit.csWarehouseS1Id
                    }
                });

                return res.status(201).json({
                    message: 'แก้ไขข้อมูลสำเร็จ',
                    task_to_edit: taskToEdit,
                    task_edited: taskEdited
                });
            } catch (err) {
                return res.status(500).json({
                    message: 'เกิดข้อผิดพลาด',
                    error_messages: err
                });
            }
        } else {
            return res.status(404).json({
                message: 'ไม่พบข้อมูลในระบบ',
            });
        }

    } catch (err) {
        return res.status(500).json({
            message: 'DB error',
            error_messages: err
        });
    }
}

// delete cswarehouse_step1
exports.destroy_cswarehouse_step1 = async function (req, res, next) {
    const { csWarehouseS1Id } = req.params;

    try {
        const taskToDestroy = await model.tb_cs_warehouse_step1.findOne({
            where: {
                csWarehouseS1Id: csWarehouseS1Id
            }
        });

        const resultNumber = await model.tb_cs_warehouse_step1.destroy({
            where: {
                csWarehouseS1Id: csWarehouseS1Id
            }
        });

        if (resultNumber === 0) {
            return res.status(404).json({
                message: `ไม่พบข้อมูล ${csWarehouseS1Id}`
            })
        } else {
            return res.status(200).json({
                taskToDestroy: taskToDestroy,
                data: `ลบรายการ ID: ${csWarehouseS1Id} แล้ว`
            })
        }
    } catch (err) {
        return res.status(500).json({
            message: err
        })
    }
}