const model = require('../models/index');


exports.index = async function (req, res, next) {
    try {
        // Pagination
        // users?page=1&page_size=2
        const { page, page_size } = req.query;
        const myPage = page ? parseInt(page) : 1;
        // default 3 records
        const myPageSize = page_size ? parseInt(page_size) : 3;

        const data = await model.tb_transfer_list.findAll({
            offset: (myPage - 1) * myPageSize,
            limit: myPageSize,
            include: [
                {
                    model: model.mb52,
                    as: 'stuff_belongs_to_mb52'
                },
                {
                    model: model.tb_transfer,
                    as: 'transfer_list_belongs_to_tb_transfer',
                },
            ]
        });
        const totalRecord = await model.tb_transfer_list.count();
        return res.status(201).json({
            message: `จำนวนทั้งหมด ${totalRecord}`,
            total: totalRecord,
            data: data
        });
    } catch (err) {
        return res.status(500).json({
            message: err,
        });
    }
}

exports.add_tb_transfer_list = async function (req, res, next) {
    // รับค่าจาก req
    const {
        transferId, 
        stuffToAdd, //id ของ mb52
        equipmentValue
    } = req.body;

    var stuffId = '';

    // Add m52_fetches ลงใน mb52s
    try {
        const getStuff = await model.mb52_fetch.findOne({
            where: {
                id: stuffToAdd
            },
        });

        const checkStuff = await model.mb52.findOne({
            where: {
                invId: getStuff.invId,
                stockHolder: getStuff.stockHolder,
                equipmentId: getStuff.equipmentId,
                stuffName: getStuff.stuffName,
                batch: getStuff.batch,
            },
        });

        // ไม่มีข้อมูลใน mb52s จะนำข้อมูลที่ดึงมาจาก mb52_fetch โดยใช้ id มาเพิ่มเข้าไปใน mb52s
        if (!checkStuff) {
            const addedStuff = await model.mb52.create({
                invId: getStuff.invId,
                stockHolder: getStuff.stockHolder,
                equipmentId: getStuff.equipmentId,
                stuffName: getStuff.stuffName,
                batch: getStuff.batch,
                counter: getStuff.counter,
                stockAvailable: getStuff.stockAvailable,
                value: getStuff.value,
                transfering: getStuff.transfering,
                suspend: getStuff.suspend,
            });
            stuffId = addedStuff.id;
        } else {
            // ถ้ามีข้อมูลใน mb52s แล้ว จะเอาไอดีมาอ้างอิง
            stuffId = checkStuff.id;
        }

    } catch (err) {
        return res.status(500).json({
            message: 'เกิดข้อผิดพลาด ค้นหาพัสดุ',
            error_messages: err
        });
    }

    // check duplicate
    try {
        const setDuplicate = await model.tb_transfer_list.findOne({
            where: {
                transferId: transferId,
                equipmentId: stuffId,
                equipmentValue: equipmentValue
            },
            include: [
                {
                    model: model.tb_transfer,
                    as: 'transfer_list_belongs_to_tb_transfer',
                    attributes: ['warehouseIdRequester', 'warehouseIdEquipOwner', 'userId', 'sessionId'],
                    include: [
                        {
                            model: model.tb_transfer_status,
                            as: 'tb_transfer_belongs_to_tb_transfer_status',
                            attributes: ['transferStatus']
                        }
                    ],
                },
                {
                    model: model.mb52,
                    as: 'stuff_belongs_to_mb52',
                    attributes: ['invId', 'stockHolder', 'equipmentId', 'stuffName', 'batch']
                }
            ]
        });
        // check duplicate value
        if (setDuplicate) {
            try {
                return res.status(400).json({
                    message: 'มีข้อมูลในระบบแล้ว',
                    data: setDuplicate,
                });
            } catch (err) {
                return res.status(500).json({
                    message: 'เกิดข้อผิดพลาด ค้นหาข้อมูลซ้ำ',
                    error_messages: err
                });
            }
        }
    } catch (err) {
        return res.status(500).json({
            message: 'DB error',
            error_messages: err
        });
    }

    try {
        const newTransferList = await model.tb_transfer_list.create({
            transferId: transferId,
            equipmentId: stuffId,
            equipmentValue: equipmentValue
        });
        return res.status(201).json({
            message: 'บันทึกข้อมูลสำเร็จ',
            data: newTransferList,
        });
    } catch (err) {
        return res.status(500).json({
            info: `ผิดพลาดบันทึกข้อมูล transfer list`,
            messages: err,
        });
    }
}

exports.edit_tb_transfer_list = async function (req, res, next) {
    // รับค่าจาก req
    const {
        transferListId,
        newTransferId,
        newEquipmentId,
        newEquipmentValue,
    } = req.body;

    var stuffId = '';

    try {
        const getStuff = await model.mb52_fetch.findOne({
            where: {
                id: newEquipmentId
            },
        });

        const checkStuff = await model.mb52.findOne({
            where: {
                invId: getStuff.invId,
                stockHolder: getStuff.stockHolder,
                equipmentId: getStuff.equipmentId,
                stuffName: getStuff.stuffName,
                batch: getStuff.batch,
            },
        });

        // ไม่มีข้อมูลใน mb52s จะนำข้อมูลที่ดึงมาจาก mb52_fetch โดยใช้ id มาเพิ่มเข้าไปใน mb52s
        if (!checkStuff) {
            const addedStuff = await model.mb52.create({
                invId: getStuff.invId,
                stockHolder: getStuff.stockHolder,
                equipmentId: getStuff.equipmentId,
                stuffName: getStuff.stuffName,
                batch: getStuff.batch,
                counter: getStuff.counter,
                stockAvailable: getStuff.stockAvailable,
                value: getStuff.value,
                transfering: getStuff.transfering,
                suspend: getStuff.suspend,
            });
            stuffId = addedStuff.id;
        } else {
            // ถ้ามีข้อมูลใน mb52s แล้ว จะเอาไอดีมาอ้างอิง
            stuffId = checkStuff.id;
        }

    } catch (err) {
        return res.status(500).json({
            message: 'เกิดข้อผิดพลาด ค้นหาพัสดุ',
            error_messages: err
        });
    }

    // verify data
    try {
        const listToEdit = await model.tb_transfer_list.findOne({
            where: {
                transferListId: transferListId
            }
        });

        // ถ้ามีข้อมูลก็ทำการแก้ไข
        if (listToEdit) {
            try {
                await model.tb_transfer_list.update(
                    {
                        transferId: newTransferId,
                        equipmentId: stuffId,
                        equipmentValue: newEquipmentValue,
                    },
                    { where: { transferListId: listToEdit.transferListId } }
                );

                const jobEdited = await model.tb_transfer_list.findOne({
                    where: {
                        transferListId: listToEdit.transferListId
                    }
                });

                return res.status(201).json({
                    message: 'แก้ไขข้อมูลสำเร็จ',
                    job_to_edit: listToEdit,
                    job_edited: jobEdited
                });
            } catch (err) {
                return res.status(500).json({
                    message: 'เกิดข้อผิดพลาด ระหว่างการแก้ไขข้อมูล / ตรวจสอบความสัมพันธ์ของข้อมูล',
                    error_messages: err
                });
            }
        } else {
            return res.status(404).json({
                message: 'ไม่พบข้อมูลในระบบ',
            });
        }

    } catch (err) {
        return res.status(500).json({
            message: 'DB error',
            error_messages: err
        });
    }
}

// delete
exports.destroy_tb_transfer_list = async function (req, res, next) {
    const { transferListId } = req.params;

    try {
        const taskToDestroy = await model.tb_transfer_list.findOne({
            where: {
                transferListId: transferListId
            }
        });

        const resultNumber = await model.tb_transfer_list.destroy({
            where: {
                transferListId: transferListId
            },
        });

        if (resultNumber === 0) {
            return res.status(404).json({
                message: `ไม่พบข้อมูล ${transferListId}`
            })
        } else {
            return res.status(200).json({
                taskToDestroy: taskToDestroy,
                data: `ลบรายการ ID: ${transferListId} แล้ว`
            })
        }
    } catch (err) {
        return res.status(500).json({
            warning: 'กรุณาตรวจสอบความสัมพันธ์ของข้อมูล/ฐานข้อมูลมีปัญหา',
            message: err
        })
    }
}