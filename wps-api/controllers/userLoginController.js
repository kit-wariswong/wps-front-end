const model = require('../models/index');


exports.index = async function (req, res, next) {
    try {
        // Pagination
        // users?page=1&page_size=2
        const { page, page_size } = req.query;
        const myPage = page ? parseInt(page) : 1;
        // default 3 records
        const myPageSize = page_size ? parseInt(page_size) : 3;

        const users = await model.tb_user_login_info.findAll({
            // attributes: ['id', 'fullname'],
            // attributes: {
            //     exclude: ['password']
            // },
            offset: (myPage - 1) * myPageSize,
            limit: myPageSize,
            order: [['userLastLogin', 'desc']],
            include: [
                // เรียกข้อมูลจากตาราง tb_cs_warehouse_step1 ที่ user นี้ มีความสัมพันธ์
                {
                    model: model.tb_cs_warehouse_step1,
                    as: 'tb_user_login_info_tb_cs_warehouse_step1_associate',
                    attributes: ['csWarehouseS1Id', 'jobName', 'jobCustomer', 'warehouseId'],
                    // เรียกข้อมูลจากตาราง estimate_sets ที่ tb_cs_warehouse_step1 ของ user นี้ มีความสัมพันธ์
                    include: [
                        {
                            model: model.tb_estimate_set,
                            as: 'tb_cs_warehouse_step1_belongs_to_tb_estimate_set',
                            attributes: ['setId', 'setName']
                        }
                    ],
                },
                // เรียกข้อมูลจากตาราง tb_cs_warehouse_step2 ที่ user นี้ มีความสัมพันธ์
                {
                    model: model.tb_cs_warehouse_step2,
                    as: 'tb_user_login_info_tb_cs_warehouse_step2_associate',
                    attributes: ['csWarehouseS2Id', 'wbsNumber'],
                    // เรียกข้อมูลจากตาราง zcsr181 ที่ tb_cs_warehouse_step2 ของ user นี้ มีความสัมพันธ์
                    include: [
                        {
                            model: model.zcsr181,
                            as: 'tb_cs_warehouse_step2_belongs_to_zcsr181',
                            attributes: ['requestorName', 'wbsNumber']
                        }
                    ],
                },
            ]
        });
        const totalRecord = await model.tb_user_login_info.count();
        return res.status(201).json({
            message: `จำนวน user login: ${totalRecord}`,
            total: totalRecord,
            users: users
        });
    } catch (err) {
        return res.status(500).json({
            message: err,
        });
    }
}

exports.add_user_loggedin = async function (req, res, next) {
    // รับค่าจาก req
    const {
        userId,
        userLastLogin,
        userIp
    } = req.body;

    // check duplicate userId
    try {
        const userIdFromDb = await model.tb_user_login_info.findOne({
            where: {
                userId: userId
            }
        });
        // เจอ user ซ้ำ จะทำการ update ข้อมูลเดิม
        if (userIdFromDb) {
            try {
                const userIdUpdate = await model.tb_user_login_info.update(
                    {
                        userLastLogin: userLastLogin,
                        userIp: userIp
                    },
                    { where: { userId: userIdFromDb.userId } }
                );
                return res.status(201).json({
                    message: 'อัพเดทข้อมูลสำเร็จ',
                    data: userIdUpdate,
                });
            } catch (err) {
                return res.status(500).json({
                    message: 'เกิดข้อผิดพลาด',
                    error_messages: err
                });
            }
        }
    } catch (err) {
        return res.status(500).json({
            message: 'DB error',
            error_messages: err
        });
    }

    try {
        const newLoggedinUser = await model.tb_user_login_info.create({
            userId: userId,
            userLastLogin: userLastLogin,
            userIp: userIp
        });
        return res.status(201).json({
            message: 'บันทึกข้อมูลสำเร็จ',
            data: newLoggedinUser,
        });
    } catch (err) {
        return res.status(500).json({
            info: `เกิดข้อผิดพลาด`,
            messages: err,
        });
    }
}

// exports.search_query_string = async function (req, res, next) {
//     const { fullname } = req.query;
//     // res.send(`get /user/search_query_string and respond with searched users. params: ${fullname}, ${age}`);
//     try {
//         const users = await model.NWUser.findAll({
//             attributes: {
//                 exclude: ['password']
//             },
//             where: {
//                 fullname: { [Op.like]: `%${fullname}%` }
//             }
//         });

//         if (users.length === 0) {
//             return res.status(404).json({
//                 message: 'ไม่พบข้อมูล'
//             })
//         } else {
//             return res.status(200).json({
//                 data: users
//             })
//         }
//     } catch (err) {
//         return res.status(500).json({
//             message: err
//         })
//     }
// }

// exports.show_params = async function (req, res, next) {
//     const { id } = req.params;
//     // res.send(`get /user/show_params/:id and respond with searched users. params: ${id}`);

//     try {
//         const users = await model.NWUser.findByPk(id, {
//             attributes: {
//                 exclude: ['password']
//             },
//         });

//         if (!users) {
//             return res.status(404).json({
//                 message: `ไม่พบข้อมูลผู้ใช้ ${id}`
//             })
//         } else {
//             return res.status(200).json({
//                 data: users
//             })
//         }
//     } catch (err) {
//         return res.status(500).json({
//             message: err
//         })
//     }
// }

// // POST Method
// exports.insert_body = async function (req, res, next) {
//     const { fullname, email, password } = req.body;

//     // check duplicate email
//     try {
//         const userEmail = await model.NWUser.findOne({
//             where: {
//                 email: email
//             }
//         });
//         if (userEmail) {
//             return res.status(400).json({
//                 message: 'Email ถูกใช้งานในระบบแล้ว'
//             });
//         }
//     } catch (err) {
//         return res.status(500).json({
//             message: 'DB error',
//             error_messages: err
//         });
//     }

//     try {
//         const hash = await argon2.hash(password);
//         // insert to db left db(model) right insert value
//         const newUser = await model.NWUser.create({
//             fullname: fullname,
//             email: email,
//             password: hash
//         });
//         return res.status(201).json({
//             message: 'สมัครสำเร็จ',
//             user: newUser
//         });
//     } catch (err) {
//         return res.status(400).json({
//             message: 'ไม่สำเร็จ',
//             error_messages: err
//         });
//     }

// }

// // login
// exports.login = async function (req, res, next) {

//     const { email, password } = req.body;

//     try {
//         // check email with db
//         const user = await model.NWUser.findOne({
//             where: {
//                 email: email
//             }
//         });
//         if (!user) {
//             return res.status(404).json({
//                 message: 'ไม่พบข้อมูล'
//             });
//         }
//         // check input pw to db pw
//         const isValid = await argon2.verify(user.password, password);
//         if (!isValid) {
//             return res.status(401).json({
//                 message: 'Something wrong'
//             });
//         }

//         // send token
//         const token = jwt.sign(
//             { user_id: user.id, role: user.role },
//             process.env.JWT_KEY,
//             {
//                 expiresIn: "2d"
//             }
//         );

//         return res.status(200).json({
//             message: `Welcome ${user.fullname}`,
//             access_token: token
//         });
//     } catch (err) {
//         return res.status(500).json({
//             message: 'ไม่สำเร็จ',
//             error_messages: err
//         });
//     }

// }

// exports.login_info = async function (req, res, next) {

//     try {
//         const user = await model.tb_user_login_info.findAll();
//         if (!user) {
//             return res.status(404).json({
//                 message: 'ไม่พบข้อมูล'
//             });
//         }

//         return res.status(200).json({
//             data: user,
//         });
//     } catch (err) {
//         return res.status(500).json({
//             message: 'ไม่สำเร็จ',
//             error_messages: err
//         });
//     }

// }

// exports.getProfile = async function (req, res, next) {

//     return res.status(200).json({
//         user: {
//             id: req.user.id,
//             fullname: req.user.fullname,
//             email: req.user.email,
//             create_at: req.user.create_at
//         }
//     });
// }

// // delete user
// exports.destroy = async function (req, res, next) {
//     const { id } = req.params;

//     try {
//         const resultNumber = await model.NWUser.destroy({
//             where: {
//                 id: id
//             }
//         });

//         if (resultNumber === 0) {
//             return res.status(404).json({
//                 message: `ไม่พบข้อมูลผู้ใช้ ${id}`
//             })
//         } else {
//             return res.status(200).json({
//                 data: `ลบผู้ใช้ ID: ${id} แล้ว`
//             })
//         }
//     } catch (err) {
//         return res.status(500).json({
//             message: err
//         })
//     }
// }


