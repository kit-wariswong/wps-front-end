const model = require('../models/index');


exports.index = async function (req, res, next) {
    try {
        // Pagination
        // users?page=1&page_size=2
        const { page, page_size } = req.query;
        const myPage = page ? parseInt(page) : 1;
        // default 3 records
        const myPageSize = page_size ? parseInt(page_size) : 3;

        const data = await model.tb_transfer.findAll({
            offset: (myPage - 1) * myPageSize,
            limit: myPageSize,
            include: [
                {
                    model: model.tb_transfer_list,
                    as: 'tb_transfer_tb_transfer_list_associate'
                },
                {
                    model: model.tb_transfer_status,
                    as: 'tb_transfer_belongs_to_tb_transfer_status',
                },
            ]
        });
        const totalRecord = await model.tb_transfer.count();
        return res.status(201).json({
            message: `จำนวนทั้งหมด ${totalRecord}`,
            total: totalRecord,
            data: data
        });
    } catch (err) {
        return res.status(500).json({
            message: err,
        });
    }
}

exports.add_tb_transfer = async function (req, res, next) {
    // รับค่าจาก req
    const {
        warehouseIdRequester,
        warehouseIdEquipOwner, //id ของ mb52
        userId,
        sessionId,
        transferStatus //id ของ tb_transfer_statuses
    } = req.body;

    // check duplicate
    try {
        const checkDuplicate = await model.tb_transfer.findOne({
            where: {
                warehouseIdRequester: warehouseIdRequester,
                warehouseIdEquipOwner: warehouseIdEquipOwner,
                userId: userId,
                sessionId: sessionId,
            },
            include: [
                {
                    model: model.tb_transfer_status,
                    as: 'tb_transfer_belongs_to_tb_transfer_status',
                    attributes: ['transferStatus']
                }
            ]
        });
        // check duplicate value
        if (checkDuplicate) {
            try {
                return res.status(400).json({
                    message: 'มีข้อมูลในระบบแล้ว',
                    data: checkDuplicate,
                });
            } catch (err) {
                return res.status(500).json({
                    message: 'เกิดข้อผิดพลาด ค้นหาข้อมูลซ้ำ',
                    error_messages: err
                });
            }
        }
    } catch (err) {
        return res.status(500).json({
            message: 'DB error',
            error_messages: err
        });
    }

    try {
        const newTransfer = await model.tb_transfer.create({
            warehouseIdRequester: warehouseIdRequester,
            warehouseIdEquipOwner: warehouseIdEquipOwner,
            userId: userId,
            sessionId: sessionId,
            transferStatus: transferStatus
        });
        return res.status(201).json({
            message: 'บันทึกข้อมูลสำเร็จ',
            data: newTransfer,
        });
    } catch (err) {
        return res.status(500).json({
            info: `ผิดพลาดบันทึกข้อมูล transfer list`,
            messages: err,
        });
    }
}

exports.edit_tb_transfer = async function (req, res, next) {
    // รับค่าจาก req
    const {
        transferId,
        newWarehouseIdRequester,
        newWarehouseIdEquipOwner,
        newUserId,
        newSessionId,
        newTransferStatus,
    } = req.body;

    var stuffId = '';

    // verify data
    try {
        const taskToEdit = await model.tb_transfer.findOne({
            where: {
                transferId: transferId
            }
        });

        // ถ้ามีข้อมูลก็ทำการแก้ไข
        if (taskToEdit) {
            try {
                await model.tb_transfer.update(
                    {
                        warehouseIdRequester: newWarehouseIdRequester,
                        warehouseIdEquipOwner: newWarehouseIdEquipOwner,
                        userId: newUserId,
                        sessionId: newSessionId,
                        transferStatus: newTransferStatus,
                    },
                    { where: { transferId: taskToEdit.transferId } }
                );

                const jobEdited = await model.tb_transfer.findOne({
                    where: {
                        transferId: taskToEdit.transferId
                    }
                });

                return res.status(201).json({
                    message: 'แก้ไขข้อมูลสำเร็จ',
                    job_to_edit: taskToEdit,
                    job_edited: jobEdited
                });
            } catch (err) {
                return res.status(500).json({
                    message: 'เกิดข้อผิดพลาด ระหว่างการแก้ไขข้อมูล / ตรวจสอบความสัมพันธ์ของข้อมูล',
                    error_messages: err
                });
            }
        } else {
            return res.status(404).json({
                message: 'ไม่พบข้อมูลในระบบ',
            });
        }

    } catch (err) {
        return res.status(500).json({
            message: 'DB error',
            error_messages: err
        });
    }
}

// delete
exports.destroy_tb_transfer = async function (req, res, next) {
    const { transferId } = req.params;

    try {
        const taskToDestroy = await model.tb_transfer.findOne({
            where: {
                transferId: transferId
            }
        });

        const resultNumber = await model.tb_transfer.destroy({
            where: {
                transferId: transferId
            },
        });

        if (resultNumber === 0) {
            return res.status(404).json({
                message: `ไม่พบข้อมูล ${transferId}`
            })
        } else {
            return res.status(200).json({
                taskToDestroy: taskToDestroy,
                data: `ลบรายการ ID: ${transferId} แล้ว`
            })
        }
    } catch (err) {
        return res.status(500).json({
            warning: 'กรุณาตรวจสอบความสัมพันธ์ของข้อมูล/ฐานข้อมูลมีปัญหา',
            message: err
        })
    }
}