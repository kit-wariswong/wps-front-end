const { Op } = require('sequelize');
const model = require('../models/index');
const path = require('path');

const readXlsxFile = require('read-excel-file/node');

exports.index = async function (req, res, next) {
    try {
        const raw_node = await model.NWMonthlyInterfaceReport.findAll({
            // attributes: ['id', 'fullname'],
            where: {
                mir_interface: ['eth1', 'eth2', 'eth7', '10GE1/4/0/9', 'GE1/2/0/11', 'GE1/2/0/15'],
            },
            order: [['mir_node', 'asc']]
        });

        const totalRecord = Object.keys(raw_node).length;
        let avg_availability = 0;
        for (let i = 0; i < totalRecord; i++) {
            avg_availability += raw_node[i].mir_availability;
        }

        return res.status(200).json({
            total_node: totalRecord,
            average: avg_availability / totalRecord,
            nodes: raw_node,
        });
    } catch (err) {
        return res.status(500).json({
            message: err,
        });
    }
}

// upload excel
exports.mir_upload_report = async function (req, res, next) {
    try {
        //req.file.filename
        if (req.file) {
            const { filename } = req.file;
            // look for uploaded file in folder
            const uploadPath = `${path.resolve('./')}/uploads/mir/${filename}`;
            const rows = await readXlsxFile(uploadPath);

            if (rows[0][0] === 'month' && rows[0][5] === 'availability') {
                // skip header
                rows.shift();

                // add rows to array
                let mir_rep = [];
                for (const item of rows) {
                    let data = {
                        mir_month: item[0],
                        mir_node: item[1],
                        mir_interface: item[2],
                        mir_mtbf: item[3],
                        mir_mttr: item[4],
                        mir_availability: item[5],
                        mir_user_id: req.user.id
                    }
                    mir_rep.push(data);
                }
                // truncate and insert all to db
                await model.NWMonthlyInterfaceReport.destroy({ truncate: true });
                await model.NWMonthlyInterfaceReport.bulkCreate(mir_rep);

                let amir_rep = [];
                for (const item of rows) {
                    let data = {
                        amir_month: item[0],
                        amir_node: item[1],
                        amir_interface: item[2],
                        amir_mtbf: item[3],
                        amir_mttr: item[4],
                        amir_availability: item[5],
                        amir_user_id: req.user.id
                    }
                    amir_rep.push(data);
                }
                // insert all to db
                await model.NWAllMonthlyInterfaceReport.bulkCreate(amir_rep);

                return res.status(200).json({
                    message: `Upload และ บันทึกลงฐานข้อมูลเรียบร้อย`,
                    // name: filename,
                    // file: req.file,
                    // rows: rows  
                });
            } else {
                return res.status(400).json({
                    message: `ตรวจสอบความถูกต้องของไฟล์ที่อัพโหลด`,
                });
            }
        } else {
            return res.status(404).json({
                message: `ไม่พบ file ที่อัพโหลด`,
            });
        }

    } catch (err) {
        return res.status(500).json({
            message: `ตรวจสอบไฟล์ที่อัพโหลด ${err}`,
        });
    }
}