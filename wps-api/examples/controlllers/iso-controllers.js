const { Op } = require('sequelize');
const model = require('../models/index');

const upload_pic = require('../modules/module_uploads');

exports.index = async function (req, res, next) {
    try {
        const isoPosts = await model.NWiso.findAll({
            // attributes: ['id', 'fullname'],
            // attributes: {
            //     exclude: ['id']
            // },
            order: [['id', 'desc']],
            // query from nwusers table (models association first)
            include: [
                {
                    model: model.NWUser,
                    as: 'iso_belongs_to_user',
                    attributes: ['fullname', 'email']
                }
            ]
        });
        const totalRecord = await model.NWiso.count();
        return res.status(201).json({
            // message: `จำนวนงาน ISO ${totalRecord} งาน`,
            data: isoPosts
        });
    } catch (err) {
        return res.status(500).json({
            message: err,
            section: 'find all ISO posts'
        });
    }
}

exports.add = async function (req, res, next) {
    const {
        iso_type,
        iso_title,
        iso_detail,
        iso_start_date,
        iso_end_date,
        iso_photo,
        iso_year,
    } = req.body;

    if (iso_photo) {
        const pic_type = 'iso';
        try {
            const pic_path = await upload_pic.saveImageToDisk(iso_photo, pic_type);
            const newIsoPost = await model.NWiso.create({
                iso_type: iso_type,
                iso_title: iso_title,
                iso_detail: iso_detail,
                iso_start_date: iso_start_date,
                iso_end_date: iso_end_date,
                iso_photo: pic_path,
                iso_user_id: req.user.id,
                iso_year: iso_year,
            });
            return res.status(201).json({
                message: 'ป้อนข้อมูลสำเร็จ',
                data: newIsoPost,
            });
        } catch (err) {
            return res.status(500).json({
                messages: err,
                section: `add an ISO post`
            });
        }
    } else {
        try {
            const newIsoPost = await model.NWiso.create({
                iso_type: iso_type,
                iso_title: iso_title,
                iso_detail: iso_detail,
                iso_start_date: iso_start_date,
                iso_end_date: iso_end_date,
                iso_user_id: req.user.id,
                iso_year: iso_year,
            });
            return res.status(201).json({
                message: 'ป้อนข้อมูลสำเร็จ',
                data: newIsoPost,
            });
        } catch (err) {
            return res.status(500).json({
                messages: err,
                section: `add an ISO post without picture`
            });
        }
    }

}

exports.edit_show = async function (req, res, next) {
    // รับ id ของงาน
    const {
        id
    } = req.body;

    // แสดงข้อมูลเดิม
    try {
        const isoPost = await model.NWiso.findOne({
            where: {
                id: id
            }
        });
        if (!isoPost) {
            return res.status(404).json({
                message: 'ไม่มีข้อมูล'
            });
        }
        return res.status(201).json({
            message: `แสดงรายละเอียดของ ${isoPost.iso_title}`,
            isoPost: isoPost
        });
    } catch (err) {
        return res.status(500).json({
            message: err,
            section: 'find an ISO post'
        });
    }


}
exports.edit = async function (req, res, next) {
    const {
        id,
        edit_iso_type,
        edit_iso_title,
        edit_iso_detail,
        edit_iso_start_date,
        edit_iso_end_date,
        edit_iso_photo,
        edit_iso_year,
    } = req.body;

    // edit
    try {

        await model.NWiso.update(
            {
                iso_type: edit_iso_type,
                iso_title: edit_iso_title,
                iso_detail: edit_iso_detail,
                iso_start_date: edit_iso_start_date,
                iso_end_date: edit_iso_end_date,
                iso_photo: edit_iso_photo,
                iso_user_id: req.user.id,
                iso_year: edit_iso_year,
            },
            {
                where: {
                    id: id
                }
            });

        const isoPostEdited = await model.NWiso.findOne({
            where: {
                id: id
            }
        });

        return res.status(201).json({
            message: `แก้ไขสำเร็จ`,
            isoPostEdited: isoPostEdited
        });

    } catch (err) {
        return res.status(500).json({
            message: err,
            section: 'update an ISO post'
        });
    }
}