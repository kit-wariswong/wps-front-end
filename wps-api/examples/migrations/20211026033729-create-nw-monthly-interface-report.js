'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('NWMonthlyInterfaceReports', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      mir_month: {
        type: Sequelize.STRING
      },
      mir_node: {
        type: Sequelize.STRING
      },
      mir_interface: {
        type: Sequelize.STRING
      },
      mir_mtbf: {
        type: Sequelize.STRING
      },
      mir_mttr: {
        type: Sequelize.STRING
      },
      mir_availability: {
        type: Sequelize.DOUBLE
      },
      mir_display: {
        type: Sequelize.BOOLEAN,
        defaultValue: 0
      },
      mir_user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: {
            tableName: 'NWUsers'
          },
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('NWMonthlyInterfaceReports');
  }
};
