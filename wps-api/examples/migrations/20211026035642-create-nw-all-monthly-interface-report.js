'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('NWAllMonthlyInterfaceReports', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      amir_month: {
        type: Sequelize.STRING
      },
      amir_node: {
        type: Sequelize.STRING
      },
      amir_interface: {
        type: Sequelize.STRING
      },
      amir_mtbf: {
        type: Sequelize.STRING
      },
      amir_mttr: {
        type: Sequelize.STRING
      },
      amir_availability: {
        type: Sequelize.DOUBLE
      },
      amir_display: {
        type: Sequelize.BOOLEAN,
        defaultValue: 0
      },
      amir_user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: {
            tableName: 'NWUsers'
          },
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('NWAllMonthlyInterfaceReports');
  }
};