'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('NWisos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      iso_type: {
        type: Sequelize.STRING
      },
      iso_title: {
        type: Sequelize.STRING
      },
      iso_detail: {
        type: Sequelize.TEXT
      },
      iso_start_date: {
        type: Sequelize.STRING
      },
      iso_end_date: {
        type: Sequelize.STRING
      },
      iso_photo: {
        type: Sequelize.STRING,
        defaultValue: 'nopic.png'
      },
      iso_is_active: {
        type: Sequelize.BOOLEAN,
        defaultValue: 1
      },
      // define FK
      iso_user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: {
            tableName: 'NWUsers'
          },
          key: 'id'
        }
      },
      // end of define FK
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('NWisos');
  }
};