'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class NWMonthlyInterfaceReport extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  NWMonthlyInterfaceReport.init({
    mir_month: {
      type: DataTypes.STRING
    },
    mir_node: {
      type: DataTypes.STRING
    },
    mir_interface: {
      type: DataTypes.STRING
    },
    mir_mtbf: {
      type: DataTypes.STRING
    },
    mir_mttr: {
      type: DataTypes.STRING
    },
    mir_availability: {
      type: DataTypes.DOUBLE
    },
    mir_display: {
      type: DataTypes.BOOLEAN,
      defaultValue: 0
    },
    mir_user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: {
          tableName: 'NWUsers'
        },
        key: 'id'
      }
    },
  }, {
    sequelize,
    modelName: 'NWMonthlyInterfaceReport',
    underscored: true,
    tableName: 'NWMonthlyInterfaceReports',
    // name column to match the db
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return NWMonthlyInterfaceReport;
};