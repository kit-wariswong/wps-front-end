'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class NWUser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.NWUser.hasMany(models.NWiso, {
        as: 'user_iso_associate',
        foreignKey: 'iso_user_id', //FK's nwisos table
        sourceKey: 'id', // PK's nwusers table
      });
    }
  };
  NWUser.init({
    fullname: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    role: {
      type: DataTypes.STRING,
      defaultValue: "member"
    },
  }, {
    sequelize,
    modelName: 'NWUser',
    underscored: true,
    tableName: 'NWUsers',
    // name column to match the db
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return NWUser;
};