'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class NWiso extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.NWiso.belongsTo(models.NWUser, {
        as: 'iso_belongs_to_user',
        foreignKey: 'iso_user_id', //FK's nwisos table
        sourceKey: 'id', // PK's nwusers table
      });
    }
  };
  NWiso.init({
    iso_type: {
      type: DataTypes.STRING,
      defaultValue: 'UNDEFINDED TYPE'
    },
    iso_title: {
      type: DataTypes.STRING,
      defaultValue: 'UNDEFINDED Title'
    },
    iso_detail: {
      type: DataTypes.TEXT,
      defaultValue: 'UNDEFINDED Detail'
    },
    iso_start_date: {
      type: DataTypes.STRING,
      defaultValue: `UNDEFINDED Start Date`
    },
    iso_end_date: {
      type: DataTypes.STRING,
      defaultValue: `UNDEFINDED End Date`
    },
    iso_photo: {
      type: DataTypes.STRING,
      defaultValue: 'nopic.png'
    },
    // virtual column
    iso_photo_url: {
      type: DataTypes.VIRTUAL,
      get() {
        return `${process.env.IMAGE_URL}iso/${this.iso_photo}`;
      }
    },
    iso_is_active: {
      type: DataTypes.BOOLEAN,
      defaultValue: 1
    },
    iso_user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: {
          tableName: 'NWUsers'
        },
        key: 'id'
      }
    },
    iso_year: {
      type: DataTypes.INTEGER,
    },
  }, {
    sequelize,
    modelName: 'NWiso',
    underscored: true,
    tableName: 'NWisos',
    // name column to match the db
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return NWiso;
};