'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class NWAllMonthlyInterfaceReport extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  NWAllMonthlyInterfaceReport.init({
    amir_month: {
      type: DataTypes.STRING
    },
    amir_node: {
      type: DataTypes.STRING
    },
    amir_interface: {
      type: DataTypes.STRING
    },
    amir_mtbf: {
      type: DataTypes.STRING
    },
    amir_mttr: {
      type: DataTypes.STRING
    },
    amir_availability: {
      type: DataTypes.DOUBLE
    },
    amir_display: {
      type: DataTypes.BOOLEAN,
      defaultValue: 0
    },
    amir_user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: {
          tableName: 'NWUsers'
        },
        key: 'id'
      }
    },
  }, {
    sequelize,
    modelName: 'NWAllMonthlyInterfaceReport',
    underscored: true,
    tableName: 'NWAllMonthlyInterfaceReports',
    // name column to match the db
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return NWAllMonthlyInterfaceReport;
};