'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tb_transfer_lists', {
      transferListId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      transferId: {
        type: Sequelize.INTEGER,
      },
      equipmentId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION',
        references: {
          model: {
            tableName: 'mb52s'
          },
          key: 'id'
        }
      },
      equipmentValue: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tb_transfer_lists');
  }
};