'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.changeColumn('tb_cs_warehouse_step1s', 'userId', {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION',
      references: {
        model: {
          tableName: 'tb_user_login_infos'
        },
        key: 'userId',
      }
    });

    await queryInterface.changeColumn('tb_transfers', 'userId', {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION',
      references: {
        model: {
          tableName: 'tb_user_login_infos'
        },
        key: 'userId',
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.changeColumn('tb_cs_warehouse_step1s', 'userId', {
      type: Sequelize.INTEGER,
    });
    await queryInterface.changeColumn('tb_transfers', 'userId', {
      type: Sequelize.INTEGER,
    });
  }
};
