'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('safety_puchaifai_ne2s', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      puchaifai_stuff_id: {
        type: Sequelize.STRING
      },
      puchaifai_stuff_name: {
        type: Sequelize.STRING
      },
      puchaifai_e010: {
        type: Sequelize.DOUBLE
      },
      puchaifai_e020: {
        type: Sequelize.DOUBLE
      },
      puchaifai_e030: {
        type: Sequelize.DOUBLE
      },
      puchaifai_e040: {
        type: Sequelize.DOUBLE
      },
      puchaifai_e050: {
        type: Sequelize.DOUBLE
      },
      puchaifai_e060: {
        type: Sequelize.DOUBLE
      },
      puchaifai_e070: {
        type: Sequelize.DOUBLE
      },
      puchaifai_e080: {
        type: Sequelize.DOUBLE
      },
      puchaifai_e090: {
        type: Sequelize.DOUBLE
      },
      puchaifai_e100: {
        type: Sequelize.DOUBLE
      },
      puchaifai_e110: {
        type: Sequelize.DOUBLE
      },
      puchaifai_e120: {
        type: Sequelize.DOUBLE
      },
      puchaifai_sum: {
        type: Sequelize.DOUBLE
      },
      puchaifai_middle_price: {
        type: Sequelize.DOUBLE
      },
      puchaifai_sum_price: {
        type: Sequelize.DOUBLE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('safety_puchaifai_ne2s');
  }
};