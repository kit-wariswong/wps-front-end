'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('zcsr181_fetches', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      officeId: {
        type: Sequelize.STRING
      },
      contractAccountNumber: {
        type: Sequelize.INTEGER
      },
      requestNumber: {
        type: Sequelize.INTEGER
      },
      requestType: {
        type: Sequelize.STRING
      },
      dateRecievedRequest: {
        type: Sequelize.STRING
      },
      requestorName: {
        type: Sequelize.STRING
      },
      requestorAddress: {
        type: Sequelize.STRING
      },
      saleOrderNumber: {
        type: Sequelize.INTEGER
      },
      billOrderNumber: {
        type: Sequelize.INTEGER
      },
      archiveType: {
        type: Sequelize.STRING
      },
      stuffId: {
        type: Sequelize.STRING
      },
      stuffName: {
        type: Sequelize.STRING
      },
      amount: {
        type: Sequelize.INTEGER
      },
      unit: {
        type: Sequelize.STRING
      },
      standardPricePerUnit: {
        type: Sequelize.DOUBLE
      },
      sellingPricePerUnit: {
        type: Sequelize.DOUBLE
      },
      taxPerUnit: {
        type: Sequelize.DOUBLE
      },
      standardPrice: {
        type: Sequelize.DOUBLE
      },
      sellingPrice: {
        type: Sequelize.DOUBLE
      },
      tax: {
        type: Sequelize.DOUBLE
      },
      totalPrice: {
        type: Sequelize.DOUBLE
      },
      debtSrDate: {
        type: Sequelize.STRING
      },
      paymentDate: {
        type: Sequelize.STRING
      },
      jobOrder: {
        type: Sequelize.STRING
      },
      wbsNumber: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('zcsr181_fetches');
  }
};