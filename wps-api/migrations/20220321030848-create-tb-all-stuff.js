'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tb_all_stuffs', {
      stuffId: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.STRING
      },
      groupId: {
        type: Sequelize.STRING
      },
      stuffIdCut: {
        type: Sequelize.STRING
      },
      stuffNameEng: {
        type: Sequelize.STRING
      },
      stuffNameTh: {
        type: Sequelize.STRING
      },
      counter: {
        type: Sequelize.STRING
      },
      stuffGroup: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tb_all_stuffs');
  }
};