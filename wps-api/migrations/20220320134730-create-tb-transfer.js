'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tb_transfers', {
      transferId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      warehouseIdRequester: {
        type: Sequelize.STRING
      },
      warehouseIdEquipOwner: {
        type: Sequelize.STRING
      },
      userId: {
        type: Sequelize.INTEGER
      },
      sessionId: {
        type: Sequelize.STRING
      },
      transferStatus: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tb_transfers');
  }
};