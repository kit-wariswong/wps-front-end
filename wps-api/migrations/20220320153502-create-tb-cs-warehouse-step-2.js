'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tb_cs_warehouse_step2s', {
      csWarehouseS2Id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      wbsNumber: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION',
        references: {
          model: {
            tableName: 'zcsr181s'
          },
          key: 'id',
        }
      },
      userId: {
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION',
        references: {
          model: {
            tableName: 'tb_user_login_infos'
          },
          key: 'userId',
        }
      },
      timeStamp: {
        type: Sequelize.STRING
      },
      warehouseId: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tb_cs_warehouse_step2s');
  }
};