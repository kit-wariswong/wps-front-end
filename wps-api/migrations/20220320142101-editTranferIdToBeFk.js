'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.changeColumn('tb_transfer_lists', 'transferId', {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION',
      references: {
        model: {
          tableName: 'tb_transfers'
        },
        key: 'transferId'
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
     await queryInterface.changeColumn('tb_transfer_lists', 'transferId', {
      type: Sequelize.INTEGER,
    });
  }
};
