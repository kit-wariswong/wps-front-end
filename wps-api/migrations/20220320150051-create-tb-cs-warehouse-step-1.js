'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tb_cs_warehouse_step1s', {
      csWarehouseS1Id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      setId: {
        type: Sequelize.INTEGER
      },
      jobName: {
        type: Sequelize.STRING
      },
      jobCustomer: {
        type: Sequelize.STRING
      },
      userId: {
        type: Sequelize.INTEGER
      },
      warehouseId: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tb_cs_warehouse_step1s');
  }
};