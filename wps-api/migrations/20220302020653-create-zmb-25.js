'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Zmb25s', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      inventory_id: {
        type: Sequelize.STRING
      },
      stuff_id: {
        type: Sequelize.STRING
      },
      date_required: {
        type: Sequelize.STRING
      },
      mvt: {
        type: Sequelize.INTEGER
      },
      qty_needed: {
        type: Sequelize.DOUBLE
      },
      qty_removed: {
        type: Sequelize.DOUBLE
      },
      qty_different: {
        type: Sequelize.DOUBLE
      },
      bun: {
        type: Sequelize.STRING
      },
      netword_number: {
        type: Sequelize.STRING
      },
      wbs_number: {
        type: Sequelize.STRING
      },
      po_id: {
        type: Sequelize.STRING
      },
      reserve_number: {
        type: Sequelize.STRING
      },
      rca: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Zmb25s');
  }
};