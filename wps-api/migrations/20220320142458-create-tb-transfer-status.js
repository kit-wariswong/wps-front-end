'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tb_transfer_statuses', {
      transferStatusId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      transferStatus: {
        type: Sequelize.ENUM,
        values: ['begin', 'confirm', 'cancel', 'approved', 'disapproved'],
        defaultValue: 'begin',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tb_transfer_statuses');
  }
};