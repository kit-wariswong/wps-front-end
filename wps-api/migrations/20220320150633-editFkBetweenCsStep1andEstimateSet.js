'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     await queryInterface.changeColumn('tb_cs_warehouse_step1s', 'setId', {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION',
      references: {
        model: {
          tableName: 'tb_estimate_sets'
        },
        key: 'setId',
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
     await queryInterface.changeColumn('tb_cs_warehouse_step1s', 'setId', {
      type: Sequelize.INTEGER,
    });
  }
};
