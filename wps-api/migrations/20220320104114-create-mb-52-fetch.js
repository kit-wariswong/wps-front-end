'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('mb52_fetches', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      invId: {
        type: Sequelize.STRING
      },
      stockHolder: {
        type: Sequelize.STRING
      },
      equipmentId: {
        type: Sequelize.STRING
      },
      stuffName: {
        type: Sequelize.STRING
      },
      batch: {
        type: Sequelize.STRING
      },
      counter: {
        type: Sequelize.STRING
      },
      stockAvailable: {
        type: Sequelize.DOUBLE
      },
      value: {
        type: Sequelize.DOUBLE
      },
      transfering: {
        type: Sequelize.DOUBLE
      },
      suspend: {
        type: Sequelize.DOUBLE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('mb52_fetches');
  }
};